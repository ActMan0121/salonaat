package com.wxcomp.comm;

import com.wxcomp.data_structure.STBookingInfo;
import com.wxcomp.data_structure.STGiftCardInfo;
import com.wxcomp.data_structure.STLiveFeedbackInfo;
import com.wxcomp.data_structure.STLoyaltyPointInfo;
import com.wxcomp.data_structure.STNotificationInfo;
import com.wxcomp.data_structure.STPromotionInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.data_structure.STSubCategoryInfo;
import com.wxcomp.data_structure.STUserInfo;

import java.util.ArrayList;

/**
 * Created by Administrator on 12/25/2016.
 */

public class CommDelegate {
	public void onUploadImageResult(int statusCode, String statusDescription, String imageUrl) {}
	public void onSignupResult(int statusCode, String statusDescription, String user_id, String auth_token) {}
	public void onLoginResult(int statusCode, String statusDescription, String user_id, int user_type, String auth_token) {}
	public void onSignoutResult(int statusCode, String statusDescription) {}
	public void onGetSubCategoriesResult(int statusCode, String statusDescription, ArrayList<STSubCategoryInfo> subCategoriesList) {}
	public void onGetServicesResult(int statusCode, String statusDescription, ArrayList<STSalonInfo> salonsList) {}
	public void onGetServiceDetailInfo(int statusCode, String statusDescription, STSalonInfo salonInfo) {}
	public void onFindServicesResult(int statusCode, String statusDescription, ArrayList<STSalonInfo> salonsList) {}
	public void onTakeFavoriteResult(int statusCode, String statusDescription) {}
	public void onSaveCardInfoResult(int statusCode, String statusDescription) {}
	public void onBookResult(int statusCode, String statusDescription) {}
	public void onReviewSalonResult(int statusCode, String statusDescription) {}
	public void onGetLiveFeedResult(int statusCode, String statusDescription, ArrayList<STLiveFeedbackInfo> liveFeedbackList) {}
	public void onGetPromotionsResult(int statusCode, String statusDescription, ArrayList<STPromotionInfo> promotionsList) {}
	public void onGetPromotionDetailResult(int statusCode, String statusDescription, STPromotionInfo promotionInfo) {}
	public void onGetServedBookingListResult(int statusCode, String statusDescription, ArrayList<STBookingInfo> bookingList) {}
	public void onGetScheduledBookingListResult(int statusCode, String statusDescription, ArrayList<STBookingInfo> bookingList) {}
	public void onGetBookingDetailResult(int statusCode, String statusDescription, STBookingInfo bookingInfo) {}
	public void onConfirmBookingResult(int statusCode, String statusDescription) {}
	public void onCancelBookingResult(int statusCode, String statusDescription) {}
	public void onGetNotificationListResult(int statusCode, String statusDescription, ArrayList<STNotificationInfo> notificationsList) {}
	public void onGetNotificationDetail(int statusCode, String statusDescription, STNotificationInfo notificationInfo) {}
	public void onReviewNotification(int statusCode, String statusDescription) {}
	public void onGetNewLoyaltyPointsList(int statusCode, String statusDescription, ArrayList<STLoyaltyPointInfo> loyaltyPointsList) {}
	public void onGetRedeemLoyaltyPointsList(int statusCode, String statusDescription, ArrayList<STLoyaltyPointInfo> loyaltyPointsList) {}
	public void onGetGiftCardsList(int statusCode, String statusDescription, ArrayList<STGiftCardInfo> giftCardsList) {}
	public void onGetGiftCardDetailResult(int statusCode, String statusDescription, STGiftCardInfo giftCardInfo) {}
	public void onGetFavoritesList(int statusCode, String statusDescription, ArrayList<STSalonInfo> favoriteSalonsList) {}
	public void onRemoveFavoriteResult(int statusCode, String statusDescription) {}
	public void onGetInviteCodeResult(int statusCode, String statusDescription, String invite_code) {}
	public void onFeedbackResult(int statusCode, String statusDescription) {}
	public void onUpdateProfileResult(int statusCode, String statusDescription) {}
	public void onGetMiscInfoResult(int statusCode, String statusDescription) {}
	public void onGetUserProfileResult(int statusCode, String statusDescription, STUserInfo userinfo) {}
	public void onGetLoyaltyPointsResult(int statusCode, String statusDescription, int loyalty_points) {}
	public void onPurchaseGiftCardResult(int statusCode, String statusDescription) {}
	public void onShareGiftCardResult(int statusCode, String statusDescription) {}
}
