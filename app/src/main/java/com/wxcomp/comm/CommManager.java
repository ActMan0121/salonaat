package com.wxcomp.comm;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wxcomp.data_structure.STBookingInfo;
import com.wxcomp.data_structure.STGiftCardInfo;
import com.wxcomp.data_structure.STLiveFeedbackInfo;
import com.wxcomp.data_structure.STLoyaltyPointInfo;
import com.wxcomp.data_structure.STNotificationInfo;
import com.wxcomp.data_structure.STPromotionInfo;
import com.wxcomp.data_structure.STReviewInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.data_structure.STServiceInfo;
import com.wxcomp.data_structure.STSubCategoryInfo;
import com.wxcomp.data_structure.STUserInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;
import com.wxcomp.salonaat.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Administrator on 12/24/2016.
 */

public class CommManager {
//	public static String baseUrl = "http://192.168.1.170:3000/";
	public static String baseUrl = "http://132.148.82.148/";

	public static int STATUS_CODE_SUCCESS = 0;
	public static int STATUS_CODE_CONN_ERROR = -9999;
	public static int STATUS_CODE_EXCEPTION = -9998;

	public static String RETCODE = "status";
	public static String RETMSG = "description";
	public static String RETDATA = "data";

	private static final int timeOut = 10 * 1000;

	public static void uploadImage(final Context context, String path, final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("file", new File(path));

			client.post(context, baseUrl + "api/media/uploadImage", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);

							String url = retdata.getString("url");
							if (delegate != null)
								delegate.onUploadImageResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), url);
						} else {
							if (delegate != null)
								delegate.onUploadImageResult(retcode, retmsg, "");
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onUploadImageResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), "");
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onUploadImageResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), "");
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void signUp(final Context context,
							  String photo,
							  String first_name,
							  String last_name,
							  String middle_name,
							  int user_type,
							  String birthday,
							  int gender,
							  String location,
							  String email,
							  String password,
							  String mobile,
							  String description,
							  int sign_type,
							  String invite_code,
							  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("photo", photo);
			params.put("first_name", first_name);
			params.put("last_name", last_name);
			params.put("middle_name", middle_name);
			params.put("user_type", user_type);			// 0 - personal, 1 - business
			params.put("sign_type", sign_type);			// 1 - normal, 2 - facebook, 3 - google
			params.put("birthday", birthday);			// yyyy-MM-dd
			params.put("gender", gender);				// 0 - male, 1 - female, 2 - undef
			params.put("location", location);
			params.put("email", email);
			params.put("password", password);
			params.put("mobile", mobile);
			params.put("description", description);
			params.put("invite_code", invite_code);

			client.post(context, baseUrl + "api/user/signup", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);

							String user_id = retdata.getString("id");
							String token = retdata.getString("auth");

							if (delegate != null)
								delegate.onSignupResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), user_id, token);
						} else {
							if (delegate != null)
								delegate.onSignupResult(retcode, retmsg, null, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onSignupResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null, null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onSignupResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null, null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void login(final Context context,
							 String email,
							 String password,
							 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("email", email);
			params.put("password", password);

			client.post(context, baseUrl + "api/user/login", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);

							String user_id = retdata.getString("id");
							int user_type = retdata.getInt("type");
							String auth_token = retdata.getString("auth");

							if (delegate != null)
								delegate.onLoginResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), user_id, user_type, auth_token);
						} else {
							if (delegate != null)
								delegate.onLoginResult(retcode, retmsg, "", -1, "");
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onLoginResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), "", -1, "");
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onLoginResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), "", -1, "");
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void signout(final Context context, final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());

			client.post(context, baseUrl + "api/user/signout", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onSignoutResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onSignoutResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onSignoutResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onSignoutResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getSubCategories(final Context context,
										int gender,
										int category,
										int service_type,
										final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("gender", gender);

			if (category == Const.SERVICE_CATEGORY_BODY)
				params.put("category", "Body");
			else if (category == Const.SERVICE_CATEGORY_FACE)
				params.put("category", "Face");
			else if (category == Const.SERVICE_CATEGORY_COSMETICS)
				params.put("category", "Cosmetics");
			else if (category == Const.SERVICE_CATEGORY_HAIR)
				params.put("category", "Hair");

			params.put("service_type", service_type);


			client.get(context, baseUrl + "api/subcategory/list_by_gender_category", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							ArrayList<STSubCategoryInfo> subCategoriesList = new ArrayList<STSubCategoryInfo>();
							JSONArray retdata = response.getJSONArray(RETDATA);
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);
								subCategoriesList.add(STSubCategoryInfo.decodeFromJSON(item));
							}

							if (delegate != null)
								delegate.onGetSubCategoriesResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), subCategoriesList);
						} else {
							if (delegate != null)
								delegate.onGetSubCategoriesResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetSubCategoriesResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetSubCategoriesResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getServicesList(final Context context,
									   String sub_category_id,
									   int page_index,
									   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("subcategory_id", sub_category_id);
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/salon/list_by_subcategory", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							ArrayList<STSalonInfo> salonsList = new ArrayList<>();
							JSONArray retdata = response.getJSONArray(RETDATA);
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STSalonInfo salonInfo = new STSalonInfo();

								try { salonInfo.uid = item.getString("id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { salonInfo.title = item.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { salonInfo.contents = item.getString("comment"); } catch (Exception ex) { ex.printStackTrace(); }
								try { salonInfo.imageUrl = item.getString("cover"); } catch (Exception ex) { ex.printStackTrace(); }

								salonsList.add(salonInfo);
							}

							if (delegate != null)
								delegate.onGetServicesResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), salonsList);
						} else {
							if (delegate != null)
								delegate.onGetServicesResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetServicesResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetServicesResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getServiceDetailInfo(final Context context,
											String salon_id,
											final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("salon_id", salon_id);

			client.get(context, baseUrl + "api/salon/info_services", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);

							STSalonInfo salonInfo = new STSalonInfo();

							try { salonInfo.uid = retdata.getString("salon_id"); } catch (Exception ex) { ex.printStackTrace(); }
							try { salonInfo.title = retdata.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
							try { salonInfo.imageUrl = retdata.getString("cover"); } catch (Exception ex) { ex.printStackTrace(); }
							try { salonInfo.contents = retdata.getString("comment"); } catch (Exception ex) { ex.printStackTrace(); }
							try { salonInfo.rating = retdata.getInt("rating"); } catch (Exception ex) { ex.printStackTrace(); }
							try { salonInfo.address = retdata.getString("address"); } catch (Exception ex) { ex.printStackTrace(); }
							try {
								String favorite = retdata.getString("favorite");
								salonInfo.favorite = favorite.equals("true");
							} catch (Exception ex) { ex.printStackTrace(); }
							try {
								JSONObject latlng = retdata.getJSONObject("location");
								salonInfo.latitude = latlng.getDouble("lat");
								salonInfo.longitude = latlng.getDouble("lon");
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							try { salonInfo.phone = retdata.getString("phone"); } catch (Exception ex) { ex.printStackTrace(); }
							try {
								JSONArray photos = retdata.getJSONArray("photos");
								for (int i = 0; i < photos.length(); i++) {
									salonInfo.photosArray.add(photos.getString(i));
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							try {
								JSONArray services = retdata.getJSONArray("services");
								for (int i = 0; i < services.length(); i++) {
									JSONObject jsonService = services.getJSONObject(i);
									STServiceInfo serviceInfo = new STServiceInfo();

									try { serviceInfo.uid = jsonService.getString("service_id"); } catch (Exception ex) { ex.printStackTrace(); }
									try { serviceInfo.title = jsonService.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
									try { serviceInfo.contents = jsonService.getString("comment"); } catch (Exception ex) { ex.printStackTrace(); }
									try { serviceInfo.price = jsonService.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }

									salonInfo.servicesArray.add(serviceInfo);
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							try {
								JSONArray reviews = retdata.getJSONArray("reviews");
								for (int i = 0; i < reviews.length(); i++) {
									JSONObject jsonReview = reviews.getJSONObject(i);
									STReviewInfo reviewInfo = new STReviewInfo();

									try { reviewInfo.uid = jsonReview.getString("review_id"); } catch (Exception ex) { ex.printStackTrace(); }
									try { reviewInfo.contents = jsonReview.getString("content"); } catch (Exception ex) { ex.printStackTrace(); }
									try { reviewInfo.rating = jsonReview.getInt("rating"); } catch (Exception ex) { ex.printStackTrace(); }

									salonInfo.reviewsArray.add(reviewInfo);
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}

							if (delegate != null)
								delegate.onGetServiceDetailInfo(STATUS_CODE_SUCCESS, context.getString(R.string.success), salonInfo);
						} else {
							if (delegate != null)
								delegate.onGetServiceDetailInfo(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetServiceDetailInfo(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetServiceDetailInfo(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void findNearbyServices(final Context context,
										  int distance,
										  float likes,
										  int price,
										  int ratings,
										  String subcategory_id,
										  double lat,
										  double lng,
										  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("lat", lat);
			params.put("lon", lng);
			params.put("distance", distance);
			params.put("like", likes);
			params.put("price", price);
			params.put("rating", ratings);
			params.put("subcategory_id", subcategory_id);

			client.get(context, baseUrl + "api/salon/find", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							ArrayList<STSalonInfo> salonsList = new ArrayList<STSalonInfo>();
							JSONArray retdata = response.getJSONArray(RETDATA);
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STSalonInfo salonInfo = new STSalonInfo();

								salonInfo.uid = item.getString("salon_id");
								salonInfo.title = item.getString("name");
								salonInfo.contents = item.getString("comment");
								salonInfo.imageUrl = item.getString("cover");
								salonInfo.price = item.getInt("price");
								salonInfo.latitude = item.getDouble("lat");
								salonInfo.longitude = item.getDouble("lon");
								salonInfo.gender = item.getInt("gender");

								salonsList.add(salonInfo);
							}

							if (delegate != null)
								delegate.onFindServicesResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), salonsList);
						} else {
							if (delegate != null)
								delegate.onFindServicesResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onFindServicesResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onFindServicesResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void takeFavorite(final Context context,
									String salon_id,
									final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("salon_id", salon_id);
			params.put("like", "true");

			client.post(context, baseUrl + "api/favorite/update", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onTakeFavoriteResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onTakeFavoriteResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onTakeFavoriteResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onTakeFavoriteResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void saveCardInfo(final Context context,
									String card_holder_name,
									String card_number,
									String cvv_no,
									String exp_date,
									final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("card_holder_name", card_holder_name);
			params.put("card_number", card_number);
			params.put("cvv_no", cvv_no);
			params.put("exp_date", exp_date);

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onSaveCardInfoResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onSaveCardInfoResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onSaveCardInfoResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onSaveCardInfoResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void bookService(final Context context,
								   String salon_id,
								   int total_price,
								   int loyalty_points,
								   String gift_card_ids,
								   String service_ids,
								   String book_time,
								   String card_token,
								   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("loyalty", loyalty_points);
			params.put("gifts", gift_card_ids);
			params.put("service", service_ids);
			params.put("date", book_time);
			params.put("salon_id", salon_id);
			params.put("price", total_price);
			params.put("card_token", card_token);

			client.post(context, baseUrl + "api/book/request", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onBookResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onBookResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onBookResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onBookResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void reviewSalon(final Context context,
								   String salon_id,
								   float rating,
								   String contents,
								   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("salon_id", salon_id);
			params.put("rating", rating);
			params.put("content", contents);

			client.post(context, baseUrl + "api/review/new", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onReviewSalonResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onReviewSalonResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onReviewSalonResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onReviewSalonResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getLiveFeedList(final Context context,
									   int page_index,
									   final CommDelegate delegate)
	{
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page", page_index);

			client.get(context, baseUrl + "api/user/list_live", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STLiveFeedbackInfo> liveFeedbackList = new ArrayList<STLiveFeedbackInfo>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STLiveFeedbackInfo info = new STLiveFeedbackInfo();

								try { info.type = item.getString("type"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.contents = item.getString("comment"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.user_image_url = item.getString("cover"); } catch (Exception ex) { ex.printStackTrace(); }

								liveFeedbackList.add(info);
							}

							if (delegate != null)
								delegate.onGetLiveFeedResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), liveFeedbackList);
						} else {
							if (delegate != null)
								delegate.onGetLiveFeedResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetLiveFeedResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetLiveFeedResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getPromotionsList(final Context context,
										 int page_index,
										 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/promotion/list", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STPromotionInfo> promotionsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);
								STPromotionInfo info = new STPromotionInfo();

								try { info.uid = item.getString("promotion_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("promotion_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.imageUrl = item.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.start_date = item.getString("start"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.end_date = item.getString("end"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.percent = item.getInt("percent"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.contents = item.getString("description"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.salon_name = item.getString("salon_name"); } catch (Exception ex) { ex.printStackTrace(); }

								promotionsList.add(info);
							}

							if (delegate != null)
								delegate.onGetPromotionsResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), promotionsList);
						} else {
							if (delegate != null)
								delegate.onGetPromotionsResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetPromotionsResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetPromotionsResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getPromotionDetail(final Context context,
										  String promotion_id,
										  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("promotion_id", promotion_id);

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							STPromotionInfo promotionInfo = STPromotionInfo.decodeFromJSON(retdata);

							if (delegate != null)
								delegate.onGetPromotionDetailResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), promotionInfo);
						} else {
							if (delegate != null)
								delegate.onGetPromotionDetailResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetPromotionDetailResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetPromotionDetailResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getServedBookingHistory(final Context context,
											   int page_index,
											   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/book/list_served", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STBookingInfo> bookingList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);
								STBookingInfo info = new STBookingInfo();

								try { info.uid = item.getString("book_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.salon_title = item.getString("salon_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.service_name = item.getString("service_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.serviceTime = item.getString("served_date"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.price = item.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }

								bookingList.add(info);
							}

							if (delegate != null)
								delegate.onGetServedBookingListResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), bookingList);
						} else {
							if (delegate != null)
								delegate.onGetServedBookingListResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetServedBookingListResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetServedBookingListResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getScheduledBookingHistory(final Context context,
												  int page_index,
												  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/book/list_scheduled", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STBookingInfo> bookingList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);
								STBookingInfo info = new STBookingInfo();

								try { info.uid = item.getString("book_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.salon_title = item.getString("salon_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.service_name = item.getString("service_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.serviceTime = item.getString("schedule_date"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.price = item.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }

								bookingList.add(info);
							}

							if (delegate != null)
								delegate.onGetScheduledBookingListResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), bookingList);
						} else {
							if (delegate != null)
								delegate.onGetScheduledBookingListResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetScheduledBookingListResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetScheduledBookingListResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getBookingInfoDetail(final Context context,
											 String booking_id,
											 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("booking_id", booking_id);

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							STBookingInfo bookingInfo = STBookingInfo.decodeFromJSON(retdata);

							if (delegate != null)
								delegate.onGetBookingDetailResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), bookingInfo);
						} else {
							if (delegate != null)
								delegate.onGetBookingDetailResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetBookingDetailResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetBookingDetailResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void confirmBookingInfoDetail(final Context context,
												String booking_id,
												final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("service_id", booking_id);

			client.post(context, baseUrl + "api/book/done", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onConfirmBookingResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onConfirmBookingResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onConfirmBookingResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onConfirmBookingResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void cancelBookingInfoDetail(final Context context,
											   String booking_id,
											   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("service_id", booking_id);

			client.post(context, baseUrl + "api/book/cancel", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onCancelBookingResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onCancelBookingResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onCancelBookingResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onCancelBookingResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getNotificationsList(final Context context,
											int page_index,
											final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/notification/list", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STNotificationInfo> notificationsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STNotificationInfo info = new STNotificationInfo();

								try { info.uid = item.getString("notification_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.contents = item.getString("description"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.new_flag = item.getBoolean("reviewed") ? 0 : 1; } catch (Exception ex) { ex.printStackTrace(); }

								notificationsList.add(info);
							}

							if (delegate != null)
								delegate.onGetNotificationListResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), notificationsList);
						} else {
							if (delegate != null)
								delegate.onGetNotificationListResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetNotificationListResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetNotificationListResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getNotificationDetail(final Context context,
											 String notification_id,
											 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("notification_id", notification_id);

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							STNotificationInfo notificationInfo = STNotificationInfo.decodeFromJSON(retdata);

							if (delegate != null)
								delegate.onGetNotificationDetail(STATUS_CODE_SUCCESS, context.getString(R.string.success), notificationInfo);
						} else {
							if (delegate != null)
								delegate.onGetNotificationDetail(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetNotificationDetail(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetNotificationDetail(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void reviewNotification(final Context context,
										  String notification_id,
										  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("notification_id", notification_id);

			client.post(context, baseUrl + "api/notification/review", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onReviewNotification(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onReviewNotification(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onReviewNotification(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onReviewNotification(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getNewLoyaltyPointsList(final Context context,
											   int page_index,
											   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page", page_index);

			client.get(context, baseUrl + "api/user/loyalty_new", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STLoyaltyPointInfo> loyaltyPointsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STLoyaltyPointInfo info = new STLoyaltyPointInfo();

								try { info.uid = item.getString("_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.remark = item.getString("content"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.loyalty_points = item.getInt("point"); } catch (Exception ex) { ex.printStackTrace(); }

								loyaltyPointsList.add(info);
							}

							if (delegate != null)
								delegate.onGetNewLoyaltyPointsList(STATUS_CODE_SUCCESS, context.getString(R.string.success), loyaltyPointsList);
						} else {
							if (delegate != null)
								delegate.onGetNewLoyaltyPointsList(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetNewLoyaltyPointsList(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetNewLoyaltyPointsList(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getRedeemLoyaltyPointsList(final Context context,
												  int page_index,
												  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page", page_index);

			client.get(context, baseUrl + "api/user/loyalty_redeem", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STLoyaltyPointInfo> loyaltyPointsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STLoyaltyPointInfo info = new STLoyaltyPointInfo();

								try { info.uid = item.getString("_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.remark = item.getString("content"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.loyalty_points = item.getInt("point"); } catch (Exception ex) { ex.printStackTrace(); }

								loyaltyPointsList.add(info);
							}

							if (delegate != null)
								delegate.onGetRedeemLoyaltyPointsList(STATUS_CODE_SUCCESS, context.getString(R.string.success), loyaltyPointsList);
						} else {
							if (delegate != null)
								delegate.onGetRedeemLoyaltyPointsList(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetRedeemLoyaltyPointsList(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetRedeemLoyaltyPointsList(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public static void getGiftCardsList(final Context context,
										int page_index,
										final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/gift/list", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STGiftCardInfo> giftCardsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STGiftCardInfo info = new STGiftCardInfo();

								try { info.uid = item.getString("_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.price = item.getInt("point"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.image_url = item.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }

								giftCardsList.add(info);
							}

							if (delegate != null)
								delegate.onGetGiftCardsList(STATUS_CODE_SUCCESS, context.getString(R.string.success), giftCardsList);
						} else {
							if (delegate != null)
								delegate.onGetGiftCardsList(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetGiftCardsList(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetGiftCardsList(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getPurchasedGiftCardsList(final Context context,
												 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();

			client.get(context, baseUrl + "api/gift/list_purchased", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STGiftCardInfo> giftCardsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STGiftCardInfo info = new STGiftCardInfo();

								try { info.uid = item.getString("gift_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("gift_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.price = item.getInt("point"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.image_url = item.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }

								giftCardsList.add(info);
							}

							if (delegate != null)
								delegate.onGetGiftCardsList(STATUS_CODE_SUCCESS, context.getString(R.string.success), giftCardsList);
						} else {
							if (delegate != null)
								delegate.onGetGiftCardsList(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetGiftCardsList(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetGiftCardsList(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public static void getGiftCardDetail(final Context context,
										 String giftcard_id,
										 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("giftcard_id", giftcard_id);

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							STGiftCardInfo giftCardInfo = STGiftCardInfo.decodeFromJSON(retdata);

							if (delegate != null)
								delegate.onGetGiftCardDetailResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), giftCardInfo);
						} else {
							if (delegate != null)
								delegate.onGetGiftCardDetailResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetGiftCardDetailResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetGiftCardDetailResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getFavoritesList(final Context context,
										int page_index,
										final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("page_index", page_index);

			client.get(context, baseUrl + "api/favorite/list", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONArray retdata = response.getJSONArray(RETDATA);
							ArrayList<STSalonInfo> salonsList = new ArrayList<>();
							for (int i = 0; i < retdata.length(); i++) {
								JSONObject item = retdata.getJSONObject(i);

								STSalonInfo info = new STSalonInfo();

								try { info.uid = item.getString("salon_id"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.title = item.getString("salon_name"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.contents = item.getString("comment"); } catch (Exception ex) { ex.printStackTrace(); }
								try { info.imageUrl = item.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }

								salonsList.add(info);
							}

							if (delegate != null)
								delegate.onGetFavoritesList(STATUS_CODE_SUCCESS, context.getString(R.string.success), salonsList);
						} else {
							if (delegate != null)
								delegate.onGetFavoritesList(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetFavoritesList(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetFavoritesList(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void removeFavorite(final Context context,
									  String salon_id,
									  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("salon_id", salon_id);
			params.put("like", "false");

			client.post(context, baseUrl + "api/favorite/update", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onRemoveFavoriteResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onRemoveFavoriteResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onRemoveFavoriteResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onRemoveFavoriteResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getInviteCode(final Context context,
									 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();

			client.get(context, baseUrl + "api/user/invite", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);

							String invite_code = retdata.getString("invite_code");

							if (delegate != null)
								delegate.onGetInviteCodeResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), invite_code);
						} else {
							if (delegate != null)
								delegate.onGetInviteCodeResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetInviteCodeResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetInviteCodeResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void uploadFeedback(final Context context,
									  String contents,
									  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("content", contents);

			client.post(context, baseUrl + "api/feedback/new", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onFeedbackResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onFeedbackResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onFeedbackResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onFeedbackResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void updateProfile(final Context context,
									 String photo,
									 String first_name,
									 String last_name,
									 String middle_name,
									 int user_type,
									 String birthday,
									 int gender,
									 String location,
									 String email,
									 String password,
									 String mobile,
									 String description,
									 int sign_type,
									 String invite_code,
									 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());
			params.put("photo", photo);
			params.put("first_name", first_name);
			params.put("last_name", last_name);
			params.put("middle_name", middle_name);
			params.put("user_type", user_type);
			params.put("sign_type", sign_type);
			params.put("birthday", birthday);
			params.put("gender", gender);
			params.put("location", location);
			params.put("email", email);
			params.put("password", password);
			params.put("mobile", mobile);
			params.put("description", description);
			params.put("invite_code", invite_code);

			client.post(context, baseUrl + "api/user/updateProfile", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onUpdateProfileResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onUpdateProfileResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onUpdateProfileResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onUpdateProfileResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getMiscInfo(final Context context,
								   final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("user_id", Global.loadUserID());

			client.post(context, baseUrl + "", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onGetMiscInfoResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onGetMiscInfoResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetMiscInfoResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetMiscInfoResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void getUserProfile(final Context context,
									  final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("id", Global.loadUserID());

			client.get(context, baseUrl + "api/user/get_by_id", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							if (delegate != null)
								delegate.onGetUserProfileResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), STUserInfo.decodeFromJSON(retdata));
						} else {
							if (delegate != null)
								delegate.onGetUserProfileResult(retcode, retmsg, null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetUserProfileResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), null);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetUserProfileResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), null);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public static void getLoyaltyPoints(final Context context,
										final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();

			client.get(context, baseUrl + "api/user/get_loyalty", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							JSONObject retdata = response.getJSONObject(RETDATA);
							int loyaltyPoints = retdata.getInt("loyalty");

							if (delegate != null)
								delegate.onGetLoyaltyPointsResult(STATUS_CODE_SUCCESS, context.getString(R.string.success), loyaltyPoints);
						} else {
							if (delegate != null)
								delegate.onGetLoyaltyPointsResult(retcode, retmsg, 0);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onGetLoyaltyPointsResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception), 0);
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onGetLoyaltyPointsResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error), 0);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void purchaseGiftCard(final Context context,
										final String gift_id,
										final int points,
										final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("gift_id", gift_id);
			params.put("point", points);

			client.post(context, baseUrl + "api/gift/purchase", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onPurchaseGiftCardResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onPurchaseGiftCardResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onPurchaseGiftCardResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onPurchaseGiftCardResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void shareGiftCard(final Context context,
									 final String gift_id,
									 final String email,
									 final CommDelegate delegate) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("auth", Global.loadAuthToken());
			client.setTimeout(timeOut);

			RequestParams params = new RequestParams();
			params.put("gift_id", gift_id);
			params.put("email", email);

			client.post(context, baseUrl + "api/gift/share", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						JSONObject response = new JSONObject(new String(responseBody));

						int retcode = response.getInt(RETCODE);
						String retmsg = response.getString(RETMSG);

						if (retcode == STATUS_CODE_SUCCESS) {
							if (delegate != null)
								delegate.onShareGiftCardResult(STATUS_CODE_SUCCESS, context.getString(R.string.success));
						} else {
							if (delegate != null)
								delegate.onShareGiftCardResult(retcode, retmsg);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						if (delegate != null)
							delegate.onShareGiftCardResult(STATUS_CODE_EXCEPTION, context.getString(R.string.parse_exception));
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					if (delegate != null)
						delegate.onShareGiftCardResult(STATUS_CODE_CONN_ERROR, context.getString(R.string.connection_error));
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}


