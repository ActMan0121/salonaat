package com.wxcomp.controls;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import com.wxcomp.salonaat.R;

import java.util.Calendar;

/**
 * Created by Administrator on 12/13/2016.
 */

public class CustomDateTimePickerDialog extends Dialog {
	private final int MIN_YEAR = 1970;
	private final int MAX_YEAR = 2099;

	private NumberPicker yearPicker = null;
	private NumberPicker monthPicker = null;
	private NumberPicker dayPicker = null;
	private NumberPicker hourPicker = null;
	private NumberPicker minutePicker = null;

	private Button doneButton = null;
	private Button cancelButton = null;


	public static final int PICKER_MODE_YEARMONTH = 0;
	public static final int PICKER_MODE_DATETIME = 1;
	public static final int PICKER_MODE_TIME = 2;
	public static final int PICKER_MODE_YEARMONTHDAY = 3;

	public int year = 2017;
	public int month = 1;
	public int day = 1;
	public int hour = 0;
	public int minute = 0;

	public int pickerMode = PICKER_MODE_TIME;

	public CustomDateTimePickerDialog(Context context) {
		super(context);
	}

	public CustomDateTimePickerDialog(Context context, int themeResId) {
		super(context, themeResId);
	}

	protected CustomDateTimePickerDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.date_time_picker_dialog);


		yearPicker = (NumberPicker)findViewById(R.id.year_picker);
		{
			yearPicker.setMinValue(MIN_YEAR);
			yearPicker.setMaxValue(MAX_YEAR);
			yearPicker.setValue(year);
			yearPicker.setOnValueChangedListener(yearChangedListener);

			if (pickerMode == PICKER_MODE_YEARMONTH || pickerMode == PICKER_MODE_YEARMONTHDAY) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)yearPicker.getLayoutParams();
				params.leftMargin = 60;
				yearPicker.setLayoutParams(params);
			}

			if (pickerMode == PICKER_MODE_TIME)
				yearPicker.setVisibility(View.GONE);
		}

		monthPicker = (NumberPicker)findViewById(R.id.month_picker);
		{
			monthPicker.setMinValue(1);
			monthPicker.setMaxValue(12);
			monthPicker.setValue(month);
			monthPicker.setOnValueChangedListener(monthChangedListener);

			if (pickerMode == PICKER_MODE_YEARMONTH) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)monthPicker.getLayoutParams();
				params.leftMargin = 60;
				params.rightMargin = 60;
				monthPicker.setLayoutParams(params);
			}

			if (pickerMode == PICKER_MODE_TIME)
				monthPicker.setVisibility(View.GONE);
		}

		dayPicker = (NumberPicker)findViewById(R.id.day_picker);
		{
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.MONTH, month - 1);

			int maxDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
			int minDay = calendar.getMinimum(Calendar.DAY_OF_MONTH);

			if (day < minDay)
				day = minDay;
			if (day > maxDay)
				day = maxDay;

			dayPicker.setMinValue(minDay);
			dayPicker.setMaxValue(maxDay);
			dayPicker.setValue(day);
			dayPicker.setOnValueChangedListener(dayChangedListener);

			if (pickerMode == PICKER_MODE_YEARMONTHDAY) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)dayPicker.getLayoutParams();
				params.rightMargin = 60;
				dayPicker.setLayoutParams(params);
			}

			if (pickerMode != PICKER_MODE_DATETIME && pickerMode != PICKER_MODE_YEARMONTHDAY)
				dayPicker.setVisibility(View.GONE);
		}

		hourPicker = (NumberPicker)findViewById(R.id.hour_picker);
		{
			hourPicker.setMinValue(0);
			hourPicker.setMaxValue(23);
			hourPicker.setValue(hour);
			hourPicker.setOnValueChangedListener(hourChangedListener);

			if (pickerMode == PICKER_MODE_TIME) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)hourPicker.getLayoutParams();
				params.leftMargin = 60;
				params.rightMargin = 60;
				hourPicker.setLayoutParams(params);
			}

			if (pickerMode == PICKER_MODE_YEARMONTH || pickerMode == PICKER_MODE_YEARMONTHDAY)
				hourPicker.setVisibility(View.GONE);
		}

		minutePicker = (NumberPicker)findViewById(R.id.minute_picker);
		{
			minutePicker.setMinValue(0);
			minutePicker.setMaxValue(59);
			minutePicker.setValue(minute);
			minutePicker.setOnValueChangedListener(minuteChangedListener);

			if (pickerMode == PICKER_MODE_TIME) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)minutePicker.getLayoutParams();
				params.rightMargin = 60;
				minutePicker.setLayoutParams(params);
			}

			if (pickerMode == PICKER_MODE_YEARMONTH || pickerMode == PICKER_MODE_YEARMONTHDAY)
				minutePicker.setVisibility(View.GONE);
		}


		doneButton = (Button)findViewById(R.id.done_button);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedDone();
			}
		});
		cancelButton = (Button)findViewById(R.id.cancel_button);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedCancel();
			}
		});
	}


	private NumberPicker.OnValueChangeListener yearChangedListener = new NumberPicker.OnValueChangeListener() {
		@Override
		public void onValueChange(NumberPicker numberPicker, int i, int i1) {
			year = numberPicker.getValue();
//			valueChanged();
		}
	};

	private NumberPicker.OnValueChangeListener monthChangedListener = new NumberPicker.OnValueChangeListener() {
		@Override
		public void onValueChange(NumberPicker numberPicker, int i, int i1) {
			month = numberPicker.getValue();

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.MONTH, month - 1);

			int maxDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
			int minDay = calendar.getMinimum(Calendar.DAY_OF_MONTH);

			if (day < minDay)
				day = minDay;
			if (day > maxDay)
				day = maxDay;

			dayPicker.setMinValue(minDay);
			dayPicker.setMaxValue(maxDay);
			dayPicker.setValue(day);
//			valueChanged();
		}
	};

	private NumberPicker.OnValueChangeListener dayChangedListener = new NumberPicker.OnValueChangeListener() {
		@Override
		public void onValueChange(NumberPicker numberPicker, int i, int i1) {
			day = numberPicker.getValue();
//			valueChanged();
		}
	};

	private NumberPicker.OnValueChangeListener hourChangedListener = new NumberPicker.OnValueChangeListener() {
		@Override
		public void onValueChange(NumberPicker numberPicker, int i, int i1) {
			hour = numberPicker.getValue();
//			valueChanged();
		}
	};

	private NumberPicker.OnValueChangeListener minuteChangedListener = new NumberPicker.OnValueChangeListener() {
		@Override
		public void onValueChange(NumberPicker numberPicker, int i, int i1) {
			minute = numberPicker.getValue();
//			valueChanged();
		}
	};

	private void valueChanged() {
		if (dateTimeChangedListener != null)
			dateTimeChangedListener.dateTimeChanged(year, month, day, hour, minute);
	}

	public OnDateTimeChangedListener dateTimeChangedListener = null;
	public interface OnDateTimeChangedListener {
		void dateTimeChanged(int year, int month, int day, int hour, int minute);
	}


	private void onClickedDone() {
		valueChanged();
		CustomDateTimePickerDialog.this.dismiss();
	}


	private void onClickedCancel() {
//		valueChanged();
		CustomDateTimePickerDialog.this.dismiss();
	}
}
