package com.wxcomp.controls;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wxcomp.misc.Global;
import com.wxcomp.salonaat.R;

/**
 * Created by Administrator on 1/10/2017.
 */

public class CustomInputDialog extends Dialog {
	public String email = "";

	public CustomInputDialog(Context context) {
		super(context);
	}

	public CustomInputDialog(Context context, int themeResId) {
		super(context, themeResId);
	}

	protected CustomInputDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dlg_custom_input);

		Button okButton = (Button)findViewById(R.id.btn_done);
		Button cancelButton = (Button)findViewById(R.id.btn_cancel);

		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedOK();
			}
		});
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedCancel();
			}
		});
	}

	private void onClickedOK() {
		EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		String email = emailEdit.getText().toString();
		if (email.isEmpty() || !email.contains("@")) {
			Global.showToast(CustomInputDialog.this.getContext(), getContext().getString(R.string.please_input_valid_email));
			return;
		}

		this.email = email;

		CustomInputDialog.this.dismiss();
	}

	private void onClickedCancel() {
		CustomInputDialog.this.dismiss();
	}


}
