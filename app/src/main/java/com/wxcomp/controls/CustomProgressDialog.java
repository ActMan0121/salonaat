package com.wxcomp.controls;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.wxcomp.salonaat.R;

/**
 * Created by Administrator on 12/25/2016.
 */

public class CustomProgressDialog extends Dialog {
	private String message = "";

	public CustomProgressDialog(Context context) {
		super(context);
	}

	public CustomProgressDialog(Context context, int themeResId) {
		super(context, themeResId);
	}

	protected CustomProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_progress);

		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);

		TextView messageTextView = (TextView)findViewById(R.id.txt_message);
		messageTextView.setText(message);

		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
