package com.wxcomp.controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.wxcomp.salonaat.R;

/**
 * Created by Administrator on 12/12/2016.
 */

public class CustomRatingView extends View {
	public static final int RATING_TYPE_FULL = 1;
	public static final int RATING_TYPE_HALF = 2;
	public static final int RATING_TYPE_FLOAT = 3;

	// Information
	public int max_rating = 5;
	public int min_rating = 0;

	public float rating = 0;

	public int rating_type = RATING_TYPE_FLOAT;
	public boolean editable = true;

	public int full_image_res_id = 0;
	public int empty_image_res_id = 0;

	public Bitmap fullImageBitmap = null, emptyImageBitmap = null;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


	private Context context = null;
	private int star_width = 0, star_margin = 0;
	private int star_height = 0;

	public CustomRatingView(Context context) {
		super(context);
		this.context = context;
	}

	public CustomRatingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public CustomRatingView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.context = context;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (star_width == 0) {
			// Calculate star width
			calculateStarSize();
		}

		if (emptyImageBitmap == null) {
			emptyImageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rating_empty);
		}

		if (fullImageBitmap == null) {
			fullImageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rating_full);
		}

		for (int i = min_rating; i < max_rating; i++) {
			int left = starXPos(i);
			int right = left + star_width;

			int top = getHeight() / 2 - star_height / 2;
			int bottom = top + star_height;

			canvas.drawBitmap(emptyImageBitmap, new Rect(0, 0, emptyImageBitmap.getWidth(), emptyImageBitmap.getHeight()), new Rect(left, top, right, bottom), new Paint());
		}

		for (int i = min_rating; i < (int)rating; i++) {
			int left = starXPos(i);
			int right = left + star_width;

			int top = getHeight() / 2 - star_height / 2;
			int bottom = top + star_height;

			canvas.drawBitmap(fullImageBitmap, new Rect(0, 0, fullImageBitmap.getWidth(), fullImageBitmap.getHeight()), new Rect(left, top, right, bottom), new Paint());
		}

		int ratingInt = (int)rating;
		float ratingDecimal = rating - (int)rating;
		if (ratingDecimal > 0) {
			int left = starXPos(ratingInt);
			int right = (int)(left + star_width * ratingDecimal);

			int top = getHeight() / 2 - star_height / 2;
			int bottom = top + star_height;

			canvas.clipRect(left, top, right, bottom);
			canvas.drawBitmap(fullImageBitmap, new Rect(0, 0, fullImageBitmap.getWidth(), fullImageBitmap.getHeight()), new Rect(left, top, left + star_width, bottom), new Paint());
		}
	}

	private void calculateStarSize() {
		if (max_rating <= 0 || min_rating < 0)
			return;

		if (fullImageBitmap == null) {
			if (full_image_res_id == 0)
				full_image_res_id = R.drawable.rating_full;

			fullImageBitmap = BitmapFactory.decodeResource(context.getResources(), full_image_res_id);
		}
		if (emptyImageBitmap == null) {
			if (empty_image_res_id == 0)
				empty_image_res_id = R.drawable.rating_empty;

			emptyImageBitmap = BitmapFactory.decodeResource(context.getResources(), empty_image_res_id);
		}

		if (fullImageBitmap == null || fullImageBitmap.getWidth() == 0 || fullImageBitmap.getHeight() == 0) {
			star_width = star_margin = 0;
			return;
		}

		int view_width = getWidth();
		int view_height = getHeight();

		int starMaxWidth = (view_width / max_rating) - 20;
		int starMaxHeight = starMaxWidth * fullImageBitmap.getHeight() / fullImageBitmap.getWidth();

		if (starMaxHeight > view_height) {
			star_height = view_height;
		} else {
			star_height = starMaxHeight;
		}

		star_width = star_height * fullImageBitmap.getWidth() / fullImageBitmap.getHeight();
		star_margin = (view_width - star_width * max_rating) / (max_rating - 1);
	}


	private int starXPos(int starIndex) {
		return (star_width + star_margin) * starIndex;
	}

	private int rating2XPos(float rating) {
		if (rating > max_rating)
			rating = max_rating;

		if (rating < min_rating)
			rating = min_rating;

		if (star_width == 0) {
			// Calculate star width
			calculateStarSize();
		}

		int ratingInt = (int)rating;
		float ratingDecimal = rating - ratingInt;

		int starStartPos = starXPos(ratingInt);
		if (ratingDecimal > 0) {
			starStartPos += (star_width + star_margin);
		}

		return (int)(starStartPos + star_width * ratingDecimal);
	}


	private float xPos2Rating(int xPos) {
		float rating = 0;

		if (xPos < 0)
			return min_rating;

		if (xPos > getWidth())
			return max_rating;

		if (star_width == 0) {
			// Calculate star width
			calculateStarSize();
		}

		for (int i = min_rating; i <= max_rating; i++) {
			int xPosStart = starXPos(i);
			int xPosEnd = xPosStart + star_width;
			int nextXPosStart = xPosEnd + star_margin;

			if (xPos >= xPosStart && xPos < xPosEnd) {
				int ratingInt = i;
				float ratingDecimal = (float)(xPos - xPosStart) / star_width;
				rating = ratingInt + ratingDecimal;

				break;
			} else if (xPos >= xPosEnd && xPos < nextXPosStart) {
				rating = i + 1;
				break;
			}
		}

		if (rating_type == RATING_TYPE_HALF) {
			float ratingDecimal = rating - (int)rating;
			if (ratingDecimal < 0.25f)
				rating = (int)rating;
			else if (ratingDecimal < 0.75f)
				rating = (int)rating + 0.5f;
			else
				rating = (int)rating + 1;
		} else if (rating_type == RATING_TYPE_FULL) {
			float ratingDecimal = rating - (int)rating;
			if (ratingDecimal < 0.5f)
				rating = (int)rating;
			else
				rating = (int)rating + 1;
		}

		return rating;
	}



	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!editable)
			return false;

		Log.d("MotionEvent", "" + event.getAction() + "------------------------------------");

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				this.rating = xPos2Rating((int)event.getX());
				if (ratingChangedListener != null)
					ratingChangedListener.onRatingChanged(this.rating);
				this.invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				this.rating = xPos2Rating((int)event.getX());
				if (ratingChangedListener != null)
					ratingChangedListener.onRatingChanged(this.rating);
				this.invalidate();
				break;
		}

		return true;
	}


	public OnRatingChangedListener ratingChangedListener = null;
	public interface OnRatingChangedListener {
		public void onRatingChanged(float rating);
	}

}
