package com.wxcomp.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Administrator on 12/12/2016.
 */

public class CustomSeekbar extends View {
	public boolean editable = true;
	public int max_value = 100;
	public int min_value = 1;
	public int current_value = 50;

	public int barColor = Color.argb(0xFF, 0xC2, 0xB5, 0x60);
	public int progressColor = Color.argb(0xFF, 0xC2, 0xB5, 0x60);

	public int thumbColor = Color.argb(0xFF, 0xC2, 0xB5, 0x60);
	public int thumbSize = 20;			// Diameter
	public int barWidth = 4;


	public CustomSeekbar(Context context) {
		super(context);
	}

	public CustomSeekbar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomSeekbar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (max_value <= 0 || max_value <= min_value)
			return;


		// Draw background bar
		Paint barPaint = new Paint();
		barPaint.setStyle(Paint.Style.FILL);
		barPaint.setColor(barColor);
		canvas.drawRect(0,
				getHeight() / 2 - barWidth / 2,
				getWidth(),
				getHeight() / 2 + barWidth / 2,
				barPaint);


		// Draw progress bar
		Paint progressPaint = new Paint();
		progressPaint.setStyle(Paint.Style.FILL);
		progressPaint.setColor(progressColor);
		canvas.drawRect(0,
				getHeight() / 2 - barWidth / 2,
				getWidth() * (current_value - min_value) / (max_value - min_value),
				getHeight() / 2 + barWidth / 2,
				progressPaint);


		// Draw thumb
		Paint thumbPaint = new Paint();
		thumbPaint.setStyle(Paint.Style.FILL);
		thumbPaint.setColor(thumbColor);
		canvas.drawCircle((getWidth() - thumbSize) * (current_value - min_value) / (max_value - min_value) + thumbSize / 2,
				getHeight() / 2,
				thumbSize / 2,
				thumbPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!editable)
			return false;

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				setValueFromPos(event.getX());
				break;
			case MotionEvent.ACTION_MOVE:
				setValueFromPos(event.getX());
				break;
		}

		return true;
	}


	private void setValueFromPos(float pos) {
		current_value = (int)((pos - thumbSize / 2) * (max_value - min_value + 1) / (getWidth() - thumbSize));
		if (current_value > max_value)
			current_value = max_value;

		if (current_value < min_value)
			current_value = min_value;

		invalidate();

		valueChanged();
	}

	public void valueChanged() {
		if (seekChangedListener != null)
			seekChangedListener.onSeekChanged(current_value);
	}

	public OnSeekChangedListener seekChangedListener = null;
	public interface OnSeekChangedListener {
		void onSeekChanged(int value);
	}

}



