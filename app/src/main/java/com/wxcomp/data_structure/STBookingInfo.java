package com.wxcomp.data_structure;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STBookingInfo {
	public String uid = "";
	public String bookTime = "";
	public String serviceTime = "";
	public String salon_uid = "";
	public String salon_title = "";
	public int state = 0;
	public int price = 0;
	public String service_name = "";


	public static STBookingInfo decodeFromJSON(JSONObject jsonObject) {
		STBookingInfo result = new STBookingInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.bookTime = jsonObject.getString("bookTime"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.serviceTime = jsonObject.getString("serviceTime"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.salon_uid = jsonObject.getString("salon_uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.salon_title = jsonObject.getString("salon_title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.state = jsonObject.getInt("state"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.price = jsonObject.getInt("price ="); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.service_name = jsonObject.getString("service_name"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("bookTime", bookTime);
			result.put("serviceTime", serviceTime);
			result.put("salon_uid", salon_uid);
			result.put("salon_title", salon_title);
			result.put("state", state);
			result.put("price", price);
			result.put("service_name", service_name);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
