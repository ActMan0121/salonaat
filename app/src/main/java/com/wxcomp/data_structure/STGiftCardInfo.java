package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STGiftCardInfo {
	public String uid = "";
	public String title = "";
	public String image_url = "";
	public int price = 0;
	public boolean selected = false;

	public static STGiftCardInfo decodeFromJSON(JSONObject jsonObject) {
		STGiftCardInfo result = new STGiftCardInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.image_url = jsonObject.getString("image_url"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.price = jsonObject.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.selected = jsonObject.getBoolean("selected"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("title", title);
			result.put("image_url", image_url);
			result.put("price", price);
			result.put("selected", selected);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

}
