package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STLiveFeedbackInfo {
	public String uid = "";
	public String user_image_url = "";
	public String type = "";
	public String contents = "";
	public String title = "";
	public float rating = 0;


	public static STLiveFeedbackInfo decodeFromJSON(JSONObject jsonObject) {
		STLiveFeedbackInfo result = new STLiveFeedbackInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.user_image_url = jsonObject.getString("user_image_url"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.type = jsonObject.getString("type"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.rating = (float)jsonObject.getDouble("rating"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("user_image_url", user_image_url);
			result.put("type", type);
			result.put("title", title);
			result.put("contents", contents);
			result.put("rating", rating);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

}
