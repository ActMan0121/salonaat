package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STLoyaltyPointInfo {
	public String			uid = "";
	public String			title = "";
	public String			remark = "";
	public int				loyalty_points = 0;
	public int				state = 0;					// 0 : New      1 : Redeemed

	public static STLoyaltyPointInfo decodeFromJSON(JSONObject jsonObject) {
		STLoyaltyPointInfo pointInfo = new STLoyaltyPointInfo();

		try { pointInfo.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { pointInfo.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { pointInfo.remark = jsonObject.getString("remark"); } catch (Exception ex) { ex.printStackTrace(); }
		try { pointInfo.loyalty_points = jsonObject.getInt("loyalty_points"); } catch (Exception ex) { ex.printStackTrace(); }
		try { pointInfo.state = jsonObject.getInt("state"); } catch (Exception ex) { ex.printStackTrace(); }

		return pointInfo;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("title", title);
			result.put("remark", remark);
			result.put("loyalty_points", loyalty_points);
			result.put("state", state);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}


}
