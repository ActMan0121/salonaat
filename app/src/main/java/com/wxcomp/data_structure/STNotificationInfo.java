package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STNotificationInfo {
	public String uid = "";
	public String title = "";
	public String contents = "";
	public int new_flag = 0;

	public static STNotificationInfo decodeFromJSON(JSONObject jsonObject) {
		STNotificationInfo result = new STNotificationInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.new_flag = jsonObject.getInt("new_flag"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}

	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("title", title);
			result.put("contents", contents);
			result.put("new_flag", new_flag);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
