package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STPromotionInfo {
	public String uid = "";
	public String imageUrl = "";
	public String title = "";
	public String contents = "";
	public String start_date = "";
	public String end_date = "";
	public int percent = 0;
	public String salon_name = "";


	public static STPromotionInfo decodeFromJSON(JSONObject jsonObject) {
		STPromotionInfo result = new STPromotionInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.imageUrl = jsonObject.getString("imageUrl"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.percent = jsonObject.getInt("percent"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.start_date = jsonObject.getString("start_date"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.end_date = jsonObject.getString("end_date"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.salon_name = jsonObject.getString("salon_name"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("imageUrl", imageUrl);
			result.put("title", title);
			result.put("contents", contents);
			result.put("percent", percent);
			result.put("start_date", start_date);
			result.put("end_date", end_date);
			result.put("salon_name", salon_name);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
