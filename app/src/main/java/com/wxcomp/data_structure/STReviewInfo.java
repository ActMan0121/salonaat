package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STReviewInfo {
	public String uid = "";
	public String contents = "";
	public String user_id = "";
	public float rating = 0;


	public static STReviewInfo decodeFromJSON(JSONObject jsonObject) {
		STReviewInfo reviewInfo = new STReviewInfo();

		try { reviewInfo.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { reviewInfo.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { reviewInfo.user_id = jsonObject.getString("user_id"); } catch (Exception ex) { ex.printStackTrace(); }
		try { reviewInfo.rating = (float)jsonObject.getDouble("rating"); } catch (Exception ex) { ex.printStackTrace(); }

		return reviewInfo;
	}

	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("contents", contents);
			result.put("user_id", user_id);
			result.put("rating", rating);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

}
