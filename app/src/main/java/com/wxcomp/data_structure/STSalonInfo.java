package com.wxcomp.data_structure;

import com.wxcomp.misc.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STSalonInfo {
	public String uid = "";
	public String imageUrl = "";
	public String title = "";
	public String contents = "";
	public boolean favorite = false;
	public int price = 0;
	public int gender = Const.GENDER_MALE;

	public int startHour = 0;
	public int startMinute = 0;
	public int endHour = 23;
	public int endMinute = 59;

	public float rating = 0;

	public String address = "";
	public String phone = "";
	public double latitude = 0;
	public double longitude = 0;

	public ArrayList<STServiceInfo> servicesArray = new ArrayList<>();
	public ArrayList<String> photosArray = new ArrayList<>();
	public ArrayList<STReviewInfo> reviewsArray = new ArrayList<>();


	public static STSalonInfo decodeFromJSON(JSONObject jsonObject) {
		STSalonInfo result = new STSalonInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.imageUrl = jsonObject.getString("imageUrl"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.price = jsonObject.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.gender = jsonObject.getInt("gender"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.favorite = jsonObject.getBoolean("favorite"); } catch (Exception ex) { ex.printStackTrace(); }

		try { result.startHour = jsonObject.getInt("startHour"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.startMinute = jsonObject.getInt("startMinute"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.endHour = jsonObject.getInt("endHour"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.endMinute = jsonObject.getInt("endMinute"); } catch (Exception ex) { ex.printStackTrace(); }

		try { result.rating = (float)jsonObject.getDouble("rating"); } catch (Exception ex) { ex.printStackTrace(); }

		try { result.address = jsonObject.getString("address"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.phone = jsonObject.getString("phone"); } catch (Exception ex) { ex.printStackTrace(); }

		try { result.latitude = jsonObject.getDouble("latitude"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.longitude = jsonObject.getDouble("longitude"); } catch (Exception ex) { ex.printStackTrace(); }

		try {
			JSONArray itemsArray = jsonObject.getJSONArray("services");
			for (int i = 0; i < itemsArray.length(); i++) {
				JSONObject item = itemsArray.getJSONObject(i);
				result.servicesArray.add(STServiceInfo.decodeFromJSON(item));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			JSONArray itemsArray = jsonObject.getJSONArray("photos");
			for (int i = 0; i < itemsArray.length(); i++) {
				String item = itemsArray.getString(i);
				result.photosArray.add(item);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			JSONArray itemsArray = jsonObject.getJSONArray("reviews");
			for (int i = 0; i < itemsArray.length(); i++) {
				JSONObject item = itemsArray.getJSONObject(i);
				result.reviewsArray.add(STReviewInfo.decodeFromJSON(item));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("imageUrl", imageUrl);
			result.put("title", title);
			result.put("contents", contents);
			result.put("price", price);
			result.put("gender", gender);
			result.put("favorite", favorite);
			result.put("startHour", startHour);
			result.put("startMinute", startMinute);
			result.put("endHour", endHour);
			result.put("endMinute", endMinute);
			result.put("rating", rating);
			result.put("address", address);
			result.put("phone", phone);
			result.put("latitude", latitude);
			result.put("longitude", longitude);

			JSONArray itemArray = new JSONArray();
			for (int i = 0; i < servicesArray.size(); i++) {
				STServiceInfo item = servicesArray.get(i);
				itemArray.put(item.encodeToJSON());
			}
			result.put("services", itemArray);

			JSONArray itemArray3 = new JSONArray();
			for (int i = 0; i < photosArray.size(); i++) {
				itemArray3.put(photosArray.get(i));
			}
			result.put("photos", itemArray3);

			JSONArray itemArray2 = new JSONArray();
			for (int i = 0; i < reviewsArray.size(); i++) {
				STReviewInfo item = reviewsArray.get(i);
				itemArray2.put(item.encodeToJSON());
			}
			result.put("reviews", itemArray2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

}
