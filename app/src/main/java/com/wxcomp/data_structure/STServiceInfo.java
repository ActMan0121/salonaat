package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STServiceInfo {
	public String uid = "";
	public String title = "";
	public String contents = "";
	public int price = 0;
	public boolean checked = false;

	public static STServiceInfo decodeFromJSON(JSONObject jsonObject) {
		STServiceInfo result = new STServiceInfo();

		try { result.uid = jsonObject.getString("uid"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("title"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.contents = jsonObject.getString("contents"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.price = jsonObject.getInt("price"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.checked = jsonObject.getBoolean("checked"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("uid", uid);
			result.put("title", title);
			result.put("contents", contents);
			result.put("price", price);
			result.put("checked", checked);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
