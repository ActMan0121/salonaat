package com.wxcomp.data_structure;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/12/2016.
 */

public class STSubCategoryInfo {
	public String uid = "";
	public String title = "";
	public int service_count = 0;
	public String image_url = "";

	public static STSubCategoryInfo decodeFromJSON(JSONObject jsonObject) {
		STSubCategoryInfo result = new STSubCategoryInfo();

		try { result.uid = jsonObject.getString("id"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.title = jsonObject.getString("name"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.service_count = jsonObject.getInt("count"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.image_url = jsonObject.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}

	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try {
			result.put("id", uid);
			result.put("name", title);
			result.put("count", service_count);
			result.put("photo", image_url);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
