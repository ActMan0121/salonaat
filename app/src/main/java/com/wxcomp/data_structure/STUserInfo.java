package com.wxcomp.data_structure;

import com.wxcomp.misc.Const;

import org.json.JSONObject;

/**
 * Created by Administrator on 1/10/2017.
 */

public class STUserInfo {
	public String uid = "";
	public String first_name = "";
	public String last_name = "";
	public String middle_name = "";
	public String image_url = "";
	public String email = "";
	public String mobile = "";
	public int gender = Const.GENDER_MALE;
	public String birthday = "";
	public String location = "";
	public String desc = "";

	public static STUserInfo decodeFromJSON(JSONObject jsonObject) {
		STUserInfo result = new STUserInfo();

		try { result.uid = jsonObject.getString("_id"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.first_name = jsonObject.getString("first_name"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.last_name = jsonObject.getString("last_name"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.middle_name = jsonObject.getString("middle_name"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.image_url = jsonObject.getString("photo"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.email = jsonObject.getString("email"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.location = jsonObject.getString("location"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.gender = jsonObject.getInt("gender"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.birthday = jsonObject.getString("birthday"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.desc = jsonObject.getString("description"); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.mobile = jsonObject.getString("mobile"); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


	public JSONObject encodeToJSON() {
		JSONObject result = new JSONObject();

		try { result.put("_id", uid); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("first_name", first_name); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("last_name", last_name); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("middle_name", middle_name); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("photo", image_url); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("email", email); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("gender", gender); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("birthday", birthday); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("location", location); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("desc", desc); } catch (Exception ex) { ex.printStackTrace(); }
		try { result.put("mobile", mobile); } catch (Exception ex) { ex.printStackTrace(); }

		return result;
	}


}
