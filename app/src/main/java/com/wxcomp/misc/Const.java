package com.wxcomp.misc;

/**
 * Created by Administrator on 12/11/2016.
 */

public class Const {
	public static final int ACCOUNT_TYPE_PERSONAL			= 0;
	public static final int ACCOUNT_TYPE_BUSINESS			= 1;

	public static final int LANGUAGE_TYPE_ENGLISH			= 1;
	public static final int LANGUAGE_TYPE_ARABIC			= 2;

	public static final int GENDER_NONE						= -1;
	public static final int GENDER_MALE						= 0;
	public static final int GENDER_FEMALE					= 1;
	public static final int GENDER_UNISEX					= 2;

	public static final int SERVICE_CATEGORY_HAIR			= 1;
	public static final int SERVICE_CATEGORY_FACE			= 2;
	public static final int SERVICE_CATEGORY_COSMETICS		= 3;
	public static final int SERVICE_CATEGORY_BODY			= 4;

	public static final int SERVICE_TYPE_SPA				= 1;
	public static final int SERVICE_TYPE_SALON				= 2;
	public static final int SERVICE_TYPE_SPECIALIST			= 3;

	public static final int BOOKING_STATE_SCHEDULED			= 1;
	public static final int BOOKING_STATE_FINISHED			= 2;
	public static final int BOOKING_STATE_CANCELLED			= 3;

	public static final int PROMOTION_TYPE_20				= 0;
	public static final int PROMOTION_TYPE_40				= 1;
	public static final int PROMOTION_TYPE_60				= 2;

	public static final int SIGN_TYPE_NORMAL				= 1;
	public static final int SIGN_TYPE_FACEBOOK				= 2;
	public static final int SIGN_TYPE_GOOGLE				= 3;

}
