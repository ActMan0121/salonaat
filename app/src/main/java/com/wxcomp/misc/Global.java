package com.wxcomp.misc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wxcomp.salonaat.R;
import com.wxcomp.salonaat.SuperActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

/**
 * Created by Administrator on 12/11/2016.
 */

public class Global {

	private static String sharedPreferencesName() { return "salonaat_pref"; }

	public static void showToast(Context context, String message) {
		View contentView = SuperActivity.sharedInstance.getLayoutInflater().inflate(R.layout.custom_toast, null);
		TextView contentTextView = (TextView)contentView.findViewById(R.id.txt_contents);
		contentTextView.setText(message);

		Toast toast = new Toast(context);
		toast.setView(contentView);

		toast.setDuration(Toast.LENGTH_LONG);

		toast.show();
	}


	public static void showToast(Context context, int str_id) {
		String message = context.getResources().getString(str_id);
		showToast(context, message);
	}


	public static void saveGender(int gender) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("gender", gender);
		editor.commit();
	}

	public static int loadGender() {
		int result;

		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		result = sharedPref.getInt("gender", Const.GENDER_MALE);

		return result;
	}


	public static int dp2px(Context context, int dp) {
		DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
		int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displaymetrics);

		return pixels;
	}


	public static void saveCondition_Distance(int distance) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("cond_distance", distance);
		editor.commit();
	}

	public static int loadCondition_Distance() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		int result = sharedPref.getInt("cond_distance", 4);
		return result;
	}


	public static void saveCondition_Likes(float likes) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putFloat("cond_likes", likes);
		editor.commit();
	}

	public static float loadCondition_Likes() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		float result = sharedPref.getFloat("cond_likes", 0);
		return result;
	}


	public static void saveCondition_Price(int price) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("cond_price", price);
		editor.commit();
	}

	public static int loadCondition_Price() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		int result = sharedPref.getInt("cond_price", 5);
		return result;
	}


	public static void saveCondition_Ratings(int ratings) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("cond_ratings", ratings);
		editor.commit();
	}

	public static int loadCondition_Ratings() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		int result = sharedPref.getInt("cond_ratings", 0);
		return result;
	}

	public static void clearConditions() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.remove("cond_distance");
		editor.remove("cond_likes");
		editor.remove("cond_price");
		editor.remove("cond_ratings");
		editor.commit();
	}


	public static void saveCardHolderName(String name) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("card_holder_name", name);
		editor.commit();
	}

	public static String loadCardHolderName() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("card_holder_name", "");
	}


	public static void saveCardNumber(String number) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("card_number", number);
		editor.commit();
	}

	public static String loadCardNumber() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("card_number", "");
	}


	public static void saveExpirationMonth(int month) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("expiration_month", month);
		editor.commit();
	}

	public static int loadExpirationMonth() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getInt("expiration_month", 1);
	}


	public static void saveExpirationYear(int year) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("expiration_year", year);
		editor.commit();
	}

	public static int loadExpirationYear() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getInt("expiration_year", Calendar.getInstance().get(Calendar.YEAR) + 1);
	}


	public static void saveCVV(String cvv) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("cvv", cvv);
		editor.commit();
	}

	public static String loadCVV() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("cvv", "");
	}


	public static void showAlertWithOK(Context context, String title, String message) {
		AlertDialog alertDialog = new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton("OK", null)
				.create();
		alertDialog.show();
	}


	public static void saveEmail(String email) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("email", email);
		editor.commit();
	}

	public static String loadEmail() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("email", "");
	}


	public static void savePassword(String password) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("password", password);
		editor.commit();
	}

	public static String loadPassword() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("password", "");
	}


	public static void saveStartFlag(boolean flag) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("start_flag", flag);
		editor.commit();
	}

	public static boolean loadStartFlag() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getBoolean("start_flag", false);
	}


	public static void saveUserID(String userid) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("userid", userid);
		editor.commit();
	}

	public static String loadUserID() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("userid", "");
	}


	public static void saveAccountType(int accountType) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("account_Type", accountType);
		editor.commit();
	}

	public static int loadAccountType() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getInt("account_Type", Const.ACCOUNT_TYPE_PERSONAL);
	}

	public static void saveLanguage(int language) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("language", language);
		editor.commit();
	}

	public static int loadLanguage() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getInt("language", Const.LANGUAGE_TYPE_ENGLISH);
	}

	public static void saveAuthToken(String token) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("token", token);
		editor.commit();
	}

	public static String loadAuthToken() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getString("token", "");
	}




	// Locale management
	public static void setLanguage(Context context, String language) {
		Locale locale = new Locale(language);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}


	public static String formatString(String format, Object... args) {
		String result = "";

		Formatter formatter = new Formatter(Locale.US);
		result = result + formatter.format(format, args).out();

		return result;
	}


	public static void openDialPad(Context context, String phoneNum) {
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + phoneNum));
		context.startActivity(intent);
	}

	public static int getDistanceInMeters(double lat1, double lng1, double lat2, double lng2) {
		int result = 0;
		return result;
	}


	public static void saveCurrentLatitude(double latitude) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putFloat("latitude", (float)latitude);
		editor.commit();
	}

	public static double loadCurrentLatitude() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return (double)sharedPref.getFloat("latitude", 41);
	}


	public static void saveCurrentLongitude(double longitude) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putFloat("longitude", (float)longitude);
		editor.commit();
	}

	public static double loadCurrentLongitude() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return (double)sharedPref.getFloat("longitude", 123);
	}


	public static void saveSignType(int signtype) {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("sign_type", signtype);
		editor.commit();
	}

	public static int loadSignType() {
		SharedPreferences sharedPref = SuperActivity.sharedInstance.getApplicationContext().getSharedPreferences(sharedPreferencesName(), Context.MODE_PRIVATE);
		return sharedPref.getInt("sign_type", Const.SIGN_TYPE_NORMAL);
	}


	public static Date string2Date(String dateValue, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date result = new Date();

		try {
			result = sdf.parse(dateValue);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	public static String date2String(Date dateValue, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = "";

		try {
			result = sdf.format(dateValue);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}
}
