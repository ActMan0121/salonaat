package com.wxcomp.salonaat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.data_structure.STReviewInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.misc.Global;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/13/2016.
 */

public class AddReviewActivity extends LogoTitleActivity {
	public static final int REQUEST_CODE_ADD_REVIEW = 1;
	public static final String EXTRA_SALON_INFO = "SalonInfo";
	public static final String EXTRA_REVIEW_INFO = "ReviewInfo";

	private STSalonInfo salonInfo = null;

	private CustomRatingView ratingView = null;
	private Button saveButton = null;
	private EditText contentsEdit = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_add_review);
	}

	@Override
	public void initializeActivity() {
		String salonString = getIntent().getStringExtra(EXTRA_SALON_INFO);
		try {
			salonInfo = STSalonInfo.decodeFromJSON(new JSONObject(salonString));
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		ratingView = (CustomRatingView)findViewById(R.id.rating_view);
		ratingView.rating_type = CustomRatingView.RATING_TYPE_FULL;
		ratingView.rating = 5;

		contentsEdit = (EditText)findViewById(R.id.edt_contents);

		saveButton = (Button)findViewById(R.id.btn_save);
		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSave();
			}
		});
	}


	private void onClickedSave() {
		showProgress();
		CommManager.reviewSalon(AddReviewActivity.this, salonInfo.uid, ratingView.rating, contentsEdit.getText().toString(), new CommDelegate() {
			@Override
			public void onReviewSalonResult(int statusCode, String statusDescription) {
				super.onReviewSalonResult(statusCode, statusDescription);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(AddReviewActivity.this, "Thank you for your time and consideration");

					STReviewInfo reviewInfo = new STReviewInfo();
					reviewInfo.user_id = Global.loadUserID();
					reviewInfo.contents = contentsEdit.getText().toString();
					reviewInfo.rating = ratingView.rating;

					Intent intent = new Intent();
					intent.putExtra(EXTRA_REVIEW_INFO, reviewInfo.encodeToJSON().toString());

					contentsEdit.setText("");

					setResult(RESULT_OK, intent);
					finishFromLeft();
				} else {
					Global.showToast(AddReviewActivity.this, statusDescription);
				}
			}
		});
	}

}
