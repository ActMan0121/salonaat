package com.wxcomp.salonaat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STBookingInfo;
import com.wxcomp.data_structure.STServiceInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/14/2016.
 */

public class BookingHistoryActivity extends SuperActivity {
	private int tab_index = 1;

	private Button servedTabButton = null;
	private Button scheduledTabButton = null;

	private RelativeLayout servedLayout = null;
	private RelativeLayout scheduledLayout = null;

	private XListView servedListView = null;
	private XListView scheduledListView = null;

	private ArrayList<STBookingInfo> servedBookingsArray = new ArrayList<>();
	private ArrayList<STBookingInfo> scheduledBookingsArray = new ArrayList<>();

	private ServedDataAdapter servedDataAdapter = null;
	private ScheduledDataAdapter scheduledDataAdapter = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_booking_history);
	}

	@Override
	public void initializeActivity() {
		servedTabButton = (Button)findViewById(R.id.btn_served);
		servedTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 0;
				updateTabSelection();
			}
		});

		scheduledTabButton = (Button)findViewById(R.id.btn_scheduled);
		scheduledTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 1;
				updateTabSelection();
			}
		});

		servedLayout = (RelativeLayout)findViewById(R.id.served_layout);
		scheduledLayout = (RelativeLayout)findViewById(R.id.scheduled_layout);


		servedDataAdapter = new ServedDataAdapter(BookingHistoryActivity.this, servedBookingsArray);
		servedListView = (XListView)findViewById(R.id.served_listview);
		{
			servedListView.setAdapter(servedDataAdapter);
			servedListView.setXListViewListener(new XListView.IXListViewListener() {
				@Override
				public void onRefresh() {
					servedBookingsArray.clear();
					onServedListRefreshed();
				}
				@Override
				public void onLoadMore() {
					onServedListRefreshed();
				}
			});
			servedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
					onServedItemSelected(i - 1);
				}
			});
			servedListView.setPullLoadEnable(true);
		}

		scheduledDataAdapter = new ScheduledDataAdapter(BookingHistoryActivity.this, scheduledBookingsArray);
		scheduledListView = (XListView)findViewById(R.id.scheduled_listview);
		{
			scheduledListView.setAdapter(scheduledDataAdapter);
			scheduledListView.setXListViewListener(new XListView.IXListViewListener() {
				@Override
				public void onRefresh() {
					scheduledBookingsArray.clear();
					onScheduledListRefreshed();
				}
				@Override
				public void onLoadMore() {
					onScheduledListRefreshed();
				}
			});
			scheduledListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
					onScheduledItemSelected(i - 1);
				}
			});
			scheduledListView.setPullLoadEnable(true);
		}

		updateTabSelection();

		onScheduledListRefreshed();
		onServedListRefreshed();
	}


	private void updateTabSelection() {
		servedLayout.setVisibility(View.INVISIBLE);
		scheduledLayout.setVisibility(View.INVISIBLE);

		servedTabButton.setBackgroundColor(Color.TRANSPARENT);
		scheduledTabButton.setBackgroundColor(Color.TRANSPARENT);

		if (tab_index == 0) {
			servedLayout.setVisibility(View.VISIBLE);
			servedTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		} else {
			scheduledLayout.setVisibility(View.VISIBLE);
			scheduledTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}
	}



	public class ServedViewHolder {
		public TextView title_textview = null;
		public TextView time_textview = null;
		public TextView price_textview = null;
	}

	public class ServedDataAdapter extends ArrayAdapter {
		public ServedDataAdapter(Context context, List<STBookingInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ServedViewHolder holder = null;
			STBookingInfo itemInfo = servedBookingsArray.get(position);

			if (convertView == null) {
				holder = new ServedViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_served_booking_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.time_textview = (TextView)itemView.findViewById(R.id.txt_working_time);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ServedViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.salon_title);
			holder.time_textview.setText(itemInfo.serviceTime);
			holder.price_textview.setText("AED\n" + itemInfo.price);

			return convertView;
		}
	}


	private void onServedListRefreshed() {
		if (servedBookingsArray.size() == 0)
			showProgress();

		CommManager.getServedBookingHistory(BookingHistoryActivity.this, servedBookingsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetServedBookingListResult(int statusCode, String statusDescription, ArrayList<STBookingInfo> bookingList) {
				super.onGetServedBookingListResult(statusCode, statusDescription, bookingList);

				stopProgress();
				servedListView.stopRefresh();
				servedListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < bookingList.size(); i++) {
						STBookingInfo bookingInfo = bookingList.get(i);
						boolean isExist = false;
						for (int j = 0; j < servedBookingsArray.size(); j++) {
							STBookingInfo oldInfo = servedBookingsArray.get(j);
							if (bookingInfo.uid.equals(oldInfo.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						servedBookingsArray.add(bookingInfo);
					}

					servedDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onServedItemSelected(int index) {
		if (index < 0 || index >= servedBookingsArray.size())
			return;

		STBookingInfo servedItem = servedBookingsArray.get(index);
	}



	public class ScheduledViewHolder {
		public TextView title_textview = null;
		public TextView time_textview = null;
		public TextView price_textview = null;
		public Button doneButton = null;
		public Button cancelButton = null;
	}

	public class ScheduledDataAdapter extends ArrayAdapter {
		public ScheduledDataAdapter(Context context, List<STBookingInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ScheduledViewHolder holder = null;
			STBookingInfo itemInfo = scheduledBookingsArray.get(position);

			if (convertView == null) {
				holder = new ScheduledViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_scheduled_booking_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.time_textview = (TextView)itemView.findViewById(R.id.txt_working_time);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);
				holder.doneButton = (Button)itemView.findViewById(R.id.btn_done);
				holder.doneButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						onClickedDone((Integer)view.getTag());
					}
				});
				holder.cancelButton = (Button)itemView.findViewById(R.id.btn_cancel);
				holder.cancelButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						onClickedCancel((Integer)view.getTag());
					}
				});

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ScheduledViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.salon_title);
			holder.time_textview.setText(itemInfo.serviceTime);
			holder.price_textview.setText("AED\n" + itemInfo.price);
			holder.doneButton.setTag(position);
			holder.cancelButton.setTag(position);

			return convertView;
		}
	}


	private void onScheduledListRefreshed() {
		if (scheduledBookingsArray.size() == 0)
			showProgress();

		CommManager.getScheduledBookingHistory(BookingHistoryActivity.this, scheduledBookingsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetScheduledBookingListResult(int statusCode, String statusDescription, ArrayList<STBookingInfo> bookingList) {
				super.onGetScheduledBookingListResult(statusCode, statusDescription, bookingList);

				stopProgress();
				scheduledListView.stopRefresh();
				scheduledListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < bookingList.size(); i++) {
						STBookingInfo bookingInfo = bookingList.get(i);
						boolean isExist = false;
						for (int j = 0; j < scheduledBookingsArray.size(); j++) {
							STBookingInfo oldInfo = scheduledBookingsArray.get(j);
							if (bookingInfo.uid.equals(oldInfo.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						scheduledBookingsArray.add(bookingInfo);
					}

					scheduledDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onScheduledItemSelected(int index) {
		if (index < 0 || index >= scheduledBookingsArray.size())
			return;

		STBookingInfo scheduledItem = scheduledBookingsArray.get(index);
	}


	private void onClickedDone(final Integer index) {
		AlertDialog alertDialog = new AlertDialog.Builder(BookingHistoryActivity.this)
				.setTitle("Note")
				.setMessage("Are you sure to confirm this booking done?")
				.setNegativeButton("Cancel", null)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						STBookingInfo bookingInfo = scheduledBookingsArray.get(index);

						showProgress();
						CommManager.confirmBookingInfoDetail(BookingHistoryActivity.this, bookingInfo.uid, new CommDelegate() {
							@Override
							public void onConfirmBookingResult(int statusCode, String statusDescription) {
								super.onConfirmBookingResult(statusCode, statusDescription);
								stopProgress();
								if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
									Global.showToast(BookingHistoryActivity.this, "Confirmed successfully");

									scheduledBookingsArray.clear();
									servedBookingsArray.clear();

									onScheduledListRefreshed();
									onServedListRefreshed();
								}
							}
						});
					}
				})
				.create();
		alertDialog.show();
	}

	private void onClickedCancel(final Integer index) {
		AlertDialog alertDialog = new AlertDialog.Builder(BookingHistoryActivity.this)
				.setTitle("Note")
				.setMessage("Are you sure to cancel this booking?")
				.setNegativeButton("Cancel", null)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						STBookingInfo bookingInfo = scheduledBookingsArray.get(index);

						showProgress();
						CommManager.cancelBookingInfoDetail(BookingHistoryActivity.this, bookingInfo.uid, new CommDelegate() {
							@Override
							public void onCancelBookingResult(int statusCode, String statusDescription) {
								super.onCancelBookingResult(statusCode, statusDescription);

								stopProgress();
								if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
									Global.showToast(BookingHistoryActivity.this, "Cancelled successfully");

									scheduledBookingsArray.clear();
									onScheduledListRefreshed();
								}
							}
						});
					}
				})
				.create();
		alertDialog.show();
	}
}
