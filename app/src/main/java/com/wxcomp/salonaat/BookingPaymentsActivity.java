package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wxcomp.controls.CustomDateTimePickerDialog;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.misc.Global;

import java.util.Calendar;

/**
 * Created by Administrator on 12/13/2016.
 */

public class BookingPaymentsActivity extends SuperActivity {
	public static final String EXTRA_SALON_INFO = "SalonInfo";

	private STSalonInfo salonInfo = null;

	private String[] monthArray = null;

	private int expirationMonth = 0;
	private int expirationYear = 0;


	private EditText cardHolderEdit = null;
	private EditText cardNumberEdit = null;

	private TextView monthTextView = null;
	private Button monthButton = null;

	private TextView yearTextView = null;
	private Button yearButton = null;

	private EditText cvvEdit = null;

	private Button cancelButton = null;
	private Button saveButton = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_booking_payments);
	}

	@Override
	public void initializeActivity() {
		monthArray = new String[] {getString(R.string.month_1),
				getString(R.string.month_2),
				getString(R.string.month_3),
				getString(R.string.month_4),
				getString(R.string.month_5),
				getString(R.string.month_6),
				getString(R.string.month_7),
				getString(R.string.month_8),
				getString(R.string.month_9),
				getString(R.string.month_10),
				getString(R.string.month_11),
				getString(R.string.month_12)};

		cardHolderEdit = (EditText)findViewById(R.id.edt_card_holder_name);
		cardHolderEdit.setText(Global.loadCardHolderName());

		cardNumberEdit = (EditText)findViewById(R.id.edt_card_number);
		cardNumberEdit.setText(Global.loadCardNumber());

		monthTextView = (TextView)findViewById(R.id.txt_month);
		monthButton = (Button)findViewById(R.id.btn_month);
		monthButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMonth();
			}
		});

		yearTextView = (TextView)findViewById(R.id.txt_year);
		yearButton = (Button)findViewById(R.id.btn_year);
		yearButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedYear();
			}
		});

		cvvEdit = (EditText)findViewById(R.id.edt_cvv);
		cvvEdit.setText(Global.loadCVV());

		cancelButton = (Button)findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedCancel();
			}
		});
		saveButton = (Button)findViewById(R.id.btn_save);
		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSave();
			}
		});


		expirationMonth = Global.loadExpirationMonth();
		expirationYear = Global.loadExpirationYear();

		updateExpirationDates();
	}

	private void onClickedMonth() {
		CustomDateTimePickerDialog dialog = new CustomDateTimePickerDialog(BookingPaymentsActivity.this);

		dialog.month = expirationMonth;
		dialog.year = expirationYear;
		dialog.pickerMode = CustomDateTimePickerDialog.PICKER_MODE_YEARMONTH;
		dialog.dateTimeChangedListener = dateTimeChangedListener;

		dialog.show();
	}

	private void onClickedYear() {
		onClickedMonth();
	}

	private void onClickedCancel() {
		finishFromLeft();
	}

	private void onClickedSave() {
		if (cardHolderEdit.getText().toString().isEmpty()) {
			Global.showToast(BookingPaymentsActivity.this, getString(R.string.booking_payments_activity_input_name));
			return;
		}

		if (cardNumberEdit.getText().toString().isEmpty()) {
			Global.showToast(BookingPaymentsActivity.this, getString(R.string.booking_payments_activity_input_card_number));
			return;
		}

		if (cvvEdit.getText().toString().isEmpty()) {
			Global.showToast(BookingPaymentsActivity.this, getString(R.string.booking_payments_activity_input_cvv));
			return;
		}


		Global.saveCardHolderName(cardHolderEdit.getText().toString());
		Global.saveCardNumber(cardNumberEdit.getText().toString());
		Global.saveExpirationMonth(expirationMonth);
		Global.saveExpirationYear(expirationYear);
		Global.saveCVV(cvvEdit.getText().toString());

		// Go to booking detail activity
		Bundle bundle = getIntent().getExtras();
		pushNewActivityAnimatedFromRight(BookingSummaryActivity.class, bundle);
	}


	private CustomDateTimePickerDialog.OnDateTimeChangedListener dateTimeChangedListener = new CustomDateTimePickerDialog.OnDateTimeChangedListener() {
		@Override
		public void dateTimeChanged(int year, int month, int day, int hour, int minute) {
			expirationYear = year;
			expirationMonth = month;

			updateExpirationDates();
		}
	};


	private void updateExpirationDates() {
		monthTextView.setText(monthArray[expirationMonth - 1]);
		yearTextView.setText("" + expirationYear);
	}


}
