package com.wxcomp.salonaat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.payfort.start.Card;
import com.payfort.start.Start;
import com.payfort.start.Token;
import com.payfort.start.TokenCallback;
import com.payfort.start.error.StartApiException;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomDateTimePickerDialog;
import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.data_structure.STGiftCardInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.data_structure.STServiceInfo;
import com.wxcomp.misc.Global;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 12/13/2016.
 */

public class BookingSummaryActivity extends SuperActivity {
	private final int REQUEST_CODE_SELECT_SERVICES = 1;
	private final int REQUEST_CODE_SELECT_LOYALTYPOINTS = 2;

	public static final String EXTRA_SALON_INFO = "SalonInfo";
	private STSalonInfo salonInfo = null;
	private int totalPrice = 0;
	private int loyaltyPoints = 0;
	private ArrayList<STGiftCardInfo> giftCardArray = new ArrayList<>();

	private String[] weekdays = null;
	private String[] monthArray = null;

	private ImageView salonInfoImageView = null;
	private TextView salonTitleTextView = null;
	private TextView workingHourTextView = null;
	private CustomRatingView ratingView = null;
	private TextView bookTimeTextView = null;
	private Button bookTimeButton = null;
	private TextView totalPriceTextView = null;
	private Button addServiceButton = null;
	private Button continueButton = null;

	private TextView bookingDetailTextView = null;

	private ArrayList<STServiceInfo> selectedServices = new ArrayList<>();
	private DataAdapter dataAdapter = null;
	private ListView serviceListView = null;

	private int bookYear, bookMonth, bookDay, bookHour, bookMinute;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_booking_summary);
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	@Override
	public void initializeActivity() {
		weekdays = new String[]{getString(R.string.weekday_1), getString(R.string.weekday_2), getString(R.string.weekday_3), getString(R.string.weekday_4), getString(R.string.weekday_5), getString(R.string.weekday_6), getString(R.string.weekday_7)};
		monthArray = new String[]{getString(R.string.month_tri_1), getString(R.string.month_tri_2), getString(R.string.month_tri_3), getString(R.string.month_tri_4), getString(R.string.month_tri_5), getString(R.string.month_tri_6), getString(R.string.month_tri_7), getString(R.string.month_tri_8), getString(R.string.month_tri_9), getString(R.string.month_tri_10), getString(R.string.month_tri_11), getString(R.string.month_tri_12)};

		Bundle bundle = getIntent().getExtras();
		String salonString = bundle.getString(EXTRA_SALON_INFO);

		try {
			salonInfo = STSalonInfo.decodeFromJSON(new JSONObject(salonString));
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		salonInfoImageView = (ImageView) findViewById(R.id.salon_imageview);
		if (!salonInfo.imageUrl.isEmpty()) {
			Picasso.with(BookingSummaryActivity.this)
					.load(salonInfo.imageUrl)
					.into(salonInfoImageView);
		}

		salonTitleTextView = (TextView) findViewById(R.id.salon_title);
		salonTitleTextView.setText(salonInfo.title);

		workingHourTextView = (TextView) findViewById(R.id.txt_working_hour);
		workingHourTextView.setText(Global.formatString("%02d:%02d-%02d:%02d", salonInfo.startHour, salonInfo.startMinute, salonInfo.endHour, salonInfo.endMinute));

		ratingView = (CustomRatingView) findViewById(R.id.salon_rating_view);
		ratingView.editable = false;
		ratingView.rating = salonInfo.rating;

		bookTimeTextView = (TextView) findViewById(R.id.txt_date);

		bookTimeButton = (Button) findViewById(R.id.btn_book_time);
		bookTimeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedBookTime();
			}
		});

		totalPriceTextView = (TextView)findViewById(R.id.txt_loyalty_points);
		totalPriceTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedLoyaltyPoints();
			}
		});

		addServiceButton = (Button) findViewById(R.id.btn_add_service);
		addServiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedAddService();
			}
		});

		continueButton = (Button) findViewById(R.id.btn_continue);
		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedContinue();
			}
		});

		bookingDetailTextView = (TextView) findViewById(R.id.txt_booking_details);

		dataAdapter = new DataAdapter(BookingSummaryActivity.this, selectedServices);

		serviceListView = (ListView) findViewById(R.id.service_listview);
		serviceListView.setAdapter(dataAdapter);
		serviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onServiceSelected(i);
			}
		});

		Calendar calendar = Calendar.getInstance();
		bookYear = calendar.get(Calendar.YEAR);
		bookMonth = calendar.get(Calendar.MONTH) + 1;
		bookDay = calendar.get(Calendar.DAY_OF_MONTH);
		bookHour = calendar.get(Calendar.HOUR_OF_DAY);
		bookMinute = calendar.get(Calendar.MINUTE);

		updateBookTimeText();
	}


	private void onClickedBookTime() {
		CustomDateTimePickerDialog dialog = new CustomDateTimePickerDialog(BookingSummaryActivity.this);

		dialog.pickerMode = CustomDateTimePickerDialog.PICKER_MODE_DATETIME;
		dialog.dateTimeChangedListener = new CustomDateTimePickerDialog.OnDateTimeChangedListener() {
			@Override
			public void dateTimeChanged(int year, int month, int day, int hour, int minute) {
				bookYear = year;
				bookMonth = month;
				bookDay = day;
				bookHour = hour;
				bookMinute = minute;

				updateBookTimeText();
			}
		};

		dialog.year = bookYear;
		dialog.month = bookMonth;
		dialog.day = bookDay;
		dialog.hour = bookHour;
		dialog.minute = bookMinute;

		dialog.show();
	}

	private void updateBookTimeText() {
		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.YEAR, bookYear);
		calendar.set(Calendar.MONTH, bookMonth - 1);
		calendar.set(Calendar.DAY_OF_MONTH, bookDay);
		calendar.set(Calendar.HOUR_OF_DAY, bookHour);
		calendar.set(Calendar.MINUTE, bookMinute);

		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;

		String timeText = weekdays[dayOfWeek] + ", " + bookDay + " " + monthArray[bookMonth - 1] + " " + bookYear + " " + Global.formatString("%02d:%02d", bookHour, bookMinute);
		bookTimeTextView.setText(timeText);
	}


	private void onClickedLoyaltyPoints() {
		JSONArray itemArray = new JSONArray();
		for (int i = 0; i < giftCardArray.size(); i++) {
			STGiftCardInfo giftCardInfo = giftCardArray.get(i);
			itemArray.put(giftCardInfo.encodeToJSON());
		}

		Bundle bundle = new Bundle();
		bundle.putString(SelectLoyaltyPointsActivity.EXTRA_SELECTED_CARDS, itemArray.toString());
		bundle.putInt(SelectLoyaltyPointsActivity.EXTRA_LOYALTY_POINTS, loyaltyPoints);

		pushNewActivityAnimatedFromRight(SelectLoyaltyPointsActivity.class, bundle, REQUEST_CODE_SELECT_LOYALTYPOINTS);
	}


	private void onClickedAddService() {
		JSONArray itemArray = new JSONArray();
		for (int i = 0; i < salonInfo.servicesArray.size(); i++) {
			STServiceInfo serviceInfo = salonInfo.servicesArray.get(i);
			serviceInfo.checked = false;

			for (int j = 0; j < selectedServices.size(); j++) {
				STServiceInfo selectedInfo = selectedServices.get(j);
				if (serviceInfo.uid.equals(selectedInfo.uid)) {
					serviceInfo.checked = true;
					break;
				}
			}

			itemArray.put(serviceInfo.encodeToJSON());
		}

		Bundle bundle = new Bundle();
		bundle.putString(SelectServiceActivity.EXTRA_SELECTED_SERVICES, itemArray.toString());

		pushNewActivityAnimatedFromRight(SelectServiceActivity.class, bundle, REQUEST_CODE_SELECT_SERVICES);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE_SELECT_SERVICES && resultCode == RESULT_OK) {
			String dataString = data.getStringExtra(SelectServiceActivity.EXTRA_SELECTED_SERVICES);
			try {
				ArrayList<STServiceInfo> tempServices = new ArrayList<>();
				JSONArray itemArray = new JSONArray(dataString);
				int totalPrice = 0;
				for (int i = 0; i < itemArray.length(); i++) {
					JSONObject item = itemArray.getJSONObject(i);
					STServiceInfo selectedService = STServiceInfo.decodeFromJSON(item);
					tempServices.add(selectedService);
					totalPrice += selectedService.price;
				}

				selectedServices.clear();
				selectedServices.addAll(tempServices);

				dataAdapter.notifyDataSetChanged();

				this.totalPrice = totalPrice;
				updateTotalPrice();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (requestCode == REQUEST_CODE_SELECT_LOYALTYPOINTS && resultCode == RESULT_OK) {
			String cardsString = data.getStringExtra(SelectLoyaltyPointsActivity.EXTRA_SELECTED_CARDS);
			int loyaltyPoints = data.getIntExtra(SelectLoyaltyPointsActivity.EXTRA_LOYALTY_POINTS, 0);

			this.loyaltyPoints = loyaltyPoints;

			try {
				giftCardArray.clear();
				JSONArray itemArray = new JSONArray(cardsString);
				for (int i = 0; i < itemArray.length(); i++) {
					JSONObject item = itemArray.getJSONObject(i);
					giftCardArray.add(STGiftCardInfo.decodeFromJSON(item));
				}

				updateTotalPrice();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}


	private void onClickedContinue() {
		if (selectedServices.size() == 0) {
			AlertDialog alertDialog = new AlertDialog.Builder(BookingSummaryActivity.this)
					.setTitle(R.string.alert_title_note)
					.setMessage(R.string.no_selected_services_select_now)
					.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							onClickedAddService();
						}
					})
					.setNegativeButton(R.string.cancel, null)
					.create();
			alertDialog.show();
		} else {
			try {
				Card card = new Card(Global.loadCardNumber(), Global.loadCVV(), Global.loadExpirationMonth(), Global.loadExpirationYear(), Global.loadCardHolderName());

				Start start = new Start("test_open_k_1f5e9f125fbd4b4ca48b");
				start.createToken(BookingSummaryActivity.this, card, tokenCallback, getActualPrice() * 100, "usd");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	public Action getIndexApiAction() {
		Thing object = new Thing.Builder()
				.setName("BookingSummary Page") // TODO: Define a title for the content shown.
				// TODO: Make sure this auto-generated URL is correct.
				.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
				.build();
		return new Action.Builder(Action.TYPE_VIEW)
				.setObject(object)
				.setActionStatus(Action.STATUS_TYPE_COMPLETED)
				.build();
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		AppIndex.AppIndexApi.start(client, getIndexApiAction());
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.end(client, getIndexApiAction());
		client.disconnect();
	}


	public class ServiceItemViewHolder {
		public TextView title_textview = null;
		public TextView description_textview = null;
		public TextView price_textview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STServiceInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ServiceItemViewHolder holder = null;
			STServiceInfo itemInfo = selectedServices.get(position);

			if (convertView == null) {
				holder = new ServiceItemViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_salon_service_layout, null);

				holder.title_textview = (TextView) itemView.findViewById(R.id.salon_title);
				holder.description_textview = (TextView) itemView.findViewById(R.id.txt_contents);
				holder.price_textview = (TextView) itemView.findViewById(R.id.txt_price);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ServiceItemViewHolder) convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.description_textview.setText(itemInfo.contents);
			holder.price_textview.setText("AED\n" + itemInfo.price);

			return convertView;
		}
	}


	private void onServiceSelected(final int index) {
		AlertDialog alertDialog = new AlertDialog.Builder(BookingSummaryActivity.this)
				.setTitle(R.string.alert_title_note)
				.setMessage(R.string.are_you_sure_to_cancel_this_service)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						selectedServices.remove(index);
						dataAdapter.notifyDataSetChanged();
					}
				})
				.setNegativeButton(R.string.cancel, null)
				.create();
		alertDialog.show();
	}


	private int getActualPrice() {
		int discount = loyaltyPoints;
		for (int i = 0; i < giftCardArray.size(); i++) {
			STGiftCardInfo giftCardInfo = giftCardArray.get(i);
			discount += giftCardInfo.price;
		}

		int actualPrice = totalPrice - discount;
		if (actualPrice < 0)
			actualPrice = 0;

		return actualPrice;
	}


	private void updateTotalPrice() {
		int actualPrice = getActualPrice();
		totalPriceTextView.setText("" + actualPrice + "/" + totalPrice + " AED");
	}

	private TokenCallback tokenCallback = new TokenCallback() {
		@Override
		public void onSuccess(Token token) {
			String giftcard_ids = "";
			for (int i = 0; i < giftCardArray.size(); i++) {
				STGiftCardInfo giftCardInfo = giftCardArray.get(i);

				if (!giftcard_ids.isEmpty())
					giftcard_ids += ":";
				giftcard_ids += giftCardInfo.uid;
			}

			String service_ids = "";
			for (int i = 0; i < selectedServices.size(); i++) {
				STServiceInfo serviceInfo = selectedServices.get(i);

				if (!service_ids.isEmpty())
					service_ids += ":";
				service_ids += serviceInfo.uid;
			}

			showProgress();
			CommManager.bookService(BookingSummaryActivity.this,
					salonInfo.uid,
					totalPrice,
					loyaltyPoints,
					giftcard_ids,
					service_ids,
					bookTimeTextView.getText().toString(),
					token.getId(),
					new CommDelegate() {
				@Override
				public void onBookResult(int statusCode, String statusDescription) {
					super.onBookResult(statusCode, statusDescription);
					stopProgress();

					if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
						AlertDialog alertDialog = new AlertDialog.Builder(BookingSummaryActivity.this)
								.setTitle(R.string.alert_title_note)
								.setMessage(R.string.your_booking_ordered_successfully)
								.setPositiveButton(R.string.ok, null)
								.create();
						alertDialog.show();
					} else {
						Global.showToast(BookingSummaryActivity.this, statusDescription);
					}
				}
			});
		}
		@Override
		public void onError(StartApiException error) {
			Global.showToast(BookingSummaryActivity.this, error.getMessage());
		}
		@Override
		public void onCancel() {
		}
	};

}

