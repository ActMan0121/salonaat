package com.wxcomp.salonaat;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.baoyz.actionsheet.ActionSheet;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.data_structure.STUserInfo;
import com.wxcomp.misc.Global;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Administrator on 12/14/2016.
 */

public class BusinessAccountEditProfileActivity extends SuperActivity {
	// User information
	private STUserInfo userInfo = null;

	// UI Controls
	private ImageButton photoButton = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_business_account_edit_profile);
	}

	@Override
	public void initializeActivity() {
		photoButton = (ImageButton)findViewById(R.id.btn_photo);
		photoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPhoto();
			}
		});

		Button signupButton = (Button)findViewById(R.id.person_signup);
		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignUp();
			}
		});

		showProgress();
		CommManager.getUserProfile(BusinessAccountEditProfileActivity.this, new CommDelegate() {
			@Override
			public void onGetUserProfileResult(int statusCode, String statusDescription, STUserInfo userinfo) {
				super.onGetUserProfileResult(statusCode, statusDescription, userinfo);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					initUI();
				}
			}
		});
	}

	private void onClickedPhoto() {
		ActionSheet.createBuilder(BusinessAccountEditProfileActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.take_photo), getString(R.string.choose_photo))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Take photo
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
							intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							startActivityForResult(intent, 1);
						} else if (index == 1) {
							// Choose photo
							Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent, 2);
						}
					}
				}).show();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {
			// Take photo
			File f = new File(Environment.getExternalStorageDirectory().toString());
			for (File temp : f.listFiles()) {
				if (temp.getName().equals("temp.jpg")) {
					f = temp;
					break;
				}
			}

			try {
				updatePhoto(f.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == 2) {
			if (data == null)
				return;

			// Choose photo
			Uri selectedImage = data.getData();
			String[] filePath = { MediaStore.Images.Media.DATA };
			Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePath[0]);
			String picturePath = c.getString(columnIndex);
			c.close();

			updatePhoto(picturePath);
		}
	}

	private void updatePhoto(String filePath) {
		showProgress();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;

		Bitmap userImage = BitmapFactory.decodeFile(filePath, options);
		photoButton.setImageBitmap(userImage);

		String path = android.os.Environment
				.getExternalStorageDirectory()
				+ File.separator
				+ "Temp";
		File fileDir = new File(path);
		if (!fileDir.exists() || !fileDir.isDirectory())
			fileDir.mkdir();

		OutputStream outFile = null;
		File file = new File(path, "photo.jpg");
		if (file.exists())
			file.delete();

		String newFilePath = file.getAbsolutePath();

		try {
			file.createNewFile();

			outFile = new FileOutputStream(file);
			userImage.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
			outFile.flush();
			outFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		CommManager.uploadImage(BusinessAccountEditProfileActivity.this, newFilePath, new CommDelegate() {
			@Override
			public void onUploadImageResult(int statusCode, String statusDescription, String imageUrl) {
				super.onUploadImageResult(statusCode, statusDescription, imageUrl);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(BusinessAccountEditProfileActivity.this, "Success : " + imageUrl);
					BusinessAccountEditProfileActivity.this.userInfo.image_url = imageUrl;
				} else {
					Global.showToast(BusinessAccountEditProfileActivity.this, statusDescription);
				}
			}
		});
	}


	private void onClickedSignUp() {
		EditText firstNameEdit = (EditText)findViewById(R.id.edt_name);
		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		EditText emailEdit = (EditText)findViewById(R.id.edt_email);
//		EditText oldPasswordEdit = (EditText)findViewById(R.id.edt_current_password);
//		EditText passwordEdit = (EditText)findViewById(R.id.edt_password);
//		EditText confirmPasswordEdit = (EditText)findViewById(R.id.edt_confirm_password);
		EditText descriptionEdit = (EditText)findViewById(R.id.edt_description);

		if (userInfo.image_url.isEmpty()) {
			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_select_photo);
			return;
		}

		if (firstNameEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_first_name);
			return;
		}

		if (lastNameEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_last_name);
			return;
		}

		if (emailEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_your_email);
			return;
		}

//		if (oldPasswordEdit.getText().toString().isEmpty()) {
//			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_current_password);
//			return;
//		}
//
//		if (passwordEdit.getText().toString().isEmpty()) {
//			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_new_password);
//			return;
//		}
//
//		if (confirmPasswordEdit.getText().toString().isEmpty()) {
//			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.please_input_password_again);
//			return;
//		}
//
//		if (!passwordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
//			Global.showToast(BusinessAccountEditProfileActivity.this, R.string.password_not_match);
//			return;
//		}


		showProgress();
		CommManager.updateProfile(BusinessAccountEditProfileActivity.this,
				userInfo.image_url,
				firstNameEdit.getText().toString(),
				lastNameEdit.getText().toString(),
				middleNameEdit.getText().toString(),
				1,
				"",
				Global.loadGender(),
				"",
				emailEdit.getText().toString(),
				"",
				"",
				descriptionEdit.getText().toString(),
				Global.loadSignType(),
				"",
				new CommDelegate() {
					@Override
					public void onUpdateProfileResult(int statusCode, String statusDescription) {
						super.onUpdateProfileResult(statusCode, statusDescription);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							Global.showToast(BusinessAccountEditProfileActivity.this, R.string.your_profile_updated_successfully);
							finishFromLeft();
						} else {
							Global.showToast(BusinessAccountEditProfileActivity.this, statusDescription);
						}
					}
				});
	}


	private void initUI() {
		EditText firstNameEdit = (EditText)findViewById(R.id.edt_name);
		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		EditText descriptionEdit = (EditText)findViewById(R.id.edt_description);

		firstNameEdit.setText(userInfo.first_name);
		lastNameEdit.setText(userInfo.last_name);
		middleNameEdit.setText(userInfo.middle_name);
		emailEdit.setText(userInfo.email);
		descriptionEdit.setText(userInfo.desc);

		if (userInfo.image_url.isEmpty()){
			Picasso.with(BusinessAccountEditProfileActivity.this)
					.load(userInfo.image_url)
					.into(photoButton);
		}
	}




}
