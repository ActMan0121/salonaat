package com.wxcomp.salonaat;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.baoyz.actionsheet.ActionSheet;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import cz.msebera.android.httpclient.auth.AUTH;

/**
 * Created by Administrator on 12/11/2016.
 */

public class BusinessSignupActivity extends SuperActivity {
	// User information
	private Bitmap userImage = null;
	private String imageUrl = "";

	// UI Controls
	private ImageButton photoButton = null;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_business_signup);
	}


	@Override
	public void initializeActivity() {
		photoButton = (ImageButton)findViewById(R.id.btn_photo);
		photoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPhoto();
			}
		});

		Button signupButton = (Button)findViewById(R.id.person_signup);
		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignUp();
			}
		});
	}

	private void onClickedPhoto() {
		ActionSheet.createBuilder(BusinessSignupActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.take_photo), getString(R.string.choose_photo))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Take photo
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
							intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							startActivityForResult(intent, 1);
						} else if (index == 1) {
							// Choose photo
							Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent, 2);
						}
					}
				}).show();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			// Take photo
			File f = new File(Environment.getExternalStorageDirectory().toString());
			for (File temp : f.listFiles()) {
				if (temp.getName().equals("temp.jpg")) {
					f = temp;
					break;
				}
			}

			try {
				updatePhoto(f.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == 2) {
			if (data == null)
				return;

			// Choose photo
			Uri selectedImage = data.getData();
			String[] filePath = { MediaStore.Images.Media.DATA };
			Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePath[0]);
			String picturePath = c.getString(columnIndex);
			c.close();

			updatePhoto(picturePath);
		}
	}


	private void updatePhoto(String filePath) {
		showProgress();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;

		userImage = BitmapFactory.decodeFile(filePath, options);
		photoButton.setImageBitmap(userImage);

		String path = android.os.Environment
				.getExternalStorageDirectory()
				+ File.separator
				+ "Temp";
		File fileDir = new File(path);
		if (!fileDir.exists() || !fileDir.isDirectory())
			fileDir.mkdir();

		OutputStream outFile = null;
		File file = new File(path, "photo.jpg");
		if (file.exists())
			file.delete();

		String newFilePath = file.getAbsolutePath();

		try {
			file.createNewFile();

			outFile = new FileOutputStream(file);
			userImage.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
			outFile.flush();
			outFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		CommManager.uploadImage(BusinessSignupActivity.this, newFilePath, new CommDelegate() {
			@Override
			public void onUploadImageResult(int statusCode, String statusDescription, String imageUrl) {
				super.onUploadImageResult(statusCode, statusDescription, imageUrl);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(BusinessSignupActivity.this, "Success : " + imageUrl);
					BusinessSignupActivity.this.imageUrl = imageUrl;
				} else {
					Global.showToast(BusinessSignupActivity.this, statusDescription);
				}
			}
		});
	}


	private void onClickedSignUp() {
		EditText nameEdit = (EditText)findViewById(R.id.edt_name);
		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		final EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		final EditText passwordEdit = (EditText)findViewById(R.id.edt_password);
		EditText confirmPasswordEdit = (EditText)findViewById(R.id.edt_confirm_password);
		EditText descriptionEdit = (EditText)findViewById(R.id.edt_description);
		EditText inviteCodeEdit = (EditText)findViewById(R.id.edt_invite_code);

		if (imageUrl == null || imageUrl.length() == 0) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_select_photo);
			return;
		}

		if (nameEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_input_first_name);
			return;
		}

		if (lastNameEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_input_last_name);
			return;
		}

		if (emailEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_input_your_email);
			return;
		}

		if (passwordEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_input_user_password);
			return;
		}

		if (confirmPasswordEdit.getText().toString().isEmpty()) {
			Global.showToast(BusinessSignupActivity.this, R.string.please_input_password_again);
			return;
		}

		if (!passwordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
			Global.showToast(BusinessSignupActivity.this, R.string.password_not_match);
			return;
		}


		showProgress();
		CommManager.signUp(BusinessSignupActivity.this,
				imageUrl,
				nameEdit.getText().toString(),
				lastNameEdit.getText().toString(),
				middleNameEdit.getText().toString(),
				1,
				"",
				2,
				"",
				emailEdit.getText().toString(),
				passwordEdit.getText().toString(),
				"",
				descriptionEdit.getText().toString(),
				1,
				inviteCodeEdit.getText().toString(),
				new CommDelegate() {
					@Override
					public void onSignupResult(int statusCode, String statusDescription, String user_id, String auth_token) {
						super.onSignupResult(statusCode, statusDescription, user_id, auth_token);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							Global.saveAccountType(Const.ACCOUNT_TYPE_BUSINESS);
							Global.saveUserID(user_id);
							Global.saveAuthToken(auth_token);
							Global.saveEmail(emailEdit.getText().toString());
							Global.savePassword(passwordEdit.getText().toString());

							Global.saveSignType(Const.SIGN_TYPE_NORMAL);

							// Go to home activity
							Intent intent = new Intent(BusinessSignupActivity.this, HomeActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							startActivity(intent);
							overridePendingTransition(R.anim.enter_from_bottom, R.anim.exit_to_top);
						} else {
							Global.showToast(BusinessSignupActivity.this, statusDescription);
						}
					}
				});
	}
}
