package com.wxcomp.salonaat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.games.Notifications;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 12/14/2016.
 */

public class FavoritesActivity extends SuperActivity {
	private ArrayList<STSalonInfo> itemsArray = new ArrayList<>();
	private DataAdapter dataAdapter = null;
	private XListView listView = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_favorites);
	}


	@Override
	public void initializeActivity() {
		dataAdapter = new DataAdapter(FavoritesActivity.this, itemsArray);

		listView = (XListView)findViewById(R.id.favourites_list_view);
		listView.setAdapter(dataAdapter);
		listView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				itemsArray.clear();
				onListRefreshed();
			}
			@Override
			public void onLoadMore() {
				onListRefreshed();
			}
		});
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i - 1);
			}
		});
		listView.setPullLoadEnable(true);

		onListRefreshed();
	}


	public class ViewHolder {
		public TextView title_textview = null;
		public ImageView salon_imageview = null;
		public TextView contents_textview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STSalonInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			STSalonInfo itemInfo = itemsArray.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_favourite_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.salon_imageview = (ImageView)itemView.findViewById(R.id.img_salon);
				holder.contents_textview = (TextView)itemView.findViewById(R.id.txt_contents);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			if (!itemInfo.imageUrl.isEmpty()) {
				Picasso.with(FavoritesActivity.this)
						.load(itemInfo.imageUrl)
						.into(holder.salon_imageview);
			}
			holder.contents_textview.setText(itemInfo.contents);

			return convertView;
		}
	}


	private void onListRefreshed() {
		if (itemsArray.size() == 0)
			showProgress();

		CommManager.getFavoritesList(FavoritesActivity.this, itemsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetFavoritesList(int statusCode, String statusDescription, ArrayList<STSalonInfo> favoriteSalonsList) {
				super.onGetFavoritesList(statusCode, statusDescription, favoriteSalonsList);

				stopProgress();
				listView.stopRefresh();
				listView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < favoriteSalonsList.size(); i++) {
						boolean isExist = false;
						STSalonInfo newItem = favoriteSalonsList.get(i);
						for (int j = 0; j < itemsArray.size(); j++) {
							STSalonInfo oldItem = itemsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						itemsArray.add(newItem);
					}

					dataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onItemSelected(final int index) {
		if (index < 0 || index >= itemsArray.size())
			return;

		AlertDialog alertDialog = new AlertDialog.Builder(FavoritesActivity.this)
				.setTitle("Note")
				.setMessage("Are you sure to remove from favourites?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						onRemoveFavourite(index);
					}
				})
				.setNegativeButton("Cancel", null)
				.create();
		alertDialog.show();
	}


	private void onRemoveFavourite(final int index) {
		STSalonInfo salonInfo = itemsArray.get(index);
		stopProgress();
		CommManager.removeFavorite(FavoritesActivity.this, salonInfo.uid, new CommDelegate() {
			@Override
			public void onRemoveFavoriteResult(int statusCode, String statusDescription) {
				super.onRemoveFavoriteResult(statusCode, statusDescription);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					itemsArray.remove(index);
					dataAdapter.notifyDataSetChanged();
				} else {
					Global.showToast(FavoritesActivity.this, statusDescription);
				}
			}
		});
	}
}
