package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/14/2016.
 */

public class FeedbackActivity extends SuperActivity {
	private EditText contentsEdit = null;
	private Button submitButton = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_feedback);
	}


	@Override
	public void initializeActivity() {
		contentsEdit = (EditText)findViewById(R.id.edt_contents);

		submitButton = (Button)findViewById(R.id.btn_summit);
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSubmit();
			}
		});
	}


	private void onClickedSubmit() {
		if (contentsEdit.getText().toString().isEmpty()) {
			Global.showToast(FeedbackActivity.this, getString(R.string.please_input_feedback_contents));
			return;
		}


		showProgress();
		CommManager.uploadFeedback(FeedbackActivity.this, contentsEdit.getText().toString(), new CommDelegate() {
			@Override
			public void onFeedbackResult(int statusCode, String statusDescription) {
				super.onFeedbackResult(statusCode, statusDescription);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(FeedbackActivity.this, getString(R.string.submit_success));
					contentsEdit.setText("");
				} else {
					Global.showToast(FeedbackActivity.this, statusDescription);
				}
			}
		});
	}

}
