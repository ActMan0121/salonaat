package com.wxcomp.salonaat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomInputDialog;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STGiftCardInfo;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/14/2016.
 */

public class GiftCardsActivity extends SuperActivity {
	private ArrayList<STGiftCardInfo> totalCardsArray = new ArrayList<>();
	private TotalCardsDataAdapter totalCardsDataAdapter = null;
	private XListView totalCardsListView = null;

	private ArrayList<STGiftCardInfo> purchasedCardsArray = new ArrayList<>();
	private PurchasedCardsDataAdapter purchasedCardsDataAdapter = null;
	private XListView purchasedCardsListView = null;

	private int tab_index = 1;

	private Button allButton = null, purchasedButton = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_gift_cards);
	}


	@Override
	public void initializeActivity() {
		allButton = (Button)findViewById(R.id.btn_all);
		allButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 0;
				updateTabSelection();
			}
		});
		purchasedButton = (Button)findViewById(R.id.btn_purchased);
		purchasedButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 1;
				updateTabSelection();
			}
		});


		totalCardsDataAdapter = new TotalCardsDataAdapter(GiftCardsActivity.this, totalCardsArray);

		totalCardsListView = (XListView)findViewById(R.id.gift_cards_listview);
		totalCardsListView.setAdapter(totalCardsDataAdapter);
		totalCardsListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				totalCardsArray.clear();
				onTotalCardListRefreshed();
			}
			@Override
			public void onLoadMore() {
				onTotalCardListRefreshed();
			}
		});
		totalCardsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onTotalCardItemSelected(i - 1);
			}
		});
		totalCardsListView.setPullLoadEnable(true);

		onTotalCardListRefreshed();



		purchasedCardsDataAdapter = new PurchasedCardsDataAdapter(GiftCardsActivity.this, purchasedCardsArray);

		purchasedCardsListView = (XListView)findViewById(R.id.purchased_cards_listview);
		purchasedCardsListView.setAdapter(purchasedCardsDataAdapter);
		purchasedCardsListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				purchasedCardsArray.clear();
				onPurchasedCardListRefreshed();
			}
			@Override
			public void onLoadMore() {
				onPurchasedCardListRefreshed();
			}
		});
		purchasedCardsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onPurchasedCardItemSelected(i - 1);
			}
		});
		purchasedCardsListView.setPullLoadEnable(true);

		onPurchasedCardListRefreshed();


		updateTabSelection();
	}


	private void updateTabSelection() {
		purchasedCardsListView.setVisibility(View.INVISIBLE);
		totalCardsListView.setVisibility(View.INVISIBLE);

		purchasedButton.setBackgroundColor(Color.TRANSPARENT);
		allButton.setBackgroundColor(Color.TRANSPARENT);

		if (tab_index == 0) {
			totalCardsListView.setVisibility(View.VISIBLE);
			allButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		} else {
			purchasedCardsListView.setVisibility(View.VISIBLE);
			purchasedButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}
	}


	public class TotalCardsViewHolder {
		public TextView title_textview = null;
		public ImageView gift_card_imageview = null;
		public TextView save_textview = null;
	}

	public class TotalCardsDataAdapter extends ArrayAdapter {
		public TotalCardsDataAdapter(Context context, List<STGiftCardInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TotalCardsViewHolder holder = null;
			STGiftCardInfo itemInfo = totalCardsArray.get(position);

			if (convertView == null) {
				holder = new TotalCardsViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_gift_card_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.gift_card_imageview = (ImageView)itemView.findViewById(R.id.img_salon);
				holder.save_textview = (TextView)itemView.findViewById(R.id.txt_save);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (TotalCardsViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			if (!itemInfo.image_url.isEmpty()) {
				Picasso.with(GiftCardsActivity.this)
						.load(itemInfo.image_url)
						.into(holder.gift_card_imageview);
			}
			holder.save_textview.setText("" + itemInfo.price);

			return convertView;
		}
	}


	private void onTotalCardListRefreshed() {
		if (totalCardsArray.size() == 0)
			showProgress();

		CommManager.getGiftCardsList(GiftCardsActivity.this, totalCardsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetGiftCardsList(int statusCode, String statusDescription, ArrayList<STGiftCardInfo> giftCardsList) {
				super.onGetGiftCardsList(statusCode, statusDescription, giftCardsList);

				stopProgress();
				totalCardsListView.stopRefresh();
				totalCardsListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < giftCardsList.size(); i++) {
						boolean isExist = false;
						STGiftCardInfo newItem = giftCardsList.get(i);
						for (int j = 0; j < totalCardsArray.size(); j++) {
							STGiftCardInfo oldItem = totalCardsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						totalCardsArray.add(newItem);
					}

					totalCardsDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onTotalCardItemSelected(int index) {
		if (index < 0 || index >= totalCardsArray.size())
			return;

		final STGiftCardInfo itemInfo = totalCardsArray.get(index);

		AlertDialog alertDialog = new AlertDialog.Builder(GiftCardsActivity.this)
				.setTitle(R.string.alert_title_note)
				.setMessage(R.string.are_you_sure_to_purchase_this_gift_card)
				.setNeutralButton("50 AED", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						onPurchaseGiftCard(itemInfo, 50);
					}
				})
				.setPositiveButton("100 AED", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						onPurchaseGiftCard(itemInfo, 100);
					}
				})
				.setNegativeButton(R.string.cancel, null)
				.create();
		alertDialog.show();
	}


	private void onPurchaseGiftCard(STGiftCardInfo giftCardInfo, int point) {
		showProgress();
		CommManager.purchaseGiftCard(GiftCardsActivity.this, giftCardInfo.uid, point, new CommDelegate() {
			@Override
			public void onPurchaseGiftCardResult(int statusCode, String statusDescription) {
				super.onPurchaseGiftCardResult(statusCode, statusDescription);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(GiftCardsActivity.this, getString(R.string.purchase_succeeded));

					purchasedCardsArray.clear();
					onPurchasedCardListRefreshed();
				} else {
					Global.showToast(GiftCardsActivity.this, statusDescription);
				}
			}
		});
	}





	public class PurchasedCardsViewHolder {
		public TextView title_textview = null;
		public ImageView gift_card_imageview = null;
		public TextView save_textview = null;
	}

	public class PurchasedCardsDataAdapter extends ArrayAdapter {
		public PurchasedCardsDataAdapter(Context context, List<STGiftCardInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			PurchasedCardsViewHolder holder = null;
			STGiftCardInfo itemInfo = purchasedCardsArray.get(position);

			if (convertView == null) {
				holder = new PurchasedCardsViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_gift_card_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.gift_card_imageview = (ImageView)itemView.findViewById(R.id.img_salon);
				holder.save_textview = (TextView)itemView.findViewById(R.id.txt_save);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (PurchasedCardsViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			if (!itemInfo.image_url.isEmpty()) {
				Picasso.with(GiftCardsActivity.this)
						.load(itemInfo.image_url)
						.into(holder.gift_card_imageview);
			}
			holder.save_textview.setText("" + itemInfo.price);

			return convertView;
		}
	}


	private void onPurchasedCardListRefreshed() {
		if (purchasedCardsArray.size() == 0)
			showProgress();

		CommManager.getPurchasedGiftCardsList(GiftCardsActivity.this, new CommDelegate() {
			@Override
			public void onGetGiftCardsList(int statusCode, String statusDescription, ArrayList<STGiftCardInfo> giftCardsList) {
				super.onGetGiftCardsList(statusCode, statusDescription, giftCardsList);

				stopProgress();

				purchasedCardsListView.stopRefresh();
				purchasedCardsListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < giftCardsList.size(); i++) {
						boolean isExist = false;
						STGiftCardInfo newItem = giftCardsList.get(i);
						for (int j = 0; j < purchasedCardsArray.size(); j++) {
							STGiftCardInfo oldItem = purchasedCardsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						purchasedCardsArray.add(newItem);
					}

					purchasedCardsDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onPurchasedCardItemSelected(int index) {
		if (index < 0 || index >= totalCardsArray.size())
			return;

		final STGiftCardInfo itemInfo = totalCardsArray.get(index);

		final CustomInputDialog dialog = new CustomInputDialog(GiftCardsActivity.this);
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialogInterface) {
				if (dialog.email.isEmpty())
					return;

				showProgress();
				CommManager.shareGiftCard(GiftCardsActivity.this, itemInfo.uid, dialog.email, new CommDelegate() {
					@Override
					public void onShareGiftCardResult(int statusCode, String statusDescription) {
						super.onShareGiftCardResult(statusCode, statusDescription);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							Global.showToast(GiftCardsActivity.this, getString(R.string.shared_a_gift_card_successfully));

							purchasedCardsArray.clear();
							onPurchasedCardListRefreshed();
						} else {
							Global.showToast(GiftCardsActivity.this, statusDescription);
						}
					}
				});
			}
		});
		dialog.show();
	}






}
