package com.wxcomp.salonaat;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;

import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/11/2016.
 */

public class HomeActivity extends LogoTitleActivity {
	private int tabIndex = Const.GENDER_MALE;

	ImageButton maleTabButton = null;
	ImageButton femaleTabButton = null;
	ImageButton unisexTabButton = null;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_home);
	}

	@Override
	public void initializeActivity() {
		Global.saveStartFlag(true);

		ImageButton hairButton = (ImageButton)findViewById(R.id.hair_button);
		ImageButton faceButton = (ImageButton)findViewById(R.id.face_button);
		ImageButton cosmeticsButton = (ImageButton)findViewById(R.id.cosmetics_button);
		ImageButton bodyButton = (ImageButton)findViewById(R.id.body_button);

		hairButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedHair();
			}
		});
		faceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFace();
			}
		});
		cosmeticsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedCosmetics();
			}
		});
		bodyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedBody();
			}
		});


		maleTabButton = (ImageButton)findViewById(R.id.male_tab_button);
		femaleTabButton = (ImageButton)findViewById(R.id.female_tab_button);
		unisexTabButton = (ImageButton)findViewById(R.id.unisex_tab_button);

		maleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedMale();
			}
		});
		femaleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedFemale();
			}
		});
		unisexTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedUnisex();
			}
		});

		tabIndex = Global.loadGender();
		updateTabSelection();
	}


	private void onClickedHair() {
		Bundle bundle = new Bundle();
		bundle.putInt(SubCategoryActivity.EXTRA_CATEGORY, Const.SERVICE_CATEGORY_HAIR);
		pushNewActivityAnimatedFromRight(SubCategoryActivity.class, bundle);
	}


	private void onClickedFace() {
		Bundle bundle = new Bundle();
		bundle.putInt(SubCategoryActivity.EXTRA_CATEGORY, Const.SERVICE_CATEGORY_FACE);
		pushNewActivityAnimatedFromRight(SubCategoryActivity.class, bundle);
	}


	private void onClickedCosmetics() {
		Bundle bundle = new Bundle();
		bundle.putInt(SubCategoryActivity.EXTRA_CATEGORY, Const.SERVICE_CATEGORY_COSMETICS);
		pushNewActivityAnimatedFromRight(SubCategoryActivity.class, bundle);
	}

	private void onClickedBody() {
		Bundle bundle = new Bundle();
		bundle.putInt(SubCategoryActivity.EXTRA_CATEGORY, Const.SERVICE_CATEGORY_BODY);
		pushNewActivityAnimatedFromRight(SubCategoryActivity.class, bundle);
	}


	private void onSelectedMale() {
		tabIndex = Const.GENDER_MALE;
		updateTabSelection();
		Global.saveGender(tabIndex);
	}

	private void onSelectedFemale() {
		tabIndex = Const.GENDER_FEMALE;
		updateTabSelection();
		Global.saveGender(tabIndex);
	}

	private void onSelectedUnisex() {
		tabIndex = Const.GENDER_UNISEX;
		updateTabSelection();
		Global.saveGender(tabIndex);
	}


	private void updateTabSelection() {
		maleTabButton.setBackgroundColor(Color.TRANSPARENT);
		femaleTabButton.setBackgroundColor(Color.TRANSPARENT);
		unisexTabButton.setBackgroundColor(Color.TRANSPARENT);

		int selectedColor = getResources().getColor(R.color.gold_color);

		if (tabIndex == Const.GENDER_MALE)
			maleTabButton.setBackgroundColor(selectedColor);
		else if (tabIndex == Const.GENDER_FEMALE)
			femaleTabButton.setBackgroundColor(selectedColor);
		else if (tabIndex == Const.GENDER_UNISEX)
			unisexTabButton.setBackgroundColor(selectedColor);
	}
}
