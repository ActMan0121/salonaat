package com.wxcomp.salonaat;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.plus.PlusShare;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/14/2016.
 */

public class InviteFriendsActivity extends SuperActivity {
	private final int SHARE_TYPE_WHATSAPP = 0;
	private final int SHARE_TYPE_FACEBOOK = 1;
	private final int SHARE_TYPE_GOOGLEPLUS = 2;
	private final int SHARE_TYPE_TWITTER = 3;
	private final int SHARE_TYPE_SMS = 4;
	private final int SHARE_TYPE_EMAIL = 5;

	private RelativeLayout whatsapp_layout = null;
	private RelativeLayout facebook_layout = null;
	private RelativeLayout googleplus_layout = null;
	private RelativeLayout twitter_layout = null;
	private RelativeLayout sms_layout = null;
	private RelativeLayout email_layout = null;

	private Button inviteButton = null;

	private String invite_code = "";

	private int selection_index = 0;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_invite_friends);
	}


	@Override
	public void initializeActivity() {
		whatsapp_layout = (RelativeLayout)findViewById(R.id.whatsapp_layout);
		whatsapp_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_WHATSAPP;
				updateInviteSelection();
			}
		});
		facebook_layout = (RelativeLayout)findViewById(R.id.facebook_layout);
		facebook_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_FACEBOOK;
				updateInviteSelection();
			}
		});
		googleplus_layout = (RelativeLayout)findViewById(R.id.googleplus_layout);
		googleplus_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_GOOGLEPLUS;
				updateInviteSelection();
			}
		});
		twitter_layout = (RelativeLayout)findViewById(R.id.twitter_layout);
		twitter_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_TWITTER;
				updateInviteSelection();
			}
		});
		sms_layout = (RelativeLayout)findViewById(R.id.sms_layout);
		sms_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_SMS;
				updateInviteSelection();
			}
		});
		email_layout = (RelativeLayout)findViewById(R.id.email_layout);
		email_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selection_index = SHARE_TYPE_EMAIL;
				updateInviteSelection();
			}
		});

		inviteButton = (Button)findViewById(R.id.btn_invite);
		inviteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedInvite();
			}
		});

		updateInviteSelection();
	}


	private void onClickedInvite() {
		showProgress();
		CommManager.getInviteCode(InviteFriendsActivity.this, new CommDelegate() {
			@Override
			public void onGetInviteCodeResult(int statusCode, String statusDescription, String invite_code) {
				super.onGetInviteCodeResult(statusCode, statusDescription, invite_code);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					InviteFriendsActivity.this.invite_code = invite_code;
					gotoInvitation();
				} else {
					Global.showToast(InviteFriendsActivity.this, statusDescription);
				}
			}
		});
	}

	private void gotoInvitation() {
		if (selection_index == SHARE_TYPE_GOOGLEPLUS) {
			Intent shareIntent = new PlusShare.Builder(this)
					.setType("text/plain")
					.setText("Invite code test")
					.setContentUrl(Uri.parse("https://developers.google.com/+/"))
					.getIntent();

			startActivityForResult(shareIntent, 0);
		} else if (selection_index == SHARE_TYPE_WHATSAPP) {
			PackageManager pm=getPackageManager();
			try {
				Intent waIntent = new Intent(Intent.ACTION_SEND);
				waIntent.setType("text/plain");
				String text = "YOUR TEXT HERE";

				PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
				//Check if package exists or not. If not then code
				//in catch block will be called
				waIntent.setPackage("com.whatsapp");

				waIntent.putExtra(Intent.EXTRA_TEXT, text);
				startActivity(Intent.createChooser(waIntent, "Share with"));
			} catch (PackageManager.NameNotFoundException e) {
				Global.showToast(this, "WhatsApp not Installed");
			}
		}
	}

	private void updateInviteSelection() {
		whatsapp_layout.setBackgroundColor(Color.TRANSPARENT);
		facebook_layout.setBackgroundColor(Color.TRANSPARENT);
		googleplus_layout.setBackgroundColor(Color.TRANSPARENT);
		twitter_layout.setBackgroundColor(Color.TRANSPARENT);
		sms_layout.setBackgroundColor(Color.TRANSPARENT);
		email_layout.setBackgroundColor(Color.TRANSPARENT);

		switch (selection_index) {
			case SHARE_TYPE_WHATSAPP:
				whatsapp_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
			case SHARE_TYPE_FACEBOOK:
				facebook_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
			case SHARE_TYPE_GOOGLEPLUS:
				googleplus_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
			case SHARE_TYPE_TWITTER:
				twitter_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
			case SHARE_TYPE_SMS:
				sms_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
			case SHARE_TYPE_EMAIL:
				email_layout.setBackgroundResource(R.drawable.rect_transparent_white);
				break;
		}
	}

}
