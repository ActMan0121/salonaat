package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.data_structure.STLiveFeedbackInfo;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/15/2016.
 */

public class LiveFeedDetailActivity extends SuperActivity {
	public static final String EXTRA_FEED_INFO = "FeedInfo";
	public STLiveFeedbackInfo liveFeedInfo = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_live_feed_detail);
	}

	@Override
	public void initializeActivity() {
		String feedString = getIntent().getStringExtra(EXTRA_FEED_INFO);

		try {
			liveFeedInfo = STLiveFeedbackInfo.decodeFromJSON(new JSONObject(feedString));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		updateContents();
	}

	public void updateContents() {
		TextView titleView = (TextView)findViewById(R.id.title_text);
		titleView.setText(liveFeedInfo.type);

		ImageView userImageView = (ImageView)findViewById(R.id.promotion_image_view);
		if (!liveFeedInfo.user_image_url.isEmpty()) {
			Picasso.with(LiveFeedDetailActivity.this)
					.load(liveFeedInfo.user_image_url)
					.into(userImageView);
		}

		CustomRatingView ratingView = (CustomRatingView)findViewById(R.id.rating_view);
		ratingView.rating = liveFeedInfo.rating;
		ratingView.editable = false;

		TextView contentsTextView = (TextView)findViewById(R.id.txt_contents);
		contentsTextView.setText(liveFeedInfo.contents);
	}

}
