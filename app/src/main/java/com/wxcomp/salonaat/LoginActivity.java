package com.wxcomp.salonaat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/11/2016.
 */

public class LoginActivity extends SuperActivity {
	private EditText emailEdit = null;
	private EditText passwordEdit = null;
	private Button loginButton = null;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_login);
	}

	@Override
	public void initializeActivity() {
		emailEdit = (EditText)findViewById(R.id.edt_email);
		passwordEdit = (EditText)findViewById(R.id.edt_password);

		emailEdit.setText(Global.loadEmail());
		passwordEdit.setText(Global.loadPassword());

		loginButton = (Button) findViewById(R.id.login_button);
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedLogin();
			}
		});
	}


	private void onClickedLogin() {
		if (emailEdit.getText().toString().equals("")) {
			Toast.makeText(LoginActivity.this, R.string.please_input_user_email, Toast.LENGTH_LONG).show();
			return;
		}

		if (passwordEdit.getText().toString().equals("")) {
			Toast.makeText(LoginActivity.this, R.string.please_input_user_password, Toast.LENGTH_LONG).show();
			return;
		}

		showProgress();
		CommManager.login(LoginActivity.this, emailEdit.getText().toString(), passwordEdit.getText().toString(), new CommDelegate() {
			@Override
			public void onLoginResult(int statusCode, String statusDescription, String user_id, int user_type, String auth_token) {
				super.onLoginResult(statusCode, statusDescription, user_id, user_type, auth_token);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.saveAccountType(user_type);
					Global.saveUserID(user_id);
					Global.saveAuthToken(auth_token);
					Global.saveEmail(emailEdit.getText().toString());
					Global.savePassword(passwordEdit.getText().toString());

					// Go to home activity
					Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					overridePendingTransition(R.anim.enter_from_bottom, R.anim.exit_to_top);
				} else {
					Global.showToast(LoginActivity.this, statusDescription);
				}
			}
		});
	}

}
