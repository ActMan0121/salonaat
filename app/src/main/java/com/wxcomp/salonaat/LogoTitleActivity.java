package com.wxcomp.salonaat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by Administrator on 12/11/2016.
 */

public abstract class LogoTitleActivity extends SuperActivity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState, int layout) {
		super.onCreate(savedInstanceState, layout);
	}


	@Override
	public void initializeSuperActivity() {
		super.initializeSuperActivity();

		View titleButton = (View)findViewById(R.id.btn_title);
		if (titleButton != null) {
			titleButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					gotoHomeActivity();
				}
			});
		}
	}
}
