package com.wxcomp.salonaat;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STLoyaltyPointInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/14/2016.
 */

public class LoyaltyPointsActivity extends SuperActivity {
	private int tab_index = 0;

	private Button newTabButton = null;
	private Button redeemedTabButton = null;

	private RelativeLayout newLayout = null;
	private RelativeLayout redeemLayout = null;

	private XListView newListView = null;
	private XListView redeemListView = null;

	private ArrayList<STLoyaltyPointInfo> newLoyaltyPointsArray = new ArrayList<>();
	private ArrayList<STLoyaltyPointInfo> redeemLoyaltyPointsArray = new ArrayList<>();

	private NewDataAdapter newDataAdapter = null;
	private RedeemDataAdapter redeemDataAdapter = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_loyalty_points);
	}

	@Override
	public void initializeActivity() {
		newTabButton = (Button)findViewById(R.id.btn_new);
		newTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 0;
				updateTabSelection();
			}
		});

		redeemedTabButton = (Button)findViewById(R.id.btn_redeem);
		redeemedTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tab_index = 1;
				updateTabSelection();
			}
		});

		newLayout = (RelativeLayout)findViewById(R.id.new_layout);
		redeemLayout = (RelativeLayout)findViewById(R.id.redeemed_layout);


		newDataAdapter = new NewDataAdapter(LoyaltyPointsActivity.this, newLoyaltyPointsArray);
		newListView = (XListView)findViewById(R.id.new_listview);
		{
			newListView.setAdapter(newDataAdapter);
			newListView.setXListViewListener(new XListView.IXListViewListener() {
				@Override
				public void onRefresh() {
					newLoyaltyPointsArray.clear();
					onNewListRefreshed();
				}
				@Override
				public void onLoadMore() {
					onNewListRefreshed();
				}
			});
			newListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
					onNewItemSelected(i - 1);
				}
			});
			newListView.setPullLoadEnable(true);
		}

		redeemDataAdapter = new RedeemDataAdapter(LoyaltyPointsActivity.this, redeemLoyaltyPointsArray);
		redeemListView = (XListView)findViewById(R.id.redeemed_listview);
		{
			redeemListView.setAdapter(redeemDataAdapter);
			redeemListView.setXListViewListener(new XListView.IXListViewListener() {
				@Override
				public void onRefresh() {
					redeemLoyaltyPointsArray.clear();
					onRedeemListRefreshed();
				}
				@Override
				public void onLoadMore() {
					onRedeemListRefreshed();
				}
			});
			redeemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
					onRedeemItemSelected(i - 1);
				}
			});
			redeemListView.setPullLoadEnable(true);
		}

		updateTabSelection();

		onNewListRefreshed();
		onRedeemListRefreshed();
	}


	private void updateTabSelection() {
		newLayout.setVisibility(View.INVISIBLE);
		redeemLayout.setVisibility(View.INVISIBLE);

		newTabButton.setBackgroundColor(Color.TRANSPARENT);
		redeemedTabButton.setBackgroundColor(Color.TRANSPARENT);

		if (tab_index == 0) {
			newLayout.setVisibility(View.VISIBLE);
			newTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		} else {
			redeemLayout.setVisibility(View.VISIBLE);
			redeemedTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}
	}



	public class NewLoyaltyViewHolder {
		public TextView title_textview = null;
		public TextView contents_textview = null;
		public TextView price_textview = null;
	}

	public class NewDataAdapter extends ArrayAdapter {
		public NewDataAdapter(Context context, List<STLoyaltyPointInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			NewLoyaltyViewHolder holder = null;
			STLoyaltyPointInfo itemInfo = newLoyaltyPointsArray.get(position);

			if (convertView == null) {
				holder = new NewLoyaltyViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_new_loyalty_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.contents_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (NewLoyaltyViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.contents_textview.setText(itemInfo.remark);
			holder.price_textview.setText("" + itemInfo.loyalty_points + "\n" + getString(R.string.points));

			return convertView;
		}
	}


	private void onNewListRefreshed() {
		if (newLoyaltyPointsArray.size() == 0)
			showProgress();

		CommManager.getNewLoyaltyPointsList(LoyaltyPointsActivity.this, newLoyaltyPointsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetNewLoyaltyPointsList(int statusCode, String statusDescription, ArrayList<STLoyaltyPointInfo> loyaltyPointsList) {
				super.onGetNewLoyaltyPointsList(statusCode, statusDescription, loyaltyPointsList);

				stopProgress();
				newListView.stopRefresh();
				newListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < loyaltyPointsList.size(); i++) {
						boolean isExist = false;
						STLoyaltyPointInfo newItem = loyaltyPointsList.get(i);
						for (int j = 0; j < newLoyaltyPointsArray.size(); j++) {
							STLoyaltyPointInfo oldItem = newLoyaltyPointsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						newLoyaltyPointsArray.add(newItem);
					}

					newDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onNewItemSelected(int index) {
		if (index < 0 || index >= newLoyaltyPointsArray.size())
			return;

		STLoyaltyPointInfo item = newLoyaltyPointsArray.get(index);
	}



	public class RedeemViewHolder {
		public TextView title_textview = null;
		public TextView contents_textview = null;
		public TextView price_textview = null;
	}

	public class RedeemDataAdapter extends ArrayAdapter {
		public RedeemDataAdapter(Context context, List<STLoyaltyPointInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			RedeemViewHolder holder = null;
			STLoyaltyPointInfo itemInfo = redeemLoyaltyPointsArray.get(position);

			if (convertView == null) {
				holder = new RedeemViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_redeemed_loyalty_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.contents_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (RedeemViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.contents_textview.setText(itemInfo.remark);
			holder.price_textview.setText("" + itemInfo.loyalty_points + "\n" + getString(R.string.points));

			return convertView;
		}
	}


	private void onRedeemListRefreshed() {
		if (redeemLoyaltyPointsArray.size() == 0)
			showProgress();

		CommManager.getRedeemLoyaltyPointsList(LoyaltyPointsActivity.this, redeemLoyaltyPointsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetRedeemLoyaltyPointsList(int statusCode, String statusDescription, ArrayList<STLoyaltyPointInfo> loyaltyPointsList) {
				super.onGetRedeemLoyaltyPointsList(statusCode, statusDescription, loyaltyPointsList);

				stopProgress();
				redeemListView.stopRefresh();
				redeemListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < loyaltyPointsList.size(); i++) {
						boolean isExist = false;
						STLoyaltyPointInfo newItem = loyaltyPointsList.get(i);
						for (int j = 0; j < redeemLoyaltyPointsArray.size(); j++) {
							STLoyaltyPointInfo oldItem = redeemLoyaltyPointsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						redeemLoyaltyPointsArray.add(newItem);
					}

					redeemDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onRedeemItemSelected(int index) {
		if (index < 0 || index >= redeemLoyaltyPointsArray.size())
			return;

		STLoyaltyPointInfo item = redeemLoyaltyPointsArray.get(index);
	}
}
