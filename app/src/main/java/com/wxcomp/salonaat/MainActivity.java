package com.wxcomp.salonaat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/11/2016.
 */

public class MainActivity extends SuperActivity {
	private final int RC_GOOGLEPLUS_SIGN_IN = 1;

	private GoogleApiClient googleApiClient = null;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_main);
	}

	@Override
	public void initializeActivity() {
		Button login_facebook = (Button)findViewById(R.id.facebook_login_button);
		Button login_googleplus = (Button)findViewById(R.id.googleplus_login_button);
		Button login = (Button)findViewById(R.id.login_button);
		Button signup = (Button)findViewById(R.id.signup_button);

		login_facebook.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickLoginFacebook();
			}
		});
		login_googleplus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickeGooglePlus();
			}
		});
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedLogin();
			}
		});
		signup.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignup();
			}
		});

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
				.build();

		googleApiClient = new GoogleApiClient.Builder(this)
				.enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
					@Override
					public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
						Global.showToast(MainActivity.this, connectionResult.getErrorMessage());
					}
				})
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.build();
	}


	private void onClickLoginFacebook() {}

	private void onClickeGooglePlus() {
		googlePlusSignIn();
	}

	private void onClickedSignup() {
		pushNewActivityAnimatedFromBottom(SignUpActivity.class);
	}

	private void onClickedLogin() {
		pushNewActivityAnimatedFromBottom(LoginActivity.class);
	}


	private void googlePlusSignIn() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
		startActivityForResult(signInIntent, RC_GOOGLEPLUS_SIGN_IN);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RC_GOOGLEPLUS_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			handleGoogleSignInResult(result);
		}
	}

	private void handleGoogleSignInResult(GoogleSignInResult result) {
		if (result.isSuccess()) {
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();
			Global.showToast(MainActivity.this, acct.getEmail());
//			mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//			updateUI(true);
		} else {
			// Signed out, show unauthenticated UI.
			Global.showToast(MainActivity.this, "" + result.getStatus().getStatusCode());
//			updateUI(false);
		}
	}

}
