package com.wxcomp.salonaat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.data_structure.STNotificationInfo;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/14/2016.
 */

public class NotificationDetailActivity extends SuperActivity {
	public static final String EXTRA_NOTIFICATION_INF0 = "NotificationInfo";
	private STNotificationInfo notificationInfo = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_notification_detail);
	}

	@Override
	public void initializeActivity() {
		try {
			String notificationString = getIntent().getStringExtra(EXTRA_NOTIFICATION_INF0);
			notificationInfo = STNotificationInfo.decodeFromJSON(new JSONObject(notificationString));
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		TextView titleTextView = (TextView)findViewById(R.id.salon_title);
		titleTextView.setText(notificationInfo.title);

		TextView contentsTextView = (TextView)findViewById(R.id.txt_contents);
		contentsTextView.setText(notificationInfo.contents);

		CommManager.reviewNotification(NotificationDetailActivity.this, notificationInfo.uid, new CommDelegate() {
			@Override
			public void onReviewNotification(int statusCode, String statusDescription) {
				super.onReviewNotification(statusCode, statusDescription);

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Intent intent = new Intent();
					intent.putExtra(EXTRA_NOTIFICATION_INF0, notificationInfo.uid);
					setResult(RESULT_OK, intent);
				}
			}
		});
	}
}
