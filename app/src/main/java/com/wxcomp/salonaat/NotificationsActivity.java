package com.wxcomp.salonaat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STNotificationInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/14/2016.
 */

public class NotificationsActivity extends SuperActivity {
	private XListView notificationListView = null;
	private DataAdapter dataAdapter = null;
	private ArrayList<STNotificationInfo> notificationArray = new ArrayList<>();

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_notifications);
	}

	@Override
	public void initializeActivity() {
		dataAdapter = new DataAdapter(NotificationsActivity.this, notificationArray);

		notificationListView = (XListView)findViewById(R.id.notification_listview);
		notificationListView.setAdapter(dataAdapter);
		notificationListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				notificationArray.clear();
				onListRefreshed();
			}

			@Override
			public void onLoadMore() {
				onListRefreshed();
			}
		});
		notificationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i - 1);
			}
		});
		notificationListView.setPullLoadEnable(true);

		onListRefreshed();
	}


	public class ViewHolder {
		public TextView title_textview = null;
		public TextView contents_textview = null;
		public ImageView badge_imageview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STNotificationInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			STNotificationInfo itemInfo = notificationArray.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_notification_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.contents_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.badge_imageview = (ImageView)itemView.findViewById(R.id.img_badge);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.contents_textview.setText(itemInfo.contents);
			if (itemInfo.new_flag == 1)
				holder.badge_imageview.setVisibility(View.VISIBLE);
			else
				holder.badge_imageview.setVisibility(View.INVISIBLE);

			return convertView;
		}
	}


	private void onListRefreshed() {
		if (notificationArray.size() == 0)
			showProgress();

		CommManager.getNotificationsList(NotificationsActivity.this, notificationArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetNotificationListResult(int statusCode, String statusDescription, ArrayList<STNotificationInfo> notificationsList) {
				super.onGetNotificationListResult(statusCode, statusDescription, notificationsList);

				stopProgress();
				notificationListView.stopRefresh();
				notificationListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < notificationsList.size(); i++) {
						boolean isExist = false;
						STNotificationInfo newItem = notificationsList.get(i);
						for (int j = 0; j < notificationArray.size(); j++) {
							STNotificationInfo oldItem = notificationArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						notificationArray.add(newItem);
					}

					dataAdapter.notifyDataSetChanged();
				} else {
				}
			}
		});
	}


	private void onItemSelected(int index) {
		if (index < 0 || index >= notificationArray.size())
			return;

		STNotificationInfo notificationInfo = notificationArray.get(index);

		Bundle bundle = new Bundle();
		bundle.putString(NotificationDetailActivity.EXTRA_NOTIFICATION_INF0, notificationInfo.encodeToJSON().toString());

		pushNewActivityAnimatedFromRight(NotificationDetailActivity.class, bundle, 1);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == RESULT_OK) {
			String uid = data.getStringExtra(NotificationDetailActivity.EXTRA_NOTIFICATION_INF0);
			for (int i = 0; i < notificationArray.size(); i++) {
				STNotificationInfo info = notificationArray.get(i);
				if (info.uid.equals(uid)) {
					info.new_flag = 0;
					break;
				}
			}

			dataAdapter.notifyDataSetChanged();
		}
	}
}
