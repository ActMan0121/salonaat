package com.wxcomp.salonaat;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomDateTimePickerDialog;
import com.wxcomp.data_structure.STUserInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 12/14/2016.
 */

public class PersonalAccountEditProfileActivity extends SuperActivity {
	private STUserInfo userInfo = new STUserInfo();
	// User information

	private int birthYear = Calendar.getInstance().get(Calendar.YEAR);
	private int birthMonth = 1;
	private int birthDay = 1;

	// UI Controls
	private ImageButton photoButton = null;
	private TextView genderText = null;
	private TextView birthdayText = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_personal_account_edit_profile);
	}

	@Override
	public void initializeActivity() {
		photoButton = (ImageButton)findViewById(R.id.btn_photo);
		photoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPhoto();
			}
		});

		genderText = (TextView)findViewById(R.id.txt_gender);
		birthdayText = (TextView)findViewById(R.id.txt_age);

		Button genderButton = (Button)findViewById(R.id.btn_gender);
		genderButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedGender();
			}
		});

		Button birthdayButton = (Button)findViewById(R.id.btn_age);
		birthdayButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedBirthday();
			}
		});

		Button signupButton = (Button)findViewById(R.id.person_signup);
		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignUp();
			}
		});

		showProgress();
		CommManager.getUserProfile(PersonalAccountEditProfileActivity.this, new CommDelegate() {
			@Override
			public void onGetUserProfileResult(int statusCode, String statusDescription, STUserInfo userinfo) {
				super.onGetUserProfileResult(statusCode, statusDescription, userinfo);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					PersonalAccountEditProfileActivity.this.userInfo = userinfo;
					initUI();
				} else {
					Global.showToast(PersonalAccountEditProfileActivity.this, statusDescription);
				}
			}
		});
	}


	private void onClickedPhoto() {
		ActionSheet.createBuilder(PersonalAccountEditProfileActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.take_photo), getString(R.string.choose_photo))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Take photo
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
							intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							startActivityForResult(intent, 1);
						} else if (index == 1) {
							// Choose photo
							Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent, 2);
						}
					}
				}).show();
	}

	private void onClickedGender() {
		ActionSheet.createBuilder(PersonalAccountEditProfileActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.male), getString(R.string.female))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Male
							userInfo.gender = Const.GENDER_MALE;
							genderText.setText(R.string.male);
						} else if (index == 1) {
							// Female
							userInfo.gender = Const.GENDER_FEMALE;
							genderText.setText(R.string.female);
						}
					}
				}).show();
	}


	private void onClickedBirthday() {
		CustomDateTimePickerDialog dialog = new CustomDateTimePickerDialog(PersonalAccountEditProfileActivity.this);

		dialog.pickerMode = CustomDateTimePickerDialog.PICKER_MODE_YEARMONTHDAY;
		dialog.dateTimeChangedListener = new CustomDateTimePickerDialog.OnDateTimeChangedListener() {
			@Override
			public void dateTimeChanged(int year, int month, int day, int hour, int minute) {
				birthYear = year;
				birthMonth = month;
				birthDay = day;

				updateBirthday();
			}
		};

		dialog.year = birthYear;
		dialog.month = birthMonth;
		dialog.day = birthDay;

		dialog.show();
	}

	private void updateBirthday() {
		birthdayText.setText(String.format("%04d-%02d-%02d", birthYear, birthMonth, birthDay));
	}


	private void onClickedSignUp() {
		EditText firstNameEdit = (EditText)findViewById(R.id.edt_name);
		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		TextView birthdayText = (TextView)findViewById(R.id.txt_age);
		EditText locationEdit = (EditText)findViewById(R.id.edt_location);
		EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		EditText currentPasswordEdit = (EditText)findViewById(R.id.edt_current_password);
		EditText passwordEdit = (EditText)findViewById(R.id.edt_password);
		EditText confirmPasswordEdit = (EditText)findViewById(R.id.edt_confirm_password);
		EditText phoneEdit = (EditText)findViewById(R.id.edt_phone);
		EditText inviteCodeEdit = (EditText)findViewById(R.id.edt_invite_code);

		if (userInfo.image_url.isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_select_photo));
			return;
		}

		if (firstNameEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_first_name));
			return;
		}

		if (lastNameEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_last_name));
			return;
		}

		if (birthdayText.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_user_birthday));
			return;
		}

		if (userInfo.gender == Const.GENDER_NONE) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_user_gender));
			return;
		}

		if (locationEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_your_location));
			return;
		}

		if (emailEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_your_email));
			return;
		}

//		if (currentPasswordEdit.getText().toString().isEmpty()) {
//			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_current_password));
//			return;
//		}
//
//		if (passwordEdit.getText().toString().isEmpty()) {
//			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_new_password));
//			return;
//		}
//
//		if (confirmPasswordEdit.getText().toString().isEmpty()) {
//			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_password_again));
//			return;
//		}

//		if (!passwordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
//			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.password_not_match));
//			return;
//		}

		if (phoneEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalAccountEditProfileActivity.this, getString(R.string.please_input_user_phone_number));
			return;
		}

		showProgress();
		CommManager.updateProfile(PersonalAccountEditProfileActivity.this,
				userInfo.image_url,
				firstNameEdit.getText().toString(),
				lastNameEdit.getText().toString(),
				middleNameEdit.getText().toString(),
				0,
				birthdayText.getText().toString(),
				userInfo.gender,
				locationEdit.getText().toString(),
				emailEdit.getText().toString(),
				passwordEdit.getText().toString(),
				phoneEdit.getText().toString(),
				"",
				Global.loadSignType(),
				inviteCodeEdit.getText().toString(),
				new CommDelegate() {
					@Override
					public void onUpdateProfileResult(int statusCode, String statusDescription) {
						super.onUpdateProfileResult(statusCode, statusDescription);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							Global.showToast(PersonalAccountEditProfileActivity.this, R.string.your_profile_updated_successfully);
							finishFromLeft();
						} else {
							Global.showToast(PersonalAccountEditProfileActivity.this, statusDescription);
						}
					}
				});	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {
			// Take photo
			File f = new File(Environment.getExternalStorageDirectory().toString());
			for (File temp : f.listFiles()) {
				if (temp.getName().equals("temp.jpg")) {
					f = temp;
					break;
				}
			}

			try {
				updatePhoto(f.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == 2) {
			if (data == null)
				return;

			// Choose photo
			Uri selectedImage = data.getData();
			String[] filePath = { MediaStore.Images.Media.DATA };
			Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePath[0]);
			String picturePath = c.getString(columnIndex);
			c.close();

			updatePhoto(picturePath);
		}
	}




	private void updatePhoto(String filePath) {
		showProgress();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;

		Bitmap userImage = BitmapFactory.decodeFile(filePath, options);
		photoButton.setImageBitmap(userImage);

		String path = android.os.Environment
				.getExternalStorageDirectory()
				+ File.separator
				+ "Temp";
		File fileDir = new File(path);
		if (!fileDir.exists() || !fileDir.isDirectory())
			fileDir.mkdir();

		OutputStream outFile = null;
		File file = new File(path, "photo.jpg");
		if (file.exists())
			file.delete();

		String newFilePath = file.getAbsolutePath();

		try {
			file.createNewFile();

			outFile = new FileOutputStream(file);
			userImage.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
			outFile.flush();
			outFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		CommManager.uploadImage(PersonalAccountEditProfileActivity.this, newFilePath, new CommDelegate() {
			@Override
			public void onUploadImageResult(int statusCode, String statusDescription, String imageUrl) {
				super.onUploadImageResult(statusCode, statusDescription, imageUrl);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(PersonalAccountEditProfileActivity.this, "Success : " + imageUrl);
					PersonalAccountEditProfileActivity.this.userInfo.image_url = imageUrl;
				} else {
					Global.showToast(PersonalAccountEditProfileActivity.this, statusDescription);
				}
			}
		});
	}


	private void initUI() {
		EditText firstNameEdit = (EditText)findViewById(R.id.edt_name);
		firstNameEdit.setText(userInfo.first_name);

		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		lastNameEdit.setText(userInfo.last_name);

		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		middleNameEdit.setText(userInfo.middle_name);

		TextView birthdayText = (TextView)findViewById(R.id.txt_age);
		birthdayText.setText(userInfo.birthday);

		Date birthValue = Global.string2Date(userInfo.birthday, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(birthValue);
		birthYear = calendar.get(Calendar.YEAR);
		birthMonth = calendar.get(Calendar.MONTH) + 1;
		birthDay = calendar.get(Calendar.DAY_OF_MONTH);

		TextView genderText = (TextView)findViewById(R.id.txt_gender);
		if (userInfo.gender == Const.GENDER_MALE)
			genderText.setText("Male");
		else if (userInfo.gender == Const.GENDER_FEMALE)
			genderText.setText("Female");
		else if (userInfo.gender == Const.GENDER_UNISEX)
			genderText.setText("Unisex");

		EditText locationEdit = (EditText)findViewById(R.id.edt_location);
		locationEdit.setText(userInfo.location);

		EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		emailEdit.setText(userInfo.email);


//		EditText currentPasswordEdit = (EditText)findViewById(R.id.edt_current_password);
//		EditText passwordEdit = (EditText)findViewById(R.id.edt_password);
//		EditText confirmPasswordEdit = (EditText)findViewById(R.id.edt_confirm_password);

		EditText phoneEdit = (EditText)findViewById(R.id.edt_phone);
		phoneEdit.setText(userInfo.mobile);

		if (!userInfo.image_url.equals("")) {
			Picasso.with(PersonalAccountEditProfileActivity.this)
					.load(userInfo.image_url)
					.into(photoButton);
		}
	}

}
