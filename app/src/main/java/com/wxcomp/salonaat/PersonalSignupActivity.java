package com.wxcomp.salonaat;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.google.android.gms.plus.model.people.Person;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.controls.CustomDateTimePickerDialog;
import com.wxcomp.comm.CommManager;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

/**
 * Created by Administrator on 12/11/2016.
 */

public class PersonalSignupActivity extends SuperActivity {
	// User information
	private Bitmap userImage = null;
	private String imageUrl = "";

	private int gender = Const.GENDER_NONE;

	private int birthYear = Calendar.getInstance().get(Calendar.YEAR);
	private int birthMonth = 1;
	private int birthDay = 1;

	// UI Controls
	private ImageButton photoButton = null;
	private TextView genderText = null;
	private TextView birthdayText = null;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_personal_signup);
	}


	@Override
	public void initializeActivity() {
		photoButton = (ImageButton)findViewById(R.id.btn_photo);
		photoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPhoto();
			}
		});

		genderText = (TextView)findViewById(R.id.txt_gender);
		birthdayText = (TextView)findViewById(R.id.txt_age);

		Button genderButton = (Button)findViewById(R.id.btn_gender);
		genderButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedGender();
			}
		});

		Button birthdayButton = (Button)findViewById(R.id.btn_age);
		birthdayButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedBirthday();
			}
		});

		Button signupButton = (Button)findViewById(R.id.person_signup);
		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignUp();
			}
		});
	}

	private void onClickedPhoto() {
		ActionSheet.createBuilder(PersonalSignupActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.take_photo), getString(R.string.choose_photo))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Take photo
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
							intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							startActivityForResult(intent, 1);
						} else if (index == 1) {
							// Choose photo
							Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent, 2);
						}
					}
				}).show();
	}


	private void onClickedGender() {
		ActionSheet.createBuilder(PersonalSignupActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.male), getString(R.string.female))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// Male
							gender = Const.GENDER_MALE;
							genderText.setText(R.string.male);
						} else if (index == 1) {
							// Female
							gender = Const.GENDER_FEMALE;
							genderText.setText(R.string.female);
						}
					}
				}).show();
	}


	private void onClickedBirthday() {
		CustomDateTimePickerDialog dialog = new CustomDateTimePickerDialog(PersonalSignupActivity.this);

		dialog.pickerMode = CustomDateTimePickerDialog.PICKER_MODE_YEARMONTHDAY;
		dialog.dateTimeChangedListener = new CustomDateTimePickerDialog.OnDateTimeChangedListener() {
			@Override
			public void dateTimeChanged(int year, int month, int day, int hour, int minute) {
				birthYear = year;
				birthMonth = month;
				birthDay = day;

				updateBirthday();
			}
		};

		dialog.year = birthYear;
		dialog.month = birthMonth;
		dialog.day = birthDay;

		dialog.show();
	}

	private void updateBirthday() {
		birthdayText.setText(String.format("%04d-%02d-%02d", birthYear, birthMonth, birthDay));
	}


	private void onClickedSignUp() {
		EditText firstNameEdit = (EditText)findViewById(R.id.edt_name);
		EditText lastNameEdit = (EditText)findViewById(R.id.edt_last_name);
		EditText middleNameEdit = (EditText)findViewById(R.id.edt_middle_name);
		TextView birthdayText = (TextView)findViewById(R.id.txt_age);
		EditText locationEdit = (EditText)findViewById(R.id.edt_location);
		final EditText emailEdit = (EditText)findViewById(R.id.edt_email);
		final EditText passwordEdit = (EditText)findViewById(R.id.edt_password);
		EditText confirmPasswordEdit = (EditText)findViewById(R.id.edt_confirm_password);
		EditText phoneEdit = (EditText)findViewById(R.id.edt_phone);
		EditText inviteEdit = (EditText)findViewById(R.id.edt_invite_code);

		if (imageUrl == null || imageUrl.length() == 0) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_select_photo);
			return;
		}

		if (firstNameEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, getString(R.string.please_input_first_name));
			return;
		}

		if (lastNameEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, getString(R.string.please_input_last_name));
			return;
		}

		if (birthdayText.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, getString(R.string.please_input_user_birthday));
			return;
		}

		if (gender == Const.GENDER_NONE) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_user_gender);
			return;
		}

		if (locationEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_your_location);
			return;
		}

		if (emailEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_your_email);
			return;
		}

		if (passwordEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_user_password);
			return;
		}

		if (confirmPasswordEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_password_again);
			return;
		}

		if (!passwordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
			Global.showToast(PersonalSignupActivity.this, R.string.password_not_match);
			return;
		}

		if (phoneEdit.getText().toString().isEmpty()) {
			Global.showToast(PersonalSignupActivity.this, R.string.please_input_user_phone_number);
			return;
		}


		showProgress();
		CommManager.signUp(PersonalSignupActivity.this,
				imageUrl,
				firstNameEdit.getText().toString(),
				lastNameEdit.getText().toString(),
				middleNameEdit.getText().toString(),
				0,
				birthdayText.getText().toString(),
				gender == Const.GENDER_MALE ? 0 : 1,
				locationEdit.getText().toString(),
				emailEdit.getText().toString(),
				passwordEdit.getText().toString(),
				phoneEdit.getText().toString(),
				"",
				1,
				inviteEdit.getText().toString(),
				new CommDelegate() {
					@Override
					public void onSignupResult(int statusCode, String statusDescription, String user_id, String auth_token) {
						super.onSignupResult(statusCode, statusDescription, user_id, auth_token);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							Global.saveAccountType(Const.ACCOUNT_TYPE_PERSONAL);
							Global.saveUserID(user_id);
							Global.saveAuthToken(auth_token);
							Global.saveEmail(emailEdit.getText().toString());
							Global.savePassword(passwordEdit.getText().toString());

							Global.saveSignType(Const.SIGN_TYPE_NORMAL);

							// Go to home activity
							Intent intent = new Intent(PersonalSignupActivity.this, HomeActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							startActivity(intent);
							overridePendingTransition(R.anim.enter_from_bottom, R.anim.exit_to_top);
						} else {
							Global.showToast(PersonalSignupActivity.this, statusDescription);
						}
					}
				});
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {
			// Take photo
			File f = new File(Environment.getExternalStorageDirectory().toString());
			for (File temp : f.listFiles()) {
				if (temp.getName().equals("temp.jpg")) {
					f = temp;
					break;
				}
			}

			try {
				updatePhoto(f.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == 2) {
			if (data == null)
				return;

			// Choose photo
			Uri selectedImage = data.getData();
			String[] filePath = { MediaStore.Images.Media.DATA };
			Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePath[0]);
			String picturePath = c.getString(columnIndex);
			c.close();

			updatePhoto(picturePath);
		}
	}

	private void updatePhoto(String filePath) {
		showProgress();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;

		userImage = BitmapFactory.decodeFile(filePath, options);
		photoButton.setImageBitmap(userImage);

		String path = android.os.Environment
				.getExternalStorageDirectory()
				+ File.separator
				+ "Temp";
		File fileDir = new File(path);
		if (!fileDir.exists() || !fileDir.isDirectory())
			fileDir.mkdir();

		OutputStream outFile = null;
		File file = new File(path, "photo.jpg");
		if (file.exists())
			file.delete();

		String newFilePath = file.getAbsolutePath();

		try {
			file.createNewFile();

			outFile = new FileOutputStream(file);
			userImage.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
			outFile.flush();
			outFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		CommManager.uploadImage(PersonalSignupActivity.this, newFilePath, new CommDelegate() {
			@Override
			public void onUploadImageResult(int statusCode, String statusDescription, String imageUrl) {
				super.onUploadImageResult(statusCode, statusDescription, imageUrl);
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					Global.showToast(PersonalSignupActivity.this, "Uploading success");
					PersonalSignupActivity.this.imageUrl = imageUrl;
				} else {
					Global.showToast(PersonalSignupActivity.this, statusDescription);
				}
			}
		});
	}

}


