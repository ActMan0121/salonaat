package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.data_structure.STPromotionInfo;
import com.wxcomp.misc.Const;

import org.json.JSONObject;

/**
 * Created by Administrator on 12/14/2016.
 */

public class PromotionDetailActivity extends SuperActivity {
	public static final String EXTRA_PROMOTION_INFO = "PromotionInfo";
	private STPromotionInfo promotionInfo = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_promotion_detail);
	}

	@Override
	public void initializeActivity() {
		String promotionString = getIntent().getStringExtra(EXTRA_PROMOTION_INFO);
		try {
			promotionInfo = STPromotionInfo.decodeFromJSON(new JSONObject(promotionString));
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		ImageView promotionImageView = (ImageView)findViewById(R.id.promotion_image_view);
		if (promotionInfo.percent <= 20)
			promotionImageView.setImageResource(R.drawable.promotion20);
		else if (promotionInfo.percent <= 40)
			promotionImageView.setImageResource(R.drawable.promotion40);
		else
			promotionImageView.setImageResource(R.drawable.promotion60);

		TextView discountTextView = (TextView)findViewById(R.id.txt_discount);
		discountTextView.setText("" + promotionInfo.percent + "%\nOFF");


//		if (!promotionInfo.imageUrl.equals("")) {
//			Picasso.with(PromotionDetailActivity.this)
//					.load(promotionInfo.imageUrl)
//					.into(promotionImageView);
//		}

		TextView titleTextView = (TextView)findViewById(R.id.salon_title);
		titleTextView.setText(promotionInfo.title);

		TextView contentsTextView = (TextView)findViewById(R.id.txt_contents);
		contentsTextView.setText(promotionInfo.contents);
	}
}
