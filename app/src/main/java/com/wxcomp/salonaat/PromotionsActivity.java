package com.wxcomp.salonaat;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STPromotionInfo;
import com.wxcomp.misc.Const;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/14/2016.
 */

public class PromotionsActivity extends SuperActivity {
	private ArrayList<STPromotionInfo> promotionsArray = new ArrayList<>();
	private DataAdapter dataAdapter = null;
	private XListView listView = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_promotions);
	}


	@Override
	public void initializeActivity() {
		dataAdapter = new DataAdapter(PromotionsActivity.this, promotionsArray);

		listView = (XListView)findViewById(R.id.promotions_listview);
		listView.setAdapter(dataAdapter);
		listView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				promotionsArray.clear();
				onListRefreshed();
			}

			@Override
			public void onLoadMore() {
				onListRefreshed();
			}
		});
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i - 1);
			}
		});
		listView.setPullLoadEnable(true);

		onListRefreshed();
	}


	public class ViewHolder {
		public TextView title_textview = null;
		public TextView description_textview = null;
		public ImageView salon_imageview = null;
		public ImageView promotion_imageview = null;
		public TextView discount_textview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STPromotionInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			STPromotionInfo itemInfo = promotionsArray.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_promotion_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.description_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.salon_imageview = (ImageView)itemView.findViewById(R.id.salon_image_view);
				holder.promotion_imageview = (ImageView)itemView.findViewById(R.id.promotion_image_view);
				holder.discount_textview = (TextView)itemView.findViewById(R.id.discount_textview);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.description_textview.setText(itemInfo.contents);
			if (!itemInfo.imageUrl.isEmpty()) {
				Picasso.with(PromotionsActivity.this)
						.load(itemInfo.imageUrl)
						.into(holder.salon_imageview);
			}

			if (itemInfo.percent <= 20)
				holder.promotion_imageview.setImageResource(R.drawable.promotion20);
			else if (itemInfo.percent <= 40)
				holder.promotion_imageview.setImageResource(R.drawable.promotion40);
			else
				holder.promotion_imageview.setImageResource(R.drawable.promotion60);
			holder.discount_textview.setText("" + itemInfo.percent + "%\nOFF");

			return convertView;
		}
	}


	private void onListRefreshed() {
		if (promotionsArray.size() == 0)
			showProgress();

		CommManager.getPromotionsList(PromotionsActivity.this, promotionsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetPromotionsResult(int statusCode, String statusDescription, ArrayList<STPromotionInfo> promotionsList) {
				super.onGetPromotionsResult(statusCode, statusDescription, promotionsList);

				listView.stopRefresh();
				listView.stopLoadMore();
				stopProgress();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < promotionsList.size(); i++) {
						boolean isExist = false;
						STPromotionInfo newItem = promotionsList.get(i);
						for (int j = 0; j < promotionsArray.size(); j++) {
							STPromotionInfo oldItem = promotionsArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						promotionsArray.add(newItem);
					}

					dataAdapter.notifyDataSetChanged();
				} else {
				}
			}
		});
	}


	private void onItemSelected(int index) {
		if (index < 0 || index >= promotionsArray.size())
			return;

		STPromotionInfo promotionInfo = promotionsArray.get(index);

		Bundle bundle = new Bundle();
		bundle.putString(PromotionDetailActivity.EXTRA_PROMOTION_INFO, promotionInfo.encodeToJSON().toString());

		pushNewActivityAnimatedFromRight(PromotionDetailActivity.class, bundle);
	}

}
