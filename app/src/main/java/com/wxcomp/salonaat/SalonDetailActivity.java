package com.wxcomp.salonaat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STReviewInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.data_structure.STServiceInfo;
import com.wxcomp.misc.Global;

import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 12/12/2016.
 */

public class SalonDetailActivity extends LogoTitleActivity implements OnMapReadyCallback {
	public static final String EXTRA_SALON_INFO = "SalonInfo";

	private STSalonInfo salonInfo = null;

	private ImageView salonImageView = null;
	private Button bookButton = null;
	private TextView salonTitleTextView = null;
	private CustomRatingView ratingView = null;
	private TextView workingHoursTextView = null;
	private Button priceListButton = null;
	private Button favoriteButton = null;
	private Button callNowButton = null;

	private Button servicesTabButton = null;
	private Button aboutTabButton = null;
	private Button reviewTabButton = null;

	private RelativeLayout servicesLayouts = null;
	private RelativeLayout aboutLayouts = null;
	private RelativeLayout reviewLayouts = null;

	// Services tab
	private XListView servicesListView = null;

	// About tab
	private TextView descriptionTextView = null;
	private TextView addressTextView = null;
	private TextView phoneTextView = null;
	private RelativeLayout photosLayout = null;

	// Review tab
	private Button addReviewButton = null;
	private XListView reviewsListView = null;


	// Tab index
	private int tab_index = 0;

	private ServiceDataAdapter serviceDataAdapter = null;
	private ReviewDataAdapter reviewDataAdapter = null;

	private GoogleMap gmsMap = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_salon_detail);
	}


	@Override
	public void onMapReady(GoogleMap googleMap) {
		gmsMap = googleMap;

		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
			gmsMap.setMyLocationEnabled(true);
		}

		gmsMap.getUiSettings().setCompassEnabled(true);
		gmsMap.getUiSettings().setMyLocationButtonEnabled(true);
		gmsMap.getUiSettings().setZoomControlsEnabled(true);

		MarkerOptions markerOptions = new MarkerOptions()
				.position(new LatLng(salonInfo.latitude, salonInfo.longitude))
				.title(salonInfo.title)
				.snippet(salonInfo.contents);

		gmsMap.addMarker(markerOptions);
		gmsMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(salonInfo.latitude, salonInfo.longitude), 10));
	}


	@Override
	public void initializeActivity() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);


		Bundle bundle = getIntent().getExtras();
		String salonString = bundle.getString(EXTRA_SALON_INFO, "");
		try {
			salonInfo = STSalonInfo.decodeFromJSON(new JSONObject(salonString));
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}


		salonImageView = (ImageView)findViewById(R.id.salon_imageview);

		bookButton = (Button)findViewById(R.id.btn_book);
		bookButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedBookNow();
			}
		});

		favoriteButton = (Button)findViewById(R.id.btn_favorite);
		favoriteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFavorite();
			}
		});

		callNowButton = (Button)findViewById(R.id.btn_callnow);
		callNowButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedCallNow();
			}
		});

		salonTitleTextView = (TextView)findViewById(R.id.salon_title);
		ratingView = (CustomRatingView)findViewById(R.id.salon_rating_view);
		workingHoursTextView = (TextView)findViewById(R.id.txt_working_hour);

		priceListButton = (Button)findViewById(R.id.btn_price_list);
		priceListButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedServicesTab();
			}
		});

		servicesTabButton = (Button)findViewById(R.id.btn_tab_services);
		servicesTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedServicesTab();
			}
		});
		aboutTabButton = (Button)findViewById(R.id.btn_tab_about);
		aboutTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedAboutTab();
			}
		});
		reviewTabButton = (Button)findViewById(R.id.btn_tab_review);
		reviewTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedReviewTab();
			}
		});

		servicesLayouts = (RelativeLayout)findViewById(R.id.services_layout);
		aboutLayouts = (RelativeLayout)findViewById(R.id.about_layout);
		reviewLayouts = (RelativeLayout)findViewById(R.id.review_layout);

		// Services tab
		servicesListView = (XListView)findViewById(R.id.services_listview);
		servicesListView.setPullLoadEnable(true);
		servicesListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				onServiceListRefreshed();
			}
			@Override
			public void onLoadMore() {
				onServiceListRefreshed();
			}
		});
		servicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onServiceItemSelected(i - 1);
			}
		});

		// About tab
		descriptionTextView = (TextView)findViewById(R.id.txt_description);
		addressTextView = (TextView)findViewById(R.id.txt_address);
		phoneTextView = (TextView)findViewById(R.id.txt_phone);
		photosLayout = (RelativeLayout)findViewById(R.id.photos_layout);

		// Review tab
		addReviewButton = (Button)findViewById(R.id.btn_add_review);
		addReviewButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedAddReview();
			}
		});

		reviewsListView = (XListView)findViewById(R.id.reviews_list_view);
		reviewsListView.setPullLoadEnable(true);
		reviewsListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				onReviewListRefreshed();
			}

			@Override
			public void onLoadMore() {
				onReviewListRefreshed();
			}
		});
		reviewsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onReviewItemSelected(i - 1);
			}
		});

		showProgress();
		CommManager.getServiceDetailInfo(SalonDetailActivity.this,
				salonInfo.uid,
				new CommDelegate() {
					@Override
					public void onGetServiceDetailInfo(int statusCode, String statusDescription, STSalonInfo salonInfo) {
						super.onGetServiceDetailInfo(statusCode, statusDescription, salonInfo);
						stopProgress();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							SalonDetailActivity.this.salonInfo = salonInfo;
							initUI();
						} else {
							Global.showToast(SalonDetailActivity.this, statusDescription);
						}
					}
				});
	}


	private void onClickedFavorite() {
		showProgress();
		if (!salonInfo.favorite) {
			CommManager.takeFavorite(SalonDetailActivity.this, salonInfo.uid, new CommDelegate() {
				@Override
				public void onTakeFavoriteResult(int statusCode, String statusDescription) {
					super.onTakeFavoriteResult(statusCode, statusDescription);
					stopProgress();

					if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
						salonInfo.favorite = !salonInfo.favorite;
						updateFavoriteButton();
					} else {
						Global.showToast(SalonDetailActivity.this, statusDescription);
					}
				}
			});
		} else {
			CommManager.removeFavorite(SalonDetailActivity.this, salonInfo.uid, new CommDelegate() {
				@Override
				public void onRemoveFavoriteResult(int statusCode, String statusDescription) {
					super.onRemoveFavoriteResult(statusCode, statusDescription);
					stopProgress();

					if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
						salonInfo.favorite = !salonInfo.favorite;
						updateFavoriteButton();
					} else {
						Global.showToast(SalonDetailActivity.this, statusDescription);
					}
				}
			});
		}
	}

	private void updateFavoriteButton() {
		if (salonInfo.favorite)
			favoriteButton.setBackgroundResource(R.drawable.roundrect_pink_transparent);
		else
			favoriteButton.setBackgroundResource(R.drawable.roundrect_transparent_pink);
	}

	private void onClickedCallNow() {
		Global.openDialPad(SalonDetailActivity.this, salonInfo.phone);
	}

	private void initUI() {
		if (!salonInfo.imageUrl.isEmpty()) {
			Picasso.with(SalonDetailActivity.this)
					.load(salonInfo.imageUrl)
					.into(salonImageView);
		}

		salonTitleTextView.setText(salonInfo.title);
		ratingView.rating = salonInfo.rating;
		workingHoursTextView.setText(Global.formatString("%d:%02d-%d:%02d", salonInfo.startHour, salonInfo.startMinute, salonInfo.endHour, salonInfo.endMinute));

		tab_index = 0;
		updateTabSelection();


		serviceDataAdapter = new ServiceDataAdapter(SalonDetailActivity.this, salonInfo.servicesArray);
		servicesListView.setAdapter(serviceDataAdapter);

		reviewDataAdapter = new ReviewDataAdapter(SalonDetailActivity.this, salonInfo.reviewsArray);
		reviewsListView.setAdapter(reviewDataAdapter);

		updateFavoriteButton();

		descriptionTextView.setText(salonInfo.contents);
		addressTextView.setText(salonInfo.address);
		phoneTextView.setText(salonInfo.phone);

		Point screenSize = getScreenSize();

		int photoHeight = (screenSize.x - Global.dp2px(SalonDetailActivity.this, 48)) / 2;
		int photoMargin = 10;

		for (int i = 0; i < salonInfo.photosArray.size() / 2; i++) {
			LinearLayout rowLayout = new LinearLayout(photosLayout.getContext());
			rowLayout.setOrientation(LinearLayout.HORIZONTAL);
			RelativeLayout.LayoutParams row_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, photoHeight);
			row_layout_params.topMargin = i * (photoHeight + photoMargin);
			rowLayout.setLayoutParams(row_layout_params);

			String photoUrl1 = salonInfo.photosArray.get(i * 2);
			String photoUrl2 = salonInfo.photosArray.get(i * 2 + 1);

			ImageView imgView1 = new ImageView(photosLayout.getContext());
			LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
			layoutParams1.weight = 1;
			layoutParams1.rightMargin = photoMargin / 2;
			imgView1.setLayoutParams(layoutParams1);
			imgView1.setBackgroundResource(R.drawable.rect_transparent_gold);

			ImageView imgView2 = new ImageView(photosLayout.getContext());
			LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
			layoutParams2.weight = 1;
			layoutParams2.leftMargin = photoMargin / 2;
			imgView2.setLayoutParams(layoutParams2);
			imgView2.setBackgroundResource(R.drawable.rect_transparent_gold);

			imgView1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imgView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

			if (!photoUrl1.isEmpty()) {
				Picasso.with(SalonDetailActivity.this)
						.load(photoUrl1)
						.into(imgView1);
			}
			if (!photoUrl2.isEmpty()) {
				Picasso.with(SalonDetailActivity.this)
						.load(photoUrl2)
						.into(imgView2);
			}

			rowLayout.addView(imgView1);
			rowLayout.addView(imgView2);

			photosLayout.addView(rowLayout);
		}

		if (salonInfo.photosArray.size() % 2 == 1) {
			LinearLayout rowLayout = new LinearLayout(photosLayout.getContext());
			rowLayout.setOrientation(LinearLayout.HORIZONTAL);
			RelativeLayout.LayoutParams row_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, photoHeight);
			row_layout_params.topMargin = salonInfo.photosArray.size() / 2 * (photoHeight + photoMargin);
			rowLayout.setLayoutParams(row_layout_params);

			String photoUrl1 = salonInfo.photosArray.get(salonInfo.photosArray.size() - 1);

			ImageView imgView1 = new ImageView(photosLayout.getContext());
			LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
			layoutParams1.weight = 1;
			layoutParams1.rightMargin = photoMargin / 2;
			imgView1.setLayoutParams(layoutParams1);
			imgView1.setBackgroundResource(R.drawable.rect_transparent_gold);

			imgView1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

			if (!photoUrl1.isEmpty()) {
				Picasso.with(SalonDetailActivity.this)
						.load(photoUrl1)
						.into(imgView1);
			}

			ImageView imgView2 = new ImageView(photosLayout.getContext());
			LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
			layoutParams2.weight = 1;
			layoutParams2.leftMargin = photoMargin / 2;
			imgView2.setLayoutParams(layoutParams2);

			rowLayout.addView(imgView1);
			rowLayout.addView(imgView2);

			photosLayout.addView(rowLayout);
		}

	}


	private void updateTabSelection() {
		int selectedColor = getResources().getColor(R.color.gold_color);

		servicesLayouts.setVisibility(View.GONE);
		aboutLayouts.setVisibility(View.GONE);
		reviewLayouts.setVisibility(View.GONE);

		servicesTabButton.setBackgroundColor(Color.TRANSPARENT);
		aboutTabButton.setBackgroundColor(Color.TRANSPARENT);
		reviewTabButton.setBackgroundColor(Color.TRANSPARENT);

		if (tab_index == 0) {
			servicesLayouts.setVisibility(View.VISIBLE);
			servicesTabButton.setBackgroundColor(selectedColor);
		} else if (tab_index == 1) {
			aboutLayouts.setVisibility(View.VISIBLE);
			aboutTabButton.setBackgroundColor(selectedColor);
		} else if (tab_index == 2) {
			reviewLayouts.setVisibility(View.VISIBLE);
			reviewTabButton.setBackgroundColor(selectedColor);
		}
	}


	private void onClickedBookNow() {
		Bundle bundle = new Bundle();
		bundle.putString(BookingPaymentsActivity.EXTRA_SALON_INFO, salonInfo.encodeToJSON().toString());

		pushNewActivityAnimatedFromRight(BookingPaymentsActivity.class, bundle);
	}

	private void onClickedServicesTab() {
		tab_index = 0;
		updateTabSelection();
	}

	private void onClickedAboutTab() {
		tab_index = 1;
		updateTabSelection();
	}

	private void onClickedReviewTab() {
		tab_index = 2;
		updateTabSelection();
	}

	private void onClickedAddReview() {
		Bundle bundle = new Bundle();
		bundle.putString(AddReviewActivity.EXTRA_SALON_INFO, salonInfo.encodeToJSON().toString());

		pushNewActivityAnimatedFromRight(AddReviewActivity.class, bundle, AddReviewActivity.REQUEST_CODE_ADD_REVIEW);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == AddReviewActivity.REQUEST_CODE_ADD_REVIEW && resultCode == RESULT_OK) {
			String reviewString = data.getStringExtra(AddReviewActivity.EXTRA_REVIEW_INFO);
			try {
				JSONObject item = new JSONObject(reviewString);

				STReviewInfo reviewInfo = STReviewInfo.decodeFromJSON(item);
				salonInfo.reviewsArray.add(0, reviewInfo);

				reviewDataAdapter.notifyDataSetChanged();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}


	private void onServiceListRefreshed() {
		servicesListView.stopRefresh();
		servicesListView.stopLoadMore();
	}

	private void onServiceItemSelected(int index) {
		if (index < 0 || index >= salonInfo.servicesArray.size())
			return;
	}

	public class ServiceItemViewHolder {
		public TextView title_textview = null;
		public TextView description_textview = null;
		public TextView price_textview = null;
	}

	public class ServiceDataAdapter extends ArrayAdapter {
		public ServiceDataAdapter(Context context, List<STServiceInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ServiceItemViewHolder holder = null;
			STServiceInfo itemInfo = salonInfo.servicesArray.get(position);

			if (convertView == null) {
				holder = new ServiceItemViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_salon_service_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.description_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ServiceItemViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.description_textview.setText(itemInfo.contents);
			holder.price_textview.setText("AED\n" + itemInfo.price);

			return convertView;
		}
	}



	private void onReviewListRefreshed() {
		reviewsListView.stopRefresh();
		reviewsListView.stopLoadMore();
	}
	private void onReviewItemSelected(int index) {
		if (index < 0 || index >= salonInfo.reviewsArray.size())
			return;
	}

	public class ReviewItemViewHolder {
		public CustomRatingView ratingView = null;
		public TextView description_textview = null;
	}

	public class ReviewDataAdapter extends ArrayAdapter {
		public ReviewDataAdapter(Context context, List<STReviewInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ReviewItemViewHolder holder = null;
			STReviewInfo itemInfo = salonInfo.reviewsArray.get(position);

			if (convertView == null) {
				holder = new ReviewItemViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_salon_review_layout, null);

				holder.ratingView = (CustomRatingView) itemView.findViewById(R.id.rating_view);
				holder.ratingView.editable = false;
				holder.description_textview = (TextView)itemView.findViewById(R.id.txt_contents);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ReviewItemViewHolder)convertView.getTag();
			}

			holder.ratingView.rating = itemInfo.rating;
			holder.description_textview.setText(itemInfo.contents);

			return convertView;
		}
	}


}
