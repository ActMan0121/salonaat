package com.wxcomp.salonaat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STReviewInfo;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.data_structure.STServiceInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/12/2016.
 */

public class SalonsActivity extends SuperActivity {
	public static String EXTRA_SERVICE_MAIN_CATEGORY = "Main Category";
	public static String EXTRA_SERVICE_SUB_CATEGORY = "Sub Category";
	public static String EXTRA_SERVICE_SALON_TYPE = "Salon Type";

	private int service_category, service_type = Const.SERVICE_TYPE_SALON;
	private String sub_category_id = "";
	private int gender;

	private ArrayList<STSalonInfo> salonsArray = new ArrayList<>();

	// UI Controls
	private View type_view = null;

	private ImageButton maleTabButton = null;
	private ImageButton femaleTabButton = null;
	private ImageButton unisexTabButton = null;

	private Button spaTypeButton = null;
	private Button salonTypeButton = null;
	private Button specialistTypeButton = null;

	private XListView salonsListView = null;
	private DataAdapter adapter = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_salons);
	}

	@Override
	public void initializeActivity() {
		Bundle bundle = getIntent().getExtras();
		service_category = bundle.getInt(EXTRA_SERVICE_MAIN_CATEGORY, Const.SERVICE_CATEGORY_HAIR);
		sub_category_id = bundle.getString(EXTRA_SERVICE_SUB_CATEGORY, "");
		service_type = bundle.getInt(EXTRA_SERVICE_SALON_TYPE, Const.SERVICE_TYPE_SALON);
		gender = Global.loadGender();

		type_view = findViewById(R.id.type_tap_layout);

		maleTabButton = (ImageButton)findViewById(R.id.male_tab_button);
		femaleTabButton = (ImageButton)findViewById(R.id.female_tab_button);
		unisexTabButton = (ImageButton)findViewById(R.id.unisex_tab_button);

		maleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedMale();
			}
		});
		femaleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedFemale();
			}
		});
		unisexTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedUnisex();
			}
		});


		spaTypeButton = (Button)findViewById(R.id.spa_tab_button);
		salonTypeButton = (Button)findViewById(R.id.salon_tab_button);
		specialistTypeButton = (Button)findViewById(R.id.specialist_tab_button);

		spaTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSpa();
			}
		});
		salonTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSalon();
			}
		});
		specialistTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSpecialist();
			}
		});

		updateTabSelections();

		ImageButton searchCondButton = (ImageButton)findViewById(R.id.btn_search_cond);
		ImageButton searchButton = (ImageButton)findViewById(R.id.btn_search);
		searchCondButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSearchCond();
			}
		});
		searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSearch();
			}
		});


		salonsListView = (XListView) findViewById(R.id.salons_list_view);
		salonsListView.setDividerHeight(10);
		salonsListView.setDivider(new ColorDrawable(Color.WHITE));
		salonsListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				onListRefreshed();
			}
			@Override
			public void onLoadMore() {
				onListRefreshed();
			}
		});
		salonsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i - 1);
			}
		});
		salonsListView.setPullLoadEnable(true);

		adapter = new DataAdapter(SalonsActivity.this, salonsArray);
		salonsListView.setAdapter(adapter);

		onListRefreshed();
	}


	private void onSelectedMale() {
//		gender = Const.GENDER_MALE;
//		updateTabSelections();
//		Global.saveGender(gender);
//
//		salonsArray.clear();
//		onListRefreshed();
	}

	private void onSelectedFemale() {
//		gender = Const.GENDER_FEMALE;
//		updateTabSelections();
//		Global.saveGender(gender);
//
//		salonsArray.clear();
//		onListRefreshed();
	}

	private void onSelectedUnisex() {
//		gender = Const.GENDER_UNISEX;
//		updateTabSelections();
//		Global.saveGender(gender);
//
//		salonsArray.clear();
//		onListRefreshed();
	}



	private void onSelectedSpa() {
//		service_type = Const.SERVICE_TYPE_SPA;
//		updateTabSelections();
//
//		salonsArray.clear();
//		onListRefreshed();
	}

	private void onSelectedSalon() {
//		service_type = Const.SERVICE_TYPE_SALON;
//		updateTabSelections();
//
//		salonsArray.clear();
//		onListRefreshed();
	}

	private void onSelectedSpecialist() {
//		service_type = Const.SERVICE_TYPE_SPECIALIST;
//		updateTabSelections();
//
//		salonsArray.clear();
//		onListRefreshed();
	}



	private void updateTabSelections() {
		maleTabButton.setBackgroundColor(Color.TRANSPARENT);
		femaleTabButton.setBackgroundColor(Color.TRANSPARENT);
		unisexTabButton.setBackgroundColor(Color.TRANSPARENT);

		spaTypeButton.setBackgroundColor(Color.TRANSPARENT);
		salonTypeButton.setBackgroundColor(Color.TRANSPARENT);
		specialistTypeButton.setBackgroundColor(Color.TRANSPARENT);

		int selectedColor1 = getResources().getColor(R.color.gold_color);
		if (gender == Const.GENDER_MALE) {
			maleTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.male_color));
		} else if (gender == Const.GENDER_FEMALE) {
			femaleTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.female_color));
		} else if (gender == Const.GENDER_UNISEX) {
			unisexTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.unisex_color));
		}

		int selectedColor2 = Color.argb(160, 0, 0, 0);
		if (service_type == Const.SERVICE_TYPE_SPA) {
			spaTypeButton.setBackgroundColor(selectedColor2);
		} else if (service_type == Const.SERVICE_TYPE_SALON) {
			salonTypeButton.setBackgroundColor(selectedColor2);
		} else if (service_type == Const.SERVICE_TYPE_SPECIALIST) {
			specialistTypeButton.setBackgroundColor(selectedColor2);
		}
	}


	private void onClickedSearchCond() {
		Bundle bundle = new Bundle();
		bundle.putString("sub_category_id", sub_category_id);
		bundle.putInt("gender", gender);
		pushNewActivityAnimatedFromRight(SearchCondActivity.class, bundle);
	}


	private void onClickedSearch() {
		Bundle bundle = new Bundle();
		bundle.putString("sub_category_id", sub_category_id);
		bundle.putInt("gender", gender);
		pushNewActivityAnimatedFromRight(SearchResultActivity.class, bundle);
	}


	private void onListRefreshed() {
		CommManager.getServicesList(SalonsActivity.this, sub_category_id, salonsArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetServicesResult(int statusCode, String statusDescription, ArrayList<STSalonInfo> salonsList) {
				super.onGetServicesResult(statusCode, statusDescription, salonsList);
				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					salonsListView.stopRefresh();
					salonsListView.stopLoadMore();

					for (int i = 0; i < salonsList.size(); i++) {
						STSalonInfo salonInfo = salonsList.get(i);

						boolean isExist = false;
						for (int j = 0; j < salonsArray.size(); j++) {
							STSalonInfo oldInfo = salonsArray.get(j);
							if (oldInfo.uid.equals(salonInfo.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						salonsArray.add(salonInfo);
					}

					adapter.notifyDataSetChanged();
				} else {
				}
			}
		});
	}


	private void onItemSelected(int index) {
		if (index < 0 || index >= salonsArray.size())
			return;

		STSalonInfo itemInfo = salonsArray.get(index);

		Bundle bundle = new Bundle();
		bundle.putString(SalonDetailActivity.EXTRA_SALON_INFO, itemInfo.encodeToJSON().toString());

		pushNewActivityAnimatedFromRight(SalonDetailActivity.class, bundle);
	}


	public class ViewHolder {
		public TextView title_textview = null;
		public TextView description_textview = null;
		public ImageView salon_imageview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STSalonInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			com.wxcomp.data_structure.STSalonInfo itemInfo = salonsArray.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_salon_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.txt_salon_title);
				holder.description_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.salon_imageview = (ImageView)itemView.findViewById(R.id.img_salon);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.description_textview.setText(itemInfo.contents);
			if (!itemInfo.imageUrl.isEmpty()) {
				Picasso.with(SalonsActivity.this)
						.load(itemInfo.imageUrl)
						.into(holder.salon_imageview);
			}

			return convertView;
		}
	}
}
