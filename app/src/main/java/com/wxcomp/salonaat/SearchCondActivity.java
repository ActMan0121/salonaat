package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wxcomp.controls.CustomSeekbar;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/12/2016.
 */

public class SearchCondActivity extends LogoTitleActivity {
	private ImageButton resetImageButton = null;
	private ImageButton findImageButton = null;

	private Button resetButton = null;
	private Button findButton = null;

	private TextView distanceTextView = null;
	private CustomSeekbar distanceSeekbar = null;

	private TextView likesTextView = null;
	private CustomSeekbar likesSeekbar = null;

	private TextView priceTextView = null;
	private CustomSeekbar priceSeekbar = null;

	private TextView ratingsTextView = null;
	private CustomSeekbar ratingsSeekbar = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_search_cond);
	}


	@Override
	public void initializeActivity() {
		resetImageButton = (ImageButton)findViewById(R.id.btn_reset);
		resetImageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedReset();
			}
		});
		findImageButton = (ImageButton)findViewById(R.id.btn_find);
		findImageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFind();
			}
		});

		resetButton = (Button)findViewById(R.id.button_reset);
		resetButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedReset();
			}
		});
		findButton = (Button)findViewById(R.id.button_find);
		findButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFind();
			}
		});

		distanceTextView = (TextView)findViewById(R.id.distance_label);
		distanceSeekbar = (CustomSeekbar)findViewById(R.id.distance_spinner);
		distanceSeekbar.min_value = 1;
		distanceSeekbar.max_value = 100;
		distanceSeekbar.seekChangedListener = new CustomSeekbar.OnSeekChangedListener() {
			@Override
			public void onSeekChanged(int value) {
				distanceTextView.setText(getString(R.string.distance) + " : " + value + " km");
				Global.saveCondition_Distance(value);
			}
		};

		likesTextView = (TextView)findViewById(R.id.likes_label);
		likesSeekbar = (CustomSeekbar)findViewById(R.id.likes_spinner);
		likesSeekbar.min_value = 0;
		likesSeekbar.max_value = 10;
		likesSeekbar.seekChangedListener = new CustomSeekbar.OnSeekChangedListener() {
			@Override
			public void onSeekChanged(int value) {
				likesTextView.setText(Global.formatString(getString(R.string.likes) + " : %.1f", (float)value / 10));
				Global.saveCondition_Likes((float)value / 10);
			}
		};

		priceTextView = (TextView)findViewById(R.id.price_label);
		priceSeekbar = (CustomSeekbar)findViewById(R.id.price_spinner);
		priceSeekbar.min_value = 5;
		priceSeekbar.max_value = 1000;
		priceSeekbar.seekChangedListener = new CustomSeekbar.OnSeekChangedListener() {
			@Override
			public void onSeekChanged(int value) {
				priceTextView.setText(getString(R.string.price) + " : AED " + value);
				Global.saveCondition_Price(value);
			}
		};

		ratingsTextView = (TextView)findViewById(R.id.rating_label);
		ratingsSeekbar = (CustomSeekbar)findViewById(R.id.rating_spinner);
		ratingsSeekbar.min_value = 1;
		ratingsSeekbar.max_value = 5;
		ratingsSeekbar.seekChangedListener = new CustomSeekbar.OnSeekChangedListener() {
			@Override
			public void onSeekChanged(int value) {
				ratingsTextView.setText(getString(R.string.ratings) + " : %d" + value);
				Global.saveCondition_Ratings(value);
			}
		};

		loadConditions();
	}


	private void loadConditions() {
		distanceSeekbar.current_value = Global.loadCondition_Distance();
		distanceSeekbar.invalidate();
		distanceSeekbar.valueChanged();

		likesSeekbar.current_value = (int)(Global.loadCondition_Likes() * 10);
		likesSeekbar.invalidate();
		likesSeekbar.valueChanged();

		priceSeekbar.current_value = Global.loadCondition_Price();
		priceSeekbar.invalidate();
		priceSeekbar.valueChanged();

		ratingsSeekbar.current_value = Global.loadCondition_Ratings();
		ratingsSeekbar.invalidate();
		ratingsSeekbar.valueChanged();
	}

	private void onClickedReset() {
		Global.clearConditions();
		loadConditions();
	}


	private void onClickedFind() {
		Bundle bundle = getIntent().getExtras();
		pushNewActivityAnimatedFromRight(SearchResultActivity.class, bundle);
	}


}
