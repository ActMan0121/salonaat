package com.wxcomp.salonaat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.data_structure.STSalonInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 12/15/2016.
 */

public class SearchResultActivity extends LogoTitleActivity implements OnMapReadyCallback {
	int genderTabIndex = Const.GENDER_MALE;
	int viewTypeTabIndex = 0;

	private ArrayList<STSalonInfo> nearbyServicesForShow = new ArrayList<>();
	private ArrayList<STSalonInfo> nearbyServicesAll = new ArrayList<>();

	private DataAdapter dataAdapter = null;

	private ArrayList<Marker> markersArray = new ArrayList<>();
	private RelativeLayout mapLayout = null;
	private ListView nearbyListView = null;

	private ImageButton maleTabButton = null;
	private ImageButton femaleTabButton = null;
	private ImageButton unisexTabButton = null;

	private Button mapViewStyleButton = null;
	private Button listViewStyleButton = null;
	public GoogleMap gmsMap = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_search_results);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public void initializeActivity() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		Bundle bundle = getIntent().getExtras();
		genderTabIndex = bundle.getInt("gender", Const.GENDER_MALE);

		dataAdapter = new DataAdapter(SearchResultActivity.this, nearbyServicesForShow);

		nearbyListView = (ListView)findViewById(R.id.nearby_listview);
		nearbyListView.setAdapter(dataAdapter);
		nearbyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i);
			}
		});

		mapLayout = (RelativeLayout)findViewById(R.id.map_layout);

		maleTabButton = (ImageButton)findViewById(R.id.male_tab_button);
		maleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMaleTab();
			}
		});
		femaleTabButton = (ImageButton)findViewById(R.id.female_tab_button);
		femaleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFemaleTab();
			}
		});
		unisexTabButton = (ImageButton)findViewById(R.id.unisex_tab_button);
		unisexTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedUnisex();
			}
		});

		mapViewStyleButton = (Button)findViewById(R.id.map_style_button);
		mapViewStyleButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMapViewStyle();
			}
		});
		listViewStyleButton = (Button)findViewById(R.id.list_style_button);
		listViewStyleButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedListViewStyle();
			}
		});

		updateTabSelection();

		showProgress();
		CommManager.findNearbyServices(SearchResultActivity.this,
				Global.loadCondition_Distance(),
				Global.loadCondition_Likes(),
				Global.loadCondition_Price(),
				Global.loadCondition_Ratings(),
				bundle.getString("sub_category_id"),
				Global.loadCurrentLatitude(),
				Global.loadCurrentLongitude(),
				new CommDelegate() {
			@Override
			public void onFindServicesResult(int statusCode, String statusDescription, ArrayList<STSalonInfo> salonsList) {
				stopProgress();
				super.onFindServicesResult(statusCode, statusDescription, salonsList);

				nearbyServicesAll.clear();
				nearbyServicesForShow.clear();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					nearbyServicesAll.addAll(salonsList);
					nearbyServicesForShow.addAll(salonsList);
					refreshMarkers();
				} else {
				}
			}
		});
	}


	private void onClickedMaleTab() {
//		genderTabIndex = Const.GENDER_MALE;
//		updateTabSelection();
//		refreshMarkers();
	}

	private void onClickedFemaleTab() {
//		genderTabIndex = Const.GENDER_FEMALE;
//		updateTabSelection();
//		refreshMarkers();
	}

	private void onClickedUnisex() {
//		genderTabIndex = Const.GENDER_UNISEX;
//		updateTabSelection();
//		refreshMarkers();
	}

	private void onClickedMapViewStyle() {
		viewTypeTabIndex = 0;
		updateTabSelection();
	}

	private void onClickedListViewStyle() {
		viewTypeTabIndex = 1;
		updateTabSelection();
	}

	private void updateTabSelection() {
		nearbyServicesForShow.clear();
		for (int i = 0; i < nearbyServicesAll.size(); i++) {
			STSalonInfo serviceInfo = nearbyServicesAll.get(i);
			if (serviceInfo.gender == genderTabIndex)
				nearbyServicesForShow.add(serviceInfo);
		}
		dataAdapter.notifyDataSetChanged();


		maleTabButton.setBackgroundColor(Color.TRANSPARENT);
		femaleTabButton.setBackgroundColor(Color.TRANSPARENT);
		unisexTabButton.setBackgroundColor(Color.TRANSPARENT);

		if (genderTabIndex == Const.GENDER_MALE) {
			maleTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		} else if (genderTabIndex == Const.GENDER_FEMALE) {
			femaleTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		} else if (genderTabIndex == Const.GENDER_UNISEX) {
			unisexTabButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}


		mapViewStyleButton.setBackgroundColor(Color.TRANSPARENT);
		listViewStyleButton.setBackgroundColor(Color.TRANSPARENT);

		if (viewTypeTabIndex == 0)		// Map view
		{
			mapLayout.setVisibility(View.VISIBLE);
			nearbyListView.setVisibility(View.GONE);

			mapViewStyleButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}
		else							// List view
		{
			mapLayout.setVisibility(View.GONE);
			nearbyListView.setVisibility(View.VISIBLE);

			listViewStyleButton.setBackgroundColor(getResources().getColor(R.color.gold_color));
		}
	}


	private void refreshMarkers() {
		// Clearing markers on the map
		for (int i = 0; i < markersArray.size(); i++) {
			Marker markerItem = markersArray.get(i);
			markerItem.remove();
		}
		markersArray.clear();

		for (int i = 0; i < nearbyServicesForShow.size(); i++) {
			STSalonInfo item = nearbyServicesForShow.get(i);

			Bitmap icon = null;
			BitmapDescriptor descriptor = null;
			if (item.gender == Const.GENDER_MALE)
				icon = BitmapFactory.decodeResource(getResources(), R.drawable.placemark_male);
			else if (item.gender == Const.GENDER_FEMALE)
				icon = BitmapFactory.decodeResource(getResources(), R.drawable.placemark_female);
			else if (item.gender == Const.GENDER_UNISEX)
				icon = BitmapFactory.decodeResource(getResources(), R.drawable.placemark_unisex);

			int destWidth = icon.getWidth() / 4;
			int destHeight = icon.getHeight() / 4;
			if (destWidth < 30) {
				destHeight = destHeight * 30 / destWidth;
				destWidth = 30;
			}

			icon = Bitmap.createScaledBitmap(icon, destWidth, destHeight, true);
			descriptor = BitmapDescriptorFactory.fromBitmap(icon);

			MarkerOptions markerOptions = new MarkerOptions()
					.position(new LatLng(item.latitude, item.longitude))
					.title(item.title)
					.snippet(item.contents)
					.icon(descriptor);

			markersArray.add(gmsMap.addMarker(markerOptions));
		}
	}


	@Override
	public void onMapReady(GoogleMap googleMap) {
		gmsMap = googleMap;

		refreshMarkers();

		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
			gmsMap.setMyLocationEnabled(true);
		}

		gmsMap.getUiSettings().setCompassEnabled(true);
		gmsMap.getUiSettings().setMyLocationButtonEnabled(true);
		gmsMap.getUiSettings().setZoomControlsEnabled(true);

		updateCameraPosition();
	}

	private void updateCameraPosition() {
		if (Global.loadCurrentLatitude() == -1000) {
			TimerTask timerTask = new TimerTask() {
				@Override
				public void run() {
					updateCameraPosition();
				}
			};
			Timer timer = new Timer();
			timer.schedule(timerTask, 100);
			return;
		}

		gmsMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Global.loadCurrentLatitude(), Global.loadCurrentLongitude()), 10));
	}


	public class ViewHolder {
		public TextView title_textview = null;
		public TextView contents_textview = null;
		public TextView distance_textview = null;
		public ImageView salon_imageview = null;
	}


	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STSalonInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			STSalonInfo itemInfo = nearbyServicesForShow.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_nearby_salon_layout, null);

				holder.title_textview = (TextView) itemView.findViewById(R.id.txt_title);
				holder.contents_textview = (TextView) itemView.findViewById(R.id.txt_contents);
				holder.distance_textview = (TextView)itemView.findViewById(R.id.txt_distance);
				holder.salon_imageview = (ImageView) itemView.findViewById(R.id.img_salon);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.contents_textview.setText(itemInfo.contents);
			holder.distance_textview.setText(Global.getDistanceInMeters(Global.loadCurrentLatitude(),
					Global.loadCurrentLongitude(),
					itemInfo.latitude,
					itemInfo.longitude) + " m");
//			if (!itemInfo.imageUrl.isEmpty()) {
//				Picasso.with(SearchResultActivity.this)
//						.load(itemInfo.imageUrl)
//						.into(holder.salon_imageview);
//			}

			return convertView;
		}
	}


	private void onItemSelected(int index) {
		STSalonInfo serviceInfo = nearbyServicesForShow.get(index);

		Bundle bundle = new Bundle();
		bundle.putString(SalonDetailActivity.EXTRA_SALON_INFO, serviceInfo.encodeToJSON().toString());
		pushNewActivityAnimatedFromRight(SalonDetailActivity.class, bundle);
	}
}
