package com.wxcomp.salonaat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.data_structure.STGiftCardInfo;
import com.wxcomp.misc.Global;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/25/2016.
 */

public class SelectLoyaltyPointsActivity extends SuperActivity {
	public static final String EXTRA_SELECTED_CARDS = "CardsInfo";
	public static final String EXTRA_LOYALTY_POINTS = "LoyaltyPoints";

	private ArrayList<STGiftCardInfo> giftCardsInfo = new ArrayList<>();

	private int totalLoyaltyPoints = 300;
	private int selectedPoints = 0;

	private TextView totalPointsTextView = null;
	private EditText loyaltyPointsEdit = null;

	private ListView giftCardsListView = null;
	private DataAdapter dataAdapter = null;

	private Button doneButton = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_select_loyalty_points);
	}

	@Override
	public void initializeActivity() {
		String data = getIntent().getStringExtra(EXTRA_SELECTED_CARDS);
		try {
			JSONArray itemArray = new JSONArray(data);
			for (int i = 0; i < itemArray.length(); i++) {
				JSONObject item = itemArray.getJSONObject(i);
				giftCardsInfo.add(STGiftCardInfo.decodeFromJSON(item));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		selectedPoints = getIntent().getIntExtra(EXTRA_LOYALTY_POINTS, 0);


		totalPointsTextView = (TextView)findViewById(R.id.lbl_total_points);
		loyaltyPointsEdit = (EditText)findViewById(R.id.edt_loyalty_points);
		loyaltyPointsEdit.setText("" + selectedPoints);


		dataAdapter = new DataAdapter(SelectLoyaltyPointsActivity.this, giftCardsInfo);

		giftCardsListView = (ListView)findViewById(R.id.gift_cards_list_view);
		giftCardsListView.setAdapter(dataAdapter);
		giftCardsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i);
			}
		});

		doneButton = (Button)findViewById(R.id.btn_done);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedDone();
			}
		});

		showProgress();
		CommManager.getLoyaltyPoints(SelectLoyaltyPointsActivity.this, comm_delegate);
		CommManager.getPurchasedGiftCardsList(SelectLoyaltyPointsActivity.this, comm_delegate);
	}

	public class ViewHolder {
		public TextView title_textview = null;
		public ImageView gift_card_imageview = null;
		public TextView save_textview = null;
		public ImageView check_imageview = null;
	}

	public class DataAdapter extends ArrayAdapter {
		public DataAdapter(Context context, List<STGiftCardInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			STGiftCardInfo itemInfo = giftCardsInfo.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_gift_card_select_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.gift_card_imageview = (ImageView)itemView.findViewById(R.id.img_salon);
				holder.save_textview = (TextView)itemView.findViewById(R.id.txt_save);
				holder.check_imageview = (ImageView) itemView.findViewById(R.id.img_check);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			if (!itemInfo.image_url.isEmpty()) {
				Picasso.with(SelectLoyaltyPointsActivity.this)
						.load(itemInfo.image_url)
						.into(holder.gift_card_imageview);
			}
			holder.save_textview.setText("" + itemInfo.price);
			if (itemInfo.selected) {
				holder.check_imageview.setImageResource(R.drawable.check_sel);
			} else {
				holder.check_imageview.setImageResource(R.drawable.check_desel);
			}

			return convertView;
		}
	}


	private void onClickedDone() {
		String loyaltyPointsStr = loyaltyPointsEdit.getText().toString();
		try {
			selectedPoints = Integer.parseInt(loyaltyPointsStr);
		} catch (Exception ex) {
			ex.printStackTrace();
			Global.showToast(SelectLoyaltyPointsActivity.this, getString(R.string.please_input_numeric_value));
			return;
		}

		JSONArray itemsArray = new JSONArray();
		for (int i = 0; i < giftCardsInfo.size(); i++) {
			STGiftCardInfo item = giftCardsInfo.get(i);
			if (item.selected)
				itemsArray.put(item.encodeToJSON());
		}

		Intent intent = new Intent();
		intent.putExtra(EXTRA_LOYALTY_POINTS, selectedPoints);
		intent.putExtra(EXTRA_SELECTED_CARDS, itemsArray.toString());

		setResult(RESULT_OK, intent);
		finishFromLeft();
	}


	private void onItemSelected(int position) {
		giftCardsInfo.get(position).selected = !giftCardsInfo.get(position).selected;
		dataAdapter.notifyDataSetChanged();
	}


	private CommDelegate comm_delegate = new CommDelegate() {
		@Override
		public void onGetLoyaltyPointsResult(int statusCode, String statusDescription, int loyalty_points) {
			super.onGetLoyaltyPointsResult(statusCode, statusDescription, loyalty_points);
			stopProgress();

			if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
				SelectLoyaltyPointsActivity.this.totalLoyaltyPoints = loyalty_points;
				totalPointsTextView.setText("" + totalLoyaltyPoints);
			}
		}

		@Override
		public void onGetGiftCardsList(int statusCode, String statusDescription, ArrayList<STGiftCardInfo> giftCardsList) {
			super.onGetGiftCardsList(statusCode, statusDescription, giftCardsList);
			stopProgress();

			if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
				ArrayList<STGiftCardInfo> tempCardsArray = new ArrayList<>();
				for (int i = 0; i < giftCardsList.size(); i++) {
					STGiftCardInfo itemInfo = giftCardsList.get(i);

					for (int j = 0; j < giftCardsInfo.size(); j++) {
						STGiftCardInfo oldInfo = giftCardsInfo.get(j);
						if (oldInfo.uid.equals(itemInfo.uid)) {
							itemInfo.selected = true;
							break;
						}
					}

					tempCardsArray.add(itemInfo);
				}

				giftCardsInfo.clear();
				giftCardsInfo.addAll(tempCardsArray);
				dataAdapter.notifyDataSetChanged();
			}
		}
	};
}
