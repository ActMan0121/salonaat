package com.wxcomp.salonaat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wxcomp.data_structure.STServiceInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/13/2016.
 */

public class SelectServiceActivity extends SuperActivity {
	public static final String EXTRA_SELECTED_SERVICES = "selected_services";

	private ListView servicesListView = null;
	private ServiceDataAdapter dataAdapter = null;
	private ArrayList<STServiceInfo> selectedServices = new ArrayList<>();

	private Button doneButton = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_select_service);
	}

	@Override
	public void initializeActivity() {
		Bundle bundle = getIntent().getExtras();
		final String selectedServicesString = bundle.getString(EXTRA_SELECTED_SERVICES);

		try {
			JSONArray selectedServicesJSON = new JSONArray(selectedServicesString);
			for (int i = 0; i < selectedServicesJSON.length(); i++) {
				JSONObject item = selectedServicesJSON.getJSONObject(i);

				STServiceInfo serviceInfo = STServiceInfo.decodeFromJSON(item);
				selectedServices.add(serviceInfo);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}


		dataAdapter = new ServiceDataAdapter(SelectServiceActivity.this, selectedServices);
		servicesListView = (ListView)findViewById(R.id.services_list_view);
		servicesListView.setAdapter(dataAdapter);
		servicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i);
			}
		});

		doneButton = (Button)findViewById(R.id.btn_done);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				JSONArray jsonArray = new JSONArray();
				for (int i = 0; i < selectedServices.size(); i++) {
					STServiceInfo serviceInfo = selectedServices.get(i);
					if (serviceInfo.checked)
						jsonArray.put(serviceInfo.encodeToJSON());
				}

				intent.putExtra(EXTRA_SELECTED_SERVICES, jsonArray.toString());

				setResult(RESULT_OK, intent);
				finishFromLeft();
			}
		});
	}


	public class ServiceItemViewHolder {
		public ImageView checkImageView = null;
		public TextView title_textview = null;
		public TextView description_textview = null;
		public TextView price_textview = null;
	}

	public class ServiceDataAdapter extends ArrayAdapter {
		public ServiceDataAdapter(Context context, List<STServiceInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ServiceItemViewHolder holder = null;
			STServiceInfo itemInfo = selectedServices.get(position);

			if (convertView == null) {
				holder = new ServiceItemViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_select_service_layout, null);

				holder.title_textview = (TextView)itemView.findViewById(R.id.salon_title);
				holder.description_textview = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.price_textview = (TextView)itemView.findViewById(R.id.txt_price);
				holder.checkImageView = (ImageView) itemView.findViewById(R.id.img_check);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ServiceItemViewHolder)convertView.getTag();
			}

			holder.title_textview.setText(itemInfo.title);
			holder.description_textview.setText(itemInfo.contents);
			holder.price_textview.setText("AED\n" + itemInfo.price);
			if (itemInfo.checked)
				holder.checkImageView.setImageResource(R.drawable.check_sel);
			else
				holder.checkImageView.setImageResource(R.drawable.check_desel);

			return convertView;
		}
	}


	private void onItemSelected(int index) {
		selectedServices.get(index).checked = !selectedServices.get(index).checked;
		dataAdapter.notifyDataSetChanged();
	}


}
