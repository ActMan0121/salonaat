package com.wxcomp.salonaat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

/**
 * Created by Administrator on 12/14/2016.
 */

public class SettingsActivity extends SuperActivity {
	private TextView languageTextView = null;
	private TextView profileTextView = null;
	private TextView termCondTextView = null;
	private TextView feedbackTextView = null;
	private TextView aboutusTextView = null;
	private TextView policyTextView = null;
	private TextView agreementTextView = null;
	private TextView signoutTextView = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_settings);
	}


	@Override
	public void initializeActivity() {
		languageTextView = (TextView)findViewById(R.id.txt_language);
		languageTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedLanguage();
			}
		});

		profileTextView = (TextView)findViewById(R.id.txt_profile);
		profileTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedProfile();
			}
		});

		termCondTextView = (TextView)findViewById(R.id.txt_term_cond);
		termCondTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedTermsConditions();
			}
		});

		feedbackTextView = (TextView)findViewById(R.id.txt_feedback);
		feedbackTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedFeedback();
			}
		});

		aboutusTextView = (TextView)findViewById(R.id.txt_aboutus);
		aboutusTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedAboutUs();
			}
		});

		policyTextView = (TextView)findViewById(R.id.txt_privacy_policy);
		policyTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPrivacyPolicy();
			}
		});

		agreementTextView = (TextView)findViewById(R.id.txt_partner_agreement);
		agreementTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPartnerAgreement();
			}
		});

		signoutTextView = (TextView)findViewById(R.id.txt_signout);
		signoutTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedSignout();
			}
		});

		if (Global.loadLanguage() == Const.LANGUAGE_TYPE_ENGLISH) {
			languageTextView.setText(getString(R.string.english) + "     ");
		} else if (Global.loadLanguage() == Const.LANGUAGE_TYPE_ARABIC) {
			languageTextView.setText(getString(R.string.arabic) + "     ");
		}
	}

	private void onClickedLanguage() {
		ActionSheet.createBuilder(SettingsActivity.this, getSupportFragmentManager())
				.setCancelButtonTitle(R.string.cancel)
				.setOtherButtonTitles(getString(R.string.english), getString(R.string.arabic))
				.setCancelableOnTouchOutside(true)
				.setListener(new ActionSheet.ActionSheetListener() {
					@Override
					public void onDismiss(ActionSheet actionSheet, boolean isCancel) {}
					@Override
					public void onOtherButtonClick(ActionSheet actionSheet, int index) {
						if (index == 0) {
							// English
							Global.saveLanguage(Const.LANGUAGE_TYPE_ENGLISH);
							languageTextView.setText(getString(R.string.english) + "     ");
							Global.setLanguage(getApplicationContext(), "en");
						} else if (index == 1) {
							// Arabic
							Global.saveLanguage(Const.LANGUAGE_TYPE_ARABIC);
							languageTextView.setText(getString(R.string.arabic) + "     ");
							Global.setLanguage(getApplicationContext(), "ar");
						}
					}
				}).show();
	}

	private void onClickedProfile() {
		if (Global.loadAccountType() == Const.ACCOUNT_TYPE_PERSONAL) {
			pushNewActivityAnimatedFromRight(PersonalAccountEditProfileActivity.class);
		} else if (Global.loadAccountType() == Const.ACCOUNT_TYPE_BUSINESS) {
			pushNewActivityAnimatedFromRight(BusinessAccountEditProfileActivity.class);
		}
	}

	private void onClickedTermsConditions() {
		pushNewActivityAnimatedFromRight(TermsConditionsActivity.class);
	}

	private void onClickedFeedback() {
		pushNewActivityAnimatedFromRight(FeedbackActivity.class);
	}

	private void onClickedAboutUs() {
		pushNewActivityAnimatedFromRight(ContactUsActivity.class);
	}

	private void onClickedPrivacyPolicy() {
		pushNewActivityAnimatedFromRight(PrivacyPolicyActivity.class);
	}

	private void onClickedPartnerAgreement() {
		pushNewActivityAnimatedFromRight(PartnerAgreementActivity.class);
	}

	private void onClickedSignout() {
		AlertDialog alertDialog = new AlertDialog.Builder(SettingsActivity.this)
				.setTitle(R.string.alert_title_note)
				.setMessage(R.string.are_you_sure_to_sign_out)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
						intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(intent);
						overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
					}
				})
				.setNegativeButton(R.string.cancel, null)
				.create();
		alertDialog.show();
	}

}
