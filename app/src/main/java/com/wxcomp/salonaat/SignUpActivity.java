package com.wxcomp.salonaat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

/**
 * Created by Administrator on 12/11/2016.
 */

public class SignUpActivity extends SuperActivity {
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_signup);
	}

	@Override
	public void initializeActivity() {
		Button personalButton = (Button)findViewById(R.id.person_signup);
		Button vendorsButton = (Button)findViewById(R.id.vendor_signup);

		personalButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedPersonal();
			}
		});
		vendorsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedVendors();
			}
		});
	}

	private void onClickedPersonal() {
		pushNewActivityAnimatedFromRight(PersonalSignupActivity.class);
	}

	private void onClickedVendors() {
		pushNewActivityAnimatedFromRight(BusinessSignupActivity.class);
	}
}
