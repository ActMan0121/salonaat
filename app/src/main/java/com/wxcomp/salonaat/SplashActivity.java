package com.wxcomp.salonaat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.wxcomp.misc.Global;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends SuperActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_splash);
	}


	public void initializeActivity() {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						gotoNextActivity();
					}
				});
			}
		};

		Timer timer = new Timer();
		timer.schedule(task, 1000);
	}

	private void gotoNextActivity() {
		if (!Global.loadStartFlag()) {
			Intent intent = new Intent(SplashActivity.this, StartActivity.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
		} else {
			Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
		}

		finish();
	}
}
