package com.wxcomp.salonaat;

import android.*;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.controls.HorizontalPager;
import com.wxcomp.misc.Global;

import java.util.Locale;

/**
 * Created by Administrator on 12/11/2016.
 */

public class StartActivity extends SuperActivity {
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_start);
	}

	@Override
	public void initializeActivity() {
		ActivityCompat.requestPermissions(StartActivity.this,
				new String[]{android.Manifest.permission.CAMERA,
						android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
						android.Manifest.permission.READ_EXTERNAL_STORAGE},
				0);

		HorizontalPager scrollView = (HorizontalPager)findViewById(R.id.horizontal_pager);

		RelativeLayout itemLayout1 = new RelativeLayout(scrollView.getContext());
		RelativeLayout itemLayout2 = new RelativeLayout(scrollView.getContext());
		RelativeLayout itemLayout3 = new RelativeLayout(scrollView.getContext());
		RelativeLayout itemLayout4 = new RelativeLayout(scrollView.getContext());
		RelativeLayout itemLayout5 = new RelativeLayout(scrollView.getContext());

		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		itemLayout1.setLayoutParams(layoutParams);
		itemLayout2.setLayoutParams(layoutParams);
		itemLayout3.setLayoutParams(layoutParams);
		itemLayout4.setLayoutParams(layoutParams);
		itemLayout5.setLayoutParams(layoutParams);

		RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getScreenSize().y / 10);
		layoutParams3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams3.bottomMargin = (int)(layoutParams3.height * 1.8);

		ImageView imageView1 = new ImageView(itemLayout1.getContext());
		imageView1.setLayoutParams(layoutParams2);
		imageView1.setBackgroundResource(R.mipmap.splash_2);
		itemLayout1.addView(imageView1);

		ImageView imageView2 = new ImageView(itemLayout2.getContext());
		imageView2.setLayoutParams(layoutParams2);
		imageView2.setBackgroundResource(R.mipmap.splash_1);
		itemLayout2.addView(imageView2);

		ImageView imageView3 = new ImageView(itemLayout3.getContext());
		imageView3.setLayoutParams(layoutParams2);
		imageView3.setBackgroundResource(R.mipmap.splash_2);
		itemLayout3.addView(imageView3);

		ImageView imageView4 = new ImageView(itemLayout4.getContext());
		imageView4.setLayoutParams(layoutParams2);
		imageView4.setBackgroundResource(R.mipmap.splash_1);
		itemLayout4.addView(imageView4);

		ImageView imageView5 = new ImageView(itemLayout5.getContext());
		imageView5.setLayoutParams(layoutParams2);
		imageView5.setBackgroundResource(R.mipmap.splash_2);
		itemLayout5.addView(imageView5);

		ImageView imageView_Text1 = new ImageView(itemLayout1.getContext());
		ImageView imageView_Text2 = new ImageView(itemLayout2.getContext());
		ImageView imageView_Text3 = new ImageView(itemLayout3.getContext());
		ImageView imageView_Text4 = new ImageView(itemLayout4.getContext());
		ImageView imageView_Text5 = new ImageView(itemLayout5.getContext());

		imageView_Text1.setLayoutParams(layoutParams3);
		imageView_Text2.setLayoutParams(layoutParams3);
		imageView_Text3.setLayoutParams(layoutParams3);
		imageView_Text4.setLayoutParams(layoutParams3);
		imageView_Text5.setLayoutParams(layoutParams3);

		imageView_Text1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imageView_Text2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imageView_Text3.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imageView_Text4.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imageView_Text5.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

		imageView_Text1.setImageResource(R.drawable.text_trans);
		imageView_Text2.setImageResource(R.drawable.text2_trans);
		imageView_Text3.setImageResource(R.drawable.text_trans);
		imageView_Text4.setImageResource(R.drawable.text2_trans);
		imageView_Text5.setImageResource(R.drawable.text_trans);

		itemLayout1.addView(imageView_Text1);
		itemLayout2.addView(imageView_Text2);
		itemLayout3.addView(imageView_Text3);
		itemLayout4.addView(imageView_Text4);
		itemLayout5.addView(imageView_Text5);

		scrollView.addView(itemLayout1);
		scrollView.addView(itemLayout2);
		scrollView.addView(itemLayout3);
		scrollView.addView(itemLayout4);
		scrollView.addView(itemLayout5);

		scrollView.addOnScrollListener(new HorizontalPager.OnScrollListener() {
			@Override
			public void onScroll(int scrollX) {
			}

			@Override
			public void onViewScrollFinished(int currentPage) {
				ImageView pageDot1 = (ImageView)findViewById(R.id.page1);
				ImageView pageDot2 = (ImageView)findViewById(R.id.page2);
				ImageView pageDot3 = (ImageView)findViewById(R.id.page3);
				ImageView pageDot4 = (ImageView)findViewById(R.id.page4);
				ImageView pageDot5 = (ImageView)findViewById(R.id.page5);

				pageDot1.setImageResource(R.drawable.page_dot_desel);
				pageDot2.setImageResource(R.drawable.page_dot_desel);
				pageDot3.setImageResource(R.drawable.page_dot_desel);
				pageDot4.setImageResource(R.drawable.page_dot_desel);
				pageDot5.setImageResource(R.drawable.page_dot_desel);

				if (currentPage == 0) {
					pageDot1.setImageResource(R.drawable.page_dot_sel);
				} else if (currentPage == 1) {
					pageDot2.setImageResource(R.drawable.page_dot_sel);
				} else if (currentPage == 2) {
					pageDot3.setImageResource(R.drawable.page_dot_sel);
				} else if (currentPage == 3) {
					pageDot4.setImageResource(R.drawable.page_dot_sel);
				} else if (currentPage == 4) {
					pageDot5.setImageResource(R.drawable.page_dot_sel);
				}
			}
		});


		Button startButtonArabic = (Button)findViewById(R.id.get_started_arabic);
		Button startButtonEnglish = (Button)findViewById(R.id.get_started_english);

		startButtonArabic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedStartArabic();
			}
		});
		startButtonEnglish.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedStartEnglish();
			}
		});
	}


	private void onClickedStartArabic() {
		Global.setLanguage(getApplicationContext(), "ar");

		pushNewActivityNonanimted(MainActivity.class);
		finish();
	}


	private void onClickedStartEnglish() {
		Global.setLanguage(getApplicationContext(), "en");

		pushNewActivityNonanimted(MainActivity.class);
		finish();
	}

}
