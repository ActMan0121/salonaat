package com.wxcomp.salonaat;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.data_structure.STSubCategoryInfo;
import com.wxcomp.misc.Const;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/12/2016.
 */

public class SubCategoryActivity extends LogoTitleActivity {
	public static String EXTRA_CATEGORY = "ExtraCategory";

	private int service_category, service_type = Const.SERVICE_TYPE_SALON;
	private int gender;

	private ArrayList<STSubCategoryInfo> subCategoryInfos = new ArrayList<>();

	// UI Controls
	private View type_view = null;

	private ImageButton maleTabButton = null;
	private ImageButton femaleTabButton = null;
	private ImageButton unisexTabButton = null;

	private Button spaTypeButton = null;
	private Button salonTypeButton = null;
	private Button specialistTypeButton = null;

	private XListView subCategoryListView = null;
	private DataAdapter adapter = null;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_sub_category);
	}


	@Override
	public void initializeActivity() {
		Bundle bundle = getIntent().getExtras();
		service_category = bundle.getInt(EXTRA_CATEGORY, Const.SERVICE_CATEGORY_HAIR);
		gender = Global.loadGender();

		type_view = findViewById(R.id.type_tap_layout);

		maleTabButton = (ImageButton)findViewById(R.id.male_tab_button);
		femaleTabButton = (ImageButton)findViewById(R.id.female_tab_button);
		unisexTabButton = (ImageButton)findViewById(R.id.unisex_tab_button);

		maleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedMale();
			}
		});
		femaleTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedFemale();
			}
		});
		unisexTabButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedUnisex();
			}
		});


		spaTypeButton = (Button)findViewById(R.id.spa_tab_button);
		salonTypeButton = (Button)findViewById(R.id.salon_tab_button);
		specialistTypeButton = (Button)findViewById(R.id.specialist_tab_button);

		spaTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSpa();
			}
		});
		salonTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSalon();
			}
		});
		specialistTypeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSelectedSpecialist();
			}
		});

		updateTabSelections();


		subCategoryListView = (XListView) findViewById(R.id.subcategory_list_view);
		subCategoryListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				loadMore();
			}

			@Override
			public void onLoadMore() {
				loadMore();
			}
		});
		subCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onItemSelected(i - 1);
			}
		});
		subCategoryListView.setPullLoadEnable(true);

		adapter = new DataAdapter(SubCategoryActivity.this, subCategoryInfos);
		subCategoryListView.setAdapter(adapter);

		loadMore();
	}


	private void onSelectedMale() {
		gender = Const.GENDER_MALE;
		updateTabSelections();
		Global.saveGender(gender);

		loadMore();
	}

	private void onSelectedFemale() {
		gender = Const.GENDER_FEMALE;
		updateTabSelections();
		Global.saveGender(gender);

		loadMore();
	}

	private void onSelectedUnisex() {
		gender = Const.GENDER_UNISEX;
		updateTabSelections();
		Global.saveGender(gender);

		loadMore();
	}



	private void onSelectedSpa() {
		service_type = Const.SERVICE_TYPE_SPA;
		updateTabSelections();

		loadMore();
	}

	private void onSelectedSalon() {
		service_type = Const.SERVICE_TYPE_SALON;
		updateTabSelections();

		loadMore();
	}

	private void onSelectedSpecialist() {
		service_type = Const.SERVICE_TYPE_SPECIALIST;
		updateTabSelections();

		loadMore();
	}



	private void updateTabSelections() {
		maleTabButton.setBackgroundColor(Color.TRANSPARENT);
		femaleTabButton.setBackgroundColor(Color.TRANSPARENT);
		unisexTabButton.setBackgroundColor(Color.TRANSPARENT);

		spaTypeButton.setBackgroundColor(Color.TRANSPARENT);
		salonTypeButton.setBackgroundColor(Color.TRANSPARENT);
		specialistTypeButton.setBackgroundColor(Color.TRANSPARENT);

		int selectedColor1 = getResources().getColor(R.color.gold_color);

		if (gender == Const.GENDER_MALE) {
			maleTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.male_color));
		} else if (gender == Const.GENDER_FEMALE) {
			femaleTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.female_color));
		} else if (gender == Const.GENDER_UNISEX) {
			unisexTabButton.setBackgroundColor(selectedColor1);
			type_view.setBackgroundColor(getResources().getColor(R.color.unisex_color));
//			type_view.setBackgroundResource(R.drawable.gradient_unisex);
		}

		int selectedColor2 = Color.argb(160, 0, 0, 0);
		if (service_type == Const.SERVICE_TYPE_SPA) {
			spaTypeButton.setBackgroundColor(selectedColor2);
		} else if (service_type == Const.SERVICE_TYPE_SALON) {
			salonTypeButton.setBackgroundColor(selectedColor2);
		} else if (service_type == Const.SERVICE_TYPE_SPECIALIST) {
			specialistTypeButton.setBackgroundColor(selectedColor2);
		}
	}


	private void loadMore() {
		CommManager.getSubCategories(SubCategoryActivity.this,
				gender,
				service_category,
				service_type,
				new CommDelegate() {
					@Override
					public void onGetSubCategoriesResult(int statusCode, String statusDescription, ArrayList<STSubCategoryInfo> subCategoriesList) {
						super.onGetSubCategoriesResult(statusCode, statusDescription, subCategoriesList);
						subCategoryListView.stopRefresh();
						subCategoryListView.stopLoadMore();

						if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
							subCategoryInfos.clear();
							subCategoryInfos.addAll(subCategoriesList);

							adapter.notifyDataSetChanged();
						}
					}
				}
		);
	}


	private void onItemSelected(int index) {
		if (index < 0 || index >= subCategoryInfos.size())
			return;

		STSubCategoryInfo itemInfo = subCategoryInfos.get(index);

		Bundle bundle = new Bundle();
		bundle.putInt(SalonsActivity.EXTRA_SERVICE_MAIN_CATEGORY, service_category);
		bundle.putString(SalonsActivity.EXTRA_SERVICE_SUB_CATEGORY, itemInfo.uid);
		bundle.putInt(SalonsActivity.EXTRA_SERVICE_SALON_TYPE, service_type);

		pushNewActivityAnimatedFromRight(SalonsActivity.class, bundle);
	}


	public class ViewHolder {
		public TextView category_title_textview = null;
		public TextView service_count_textview = null;
		public ImageView background_imageview = null;
	}

	public class DataAdapter<STSubCategoryInfo> extends ArrayAdapter {
		public DataAdapter(Context context, List<Object> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			com.wxcomp.data_structure.STSubCategoryInfo itemInfo = subCategoryInfos.get(position);

			if (convertView == null) {
				holder = new ViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_sub_category_layout, null);

				holder.category_title_textview = (TextView)itemView.findViewById(R.id.txt_category_title);
				holder.service_count_textview = (TextView)itemView.findViewById(R.id.txt_count);
				holder.background_imageview = (ImageView)itemView.findViewById(R.id.img_back);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}

			holder.category_title_textview.setText(itemInfo.title);
			holder.service_count_textview.setText("" + itemInfo.service_count + " " + getString(R.string.services));
			if (!itemInfo.image_url.isEmpty()) {
				Picasso.with(SubCategoryActivity.this)
						.load(itemInfo.image_url)
						.into(holder.background_imageview);
			}

			return convertView;
		}
	}



}
