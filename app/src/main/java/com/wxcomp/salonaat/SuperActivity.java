package com.wxcomp.salonaat;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;
import com.wxcomp.comm.CommDelegate;
import com.wxcomp.comm.CommManager;
import com.wxcomp.controls.CustomProgressDialog;
import com.wxcomp.controls.CustomRatingView;
import com.wxcomp.controls.PullToRefresh.XListView;
import com.wxcomp.controls.Rotate3dAnimation;
import com.wxcomp.data_structure.STLiveFeedbackInfo;
import com.wxcomp.misc.Global;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 12/11/2016.
 */

public abstract class SuperActivity extends AppCompatActivity {
	public static ArrayList<STLiveFeedbackInfo> gLiveFeedbackArray = new ArrayList<>();

	// Menus and Animations
	public boolean isAnimating = false;

	public View parentLayout = null;
	public View leftMenu = null;
	public boolean leftMenuOpen = false;

	public View rightMenu = null;
	public XListView liveFeedListView = null;
	public LiveFeedDataAdapter liveFeedDataAdapter = null;
	public boolean rightMenuOpen = false;
	/////////////////////////////////////////////////////////////


	private long lastPressedTime = 0;
	private int animation_direction = 0;

	public static SuperActivity sharedInstance = null;

	public CustomProgressDialog prog_dlg = null;

	protected void onCreate(@Nullable Bundle savedInstanceState, int layout) {
		super.onCreate(savedInstanceState);
		setContentView(layout);

		sharedInstance = this;

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			animation_direction = bundle.getInt("ANIM_DIR");
		} else {
			animation_direction = getIntent().getIntExtra("ANIM_DIR", 0);
		}

		initializeSuperActivity();
		initializeActivity();

		initGoogleClients();

		if (gLiveFeedbackArray.size() == 0)
			onLiveFeedListRefreshed();
	}


	@Override
	protected void onResume() {
		super.onResume();
		dismissKeyboard();
	}

	@Override
	protected void onPause() {
		super.onPause();
		dismissKeyboard();
	}

	public void dismissKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}


	public void initializeSuperActivity() {
		// Initialize back button
		View backButton = findViewById(R.id.btn_back);
		if (backButton != null) {
			backButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickedBack();
				}
			});
		}

		// Initialize menu button
		View menuButton = findViewById(R.id.btn_menu);
		if (menuButton != null) {
			menuButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickedMenu(null);
				}
			});
		}

		// Initialize live feed button
		View feedButton = findViewById(R.id.btn_livefeed);
		if (feedButton != null) {
			feedButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickedFeed(null);
				}
			});
		}
	}



	private void onClickedBack() {
		if (animation_direction == R.anim.enter_from_bottom)
			finishFromTop();
		else if (animation_direction == R.anim.enter_from_right)
			finishFromLeft();
	}

	public interface OnMenuCloseListener {
		void onMenuClosed();
	}

	private void onClickedMenu(final OnMenuCloseListener listener) {
		if (isAnimating)
			return;

		if (leftMenu == null) {
			parentLayout = findViewById(R.id.parent_layout);
			parentLayout.setClickable(true);

			ViewGroup rootView = getRootView();
			rootView.setBackgroundResource(R.mipmap.background);
			ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

			leftMenu = getLayoutInflater().inflate(R.layout.left_menu_layout, null);
			leftMenu.setLayoutParams(params);

			View remain_view = leftMenu.findViewById(R.id.back_layout);
			remain_view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickedMenu(null);
				}
			});

			rootView.addView(leftMenu, 0);
			leftMenuOpen = false;

			initLeftMenu();
		}


		if (!leftMenuOpen) {
			// Showing menu
			Rotate3dAnimation animation = new Rotate3dAnimation(0, -30, parentLayout.getWidth(), parentLayout.getHeight() / 2, 0, false);
			animation.setDuration(300);
			animation.setFillAfter(true);
			animation.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					isAnimating = true;
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					if (!isAnimating)
						return;

					leftMenuOpen = true;
					isAnimating = false;

					leftMenu.bringToFront();
				}
				@Override
				public void onAnimationRepeat(Animation animation) {}
			});
			parentLayout.startAnimation(animation);
		} else {
			// hiding menu
			Rotate3dAnimation animation = new Rotate3dAnimation(-30, 0, parentLayout.getWidth(), parentLayout.getHeight() / 2, 0, false);
			animation.setDuration(300);
			animation.setFillAfter(true);
			animation.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					isAnimating = true;
					parentLayout.bringToFront();
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					if (!isAnimating)
						return;

					leftMenuOpen = false;
					isAnimating = false;

					if (listener != null)
						listener.onMenuClosed();
				}
				@Override
				public void onAnimationRepeat(Animation animation) {}
			});
			parentLayout.startAnimation(animation);
		}
	}


	private void onClickedFeed(final OnMenuCloseListener listener) {
		if (isAnimating)
			return;

		if (rightMenu == null) {
			ViewGroup rootView = getRootView();
			ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

			rightMenu = getLayoutInflater().inflate(R.layout.right_menu_layout, null);
			rightMenu.setVisibility(View.INVISIBLE);
			rightMenu.setLayoutParams(params);

			View remain_view = rightMenu.findViewById(R.id.back_layout);
			remain_view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickedFeed(null);
				}
			});

			rootView.addView(rightMenu);
			rightMenuOpen = false;

			initRightMenu();
		}


		if (!rightMenuOpen) {
			// Showing menu
			Animation anim = AnimationUtils.loadAnimation(SuperActivity.this, R.anim.enter_from_right);
			anim.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					rightMenu.setVisibility(View.VISIBLE);
					isAnimating = true;
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					if (!isAnimating)
						return;

					rightMenu.clearAnimation();
					rightMenuOpen = true;
					isAnimating = false;
				}

				@Override
				public void onAnimationRepeat(Animation animation) {}
			});
			rightMenu.startAnimation(anim);
		} else {
			// hiding menu
			Animation anim = AnimationUtils.loadAnimation(SuperActivity.this, R.anim.exit_to_right);
			anim.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					isAnimating = true;
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					if (!isAnimating)
						return;

					rightMenu.setVisibility(View.INVISIBLE);
					rightMenu.clearAnimation();

					rightMenuOpen = false;
					isAnimating = false;

					if (listener != null)
						listener.onMenuClosed();
				}
				@Override
				public void onAnimationRepeat(Animation animation) {}
			});
			rightMenu.startAnimation(anim);
		}
	}


	private ViewGroup getRootView() {
		return (ViewGroup)((ViewGroup)this.findViewById(android.R.id.content)).getChildAt(0);
	}


	public abstract void initializeActivity();


	public static Point getScreenSize() {
		Point screenSize = new Point(0, 0);

		Display display = sharedInstance.getWindowManager().getDefaultDisplay();
		display.getSize(screenSize);

		return screenSize;
	}


	public static int getStatusBarHeight() {
		Rect rect = new Rect();

		Window window = sharedInstance.getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rect);

		int statusBarHeight = rect.top;

		return statusBarHeight;
	}

	public static int getTitleBarHeight() {
		Rect rect = new Rect();

		Window window = sharedInstance.getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rect);

		int statusBarHeight = rect.top;
		int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
		int titleBarHeight= contentViewTop - statusBarHeight;

		return titleBarHeight;
	}


	public void pushNewActivityAnimatedWithBundle(Class destinationClass, int anim_enter_id, int anim_exit_id, Bundle bundle, int requestCode) {
		Intent intent = new Intent(SuperActivity.this, destinationClass);
		if (bundle != null)
			intent.putExtras(bundle);

		startActivityForResult(intent, requestCode);

		overridePendingTransition(anim_enter_id, anim_exit_id);
	}

	public void pushNewActivityAnimatedFromRight(Class destinationClass, Bundle bundle, int requestCode) {
		if (bundle == null)
			bundle = new Bundle();
		bundle.putInt("ANIM_DIR", R.anim.enter_from_right);

		pushNewActivityAnimatedWithBundle(destinationClass, R.anim.enter_from_right, R.anim.exit_to_left, bundle, requestCode);
	}

	public void pushNewActivityAnimatedFromRight(Class destinationClass, Bundle bundle) {
		if (bundle == null)
			bundle = new Bundle();
		bundle.putInt("ANIM_DIR", R.anim.enter_from_right);

		pushNewActivityAnimatedWithBundle(destinationClass, R.anim.enter_from_right, R.anim.exit_to_left, bundle, -1);
	}

	public void pushNewActivityAnimatedFromRight(Class destinationClass) {
		Bundle bundle = new Bundle();
		bundle.putInt("ANIM_DIR", R.anim.enter_from_right);

		pushNewActivityAnimatedWithBundle(destinationClass, R.anim.enter_from_right, R.anim.exit_to_left, bundle, -1);
	}

	public void pushNewActivityAnimatedFromBottom(Class destinationClass, Bundle bundle) {
		if (bundle == null)
			bundle = new Bundle();
		bundle.putInt("ANIM_DIR", R.anim.enter_from_bottom);

		pushNewActivityAnimatedWithBundle(destinationClass, R.anim.enter_from_bottom, R.anim.exit_to_top, bundle, -1);
	}

	public void pushNewActivityAnimatedFromBottom(Class destinationClass) {
		Bundle bundle = new Bundle();
		bundle.putInt("ANIM_DIR", R.anim.enter_from_bottom);
		pushNewActivityAnimatedWithBundle(destinationClass, R.anim.enter_from_bottom, R.anim.exit_to_top, bundle, -1);
	}


	public void pushNewActivityNonanimted(Class destinationClass) {
		pushNewActivityNonanimtedWithBundle(destinationClass, null);
	}

	public void pushNewActivityNonanimtedWithBundle(Class destinationClass, Bundle bundle) {
		Intent intent = new Intent(SuperActivity.this, destinationClass);
		if (bundle != null)
			intent.putExtras(bundle);

		startActivity(intent);
		overridePendingTransition(0, 0);
	}


	public void finishFromTop() {
		finish();
		overridePendingTransition(R.anim.enter_from_top, R.anim.exit_to_bottom);
	}

	public void finishFromLeft() {
		finish();
		overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isTaskRoot() && Calendar.getInstance().getTimeInMillis() - lastPressedTime > 3000) {
				Global.showToast(SuperActivity.this, getString(R.string.tap_again_to_exit));
				lastPressedTime = Calendar.getInstance().getTimeInMillis();
				return false;
			} else {
				onClickedBack();
				return super.onKeyDown(keyCode, event);
			}
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}


	private void initLeftMenu() {
		if (leftMenu == null)
			return;

		TextView itemHome = (TextView)leftMenu.findViewById(R.id.item_home);
		itemHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						gotoHomeActivity();
					}
				});
			}
		});

		TextView itemPromotions = (TextView)leftMenu.findViewById(R.id.item_promotions);
		itemPromotions.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof PromotionsActivity)) {
							Intent intent = new Intent(SuperActivity.this, PromotionsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemBookingHistory = (TextView)leftMenu.findViewById(R.id.item_booking_history);
		itemBookingHistory.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof BookingHistoryActivity)) {
							Intent intent = new Intent(SuperActivity.this, BookingHistoryActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemNotifications = (TextView)leftMenu.findViewById(R.id.item_notifications);
		itemNotifications.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof NotificationsActivity)) {
							Intent intent = new Intent(SuperActivity.this, NotificationsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemMyLoyaltyPoints = (TextView)leftMenu.findViewById(R.id.item_loyalty_points);
		itemMyLoyaltyPoints.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof LoyaltyPointsActivity)) {
							Intent intent = new Intent(SuperActivity.this, LoyaltyPointsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemGiftCards = (TextView)leftMenu.findViewById(R.id.item_gift_cards);
		itemGiftCards.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof GiftCardsActivity)) {
							Intent intent = new Intent(SuperActivity.this, GiftCardsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemFavourites = (TextView)leftMenu.findViewById(R.id.item_favourites);
		itemFavourites.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof FavoritesActivity)) {
							Intent intent = new Intent(SuperActivity.this, FavoritesActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemInviteFriends = (TextView)leftMenu.findViewById(R.id.item_invite_friends);
		itemInviteFriends.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof InviteFriendsActivity)) {
							Intent intent = new Intent(SuperActivity.this, InviteFriendsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemSettings = (TextView)leftMenu.findViewById(R.id.item_settings);
		itemSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof SettingsActivity)) {
							Intent intent = new Intent(SuperActivity.this, SettingsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemFeedback = (TextView)leftMenu.findViewById(R.id.item_feedback);
		itemFeedback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof FeedbackActivity)) {
							Intent intent = new Intent(SuperActivity.this, FeedbackActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});

		TextView itemContactUs = (TextView)leftMenu.findViewById(R.id.item_contact_us);
		itemContactUs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickedMenu(new OnMenuCloseListener() {
					@Override
					public void onMenuClosed() {
						if (!(SuperActivity.this instanceof ContactUsActivity)) {
							Intent intent = new Intent(SuperActivity.this, ContactUsActivity.class);
							intent.putExtra("ANIM_DIR", R.anim.enter_from_right);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
						}
					}
				});
			}
		});
	}

	private void initRightMenu() {
		liveFeedDataAdapter = new LiveFeedDataAdapter(SuperActivity.this, gLiveFeedbackArray);

		liveFeedListView = (XListView)findViewById(R.id.live_feed_listview);
		liveFeedListView.setBlackBackground(false);
		liveFeedListView.setAdapter(liveFeedDataAdapter);
		liveFeedListView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				gLiveFeedbackArray.clear();
				onLiveFeedListRefreshed();
			}
			@Override
			public void onLoadMore() {
				gLiveFeedbackArray.clear();
				onLiveFeedListRefreshed();
			}
		});
		liveFeedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				onLiveFeedItemSelected(i - 1);
			}
		});
		liveFeedListView.setPullLoadEnable(true);
	}


	public void gotoHomeActivity() {
		if (SuperActivity.this instanceof HomeActivity) {
			return;
		} else {
			Intent intent = new Intent(SuperActivity.this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
		}
	}



	public class LiveFeedViewHolder {
		public TextView contentsTextView = null;
		public CustomRatingView ratingView = null;
		public ImageView userImageView = null;
	}

	public class LiveFeedDataAdapter extends ArrayAdapter {
		public LiveFeedDataAdapter(Context context, List<STLiveFeedbackInfo> objects) {
			super(context, 0, objects);
		}

		@NonNull
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LiveFeedViewHolder holder = null;
			STLiveFeedbackInfo itemInfo = gLiveFeedbackArray.get(position);

			if (convertView == null) {
				holder = new LiveFeedViewHolder();
				View itemView = getLayoutInflater().inflate(R.layout.item_live_feed_layout, null);

				holder.contentsTextView = (TextView)itemView.findViewById(R.id.txt_contents);
				holder.ratingView = (CustomRatingView) itemView.findViewById(R.id.rating_view);
				holder.ratingView.editable = false;
				holder.userImageView = (ImageView)itemView.findViewById(R.id.img_user);

				convertView = itemView;
				convertView.setTag(holder);
			} else {
				holder = (LiveFeedViewHolder)convertView.getTag();
			}

			holder.contentsTextView.setText(itemInfo.contents);
			holder.ratingView.rating = itemInfo.rating;
			if (!itemInfo.user_image_url.isEmpty()) {
				Picasso.with(SuperActivity.this)
						.load(itemInfo.user_image_url)
						.into(holder.userImageView);
			}

			return convertView;
		}
	}


	private void onLiveFeedListRefreshed() {
		CommManager.getLiveFeedList(SuperActivity.this, gLiveFeedbackArray.size() / 10, new CommDelegate() {
			@Override
			public void onGetLiveFeedResult(int statusCode, String statusDescription, ArrayList<STLiveFeedbackInfo> liveFeedbackList) {
				super.onGetLiveFeedResult(statusCode, statusDescription, liveFeedbackList);

				if (liveFeedListView == null)
					return;

				liveFeedListView.stopRefresh();
				liveFeedListView.stopLoadMore();

				if (statusCode == CommManager.STATUS_CODE_SUCCESS) {
					for (int i = 0; i < liveFeedbackList.size(); i++) {
						STLiveFeedbackInfo newItem = liveFeedbackList.get(i);
						boolean isExist = false;
						for (int j = 0; j < gLiveFeedbackArray.size(); j++) {
							STLiveFeedbackInfo oldItem = gLiveFeedbackArray.get(j);
							if (newItem.uid.equals(oldItem.uid)) {
								isExist = true;
								break;
							}
						}

						if (isExist)
							continue;

						gLiveFeedbackArray.add(newItem);
					}

					liveFeedDataAdapter.notifyDataSetChanged();
				}
			}
		});
	}


	private void onLiveFeedItemSelected(final int index) {
		if (index < 0 || index >= gLiveFeedbackArray.size())
			return;

		onClickedFeed(new OnMenuCloseListener() {
			@Override
			public void onMenuClosed() {
				STLiveFeedbackInfo itemInfo = gLiveFeedbackArray.get(index);
				if (!(SuperActivity.this instanceof LiveFeedDetailActivity)) {
					Bundle bundle = new Bundle();
					bundle.putString(LiveFeedDetailActivity.EXTRA_FEED_INFO, itemInfo.encodeToJSON().toString());
					pushNewActivityAnimatedFromRight(LiveFeedDetailActivity.class, bundle);
				} else {
					LiveFeedDetailActivity selfActivity = (LiveFeedDetailActivity)SuperActivity.this;
					selfActivity.liveFeedInfo = itemInfo;
					selfActivity.updateContents();
				}
			}
		});

	}

	public void showProgress() {
		showProgress(getString(R.string.loading));
	}

	public void showProgress(String message) {
		if (prog_dlg == null) {
			prog_dlg = new CustomProgressDialog(SuperActivity.this);
		}

		prog_dlg.setMessage(message);
		prog_dlg.show();
	}

	public void stopProgress() {
		if (prog_dlg != null)
			prog_dlg.dismiss();
	}



	private void startLocationUpdates() {
		if (googleApiClient == null)
			return;

		if (locationRequest == null) {
			locationRequest = new LocationRequest();
			locationRequest.setInterval(100);
		}

		if (locationListener == null) {
			locationListener = new LocationListener() {
				@Override
				public void onLocationChanged(Location location) {
					Global.saveCurrentLatitude(location.getLatitude());
					Global.saveCurrentLongitude(location.getLongitude());

					if (locationUpdated)
						return;

					locationUpdated = true;
				}
			};
		}

		if (ContextCompat.checkSelfPermission(SuperActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
			LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
		} else {
			ActivityCompat.requestPermissions(SuperActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
		}
	}


	private void stopLocationUpdates() {
		if (googleApiClient == null)
			return;

		if (ContextCompat.checkSelfPermission(SuperActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
			LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener);
		}
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			if (ContextCompat.checkSelfPermission(SuperActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
				LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
			}
		}
	}

	private void initGoogleClients() {
		if (SuperActivity.this instanceof SplashActivity)
			return;

		if (googleApiClient == null) {
			googleApiClient = new GoogleApiClient.Builder(SuperActivity.this)
					.addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
						@Override
						public void onConnected(@Nullable Bundle bundle) {
							startLocationUpdates();
						}

						@Override
						public void onConnectionSuspended(int i) {
						}
					})
					.addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
						@Override
						public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
						}
					})
					.addApi(LocationServices.API)
					.build();
			googleApiClient.connect();
		}
	}


	public static boolean locationUpdated = false;
	public static GoogleApiClient googleApiClient = null;
	public static LocationRequest locationRequest = null;
	public static LocationListener locationListener = null;

}
